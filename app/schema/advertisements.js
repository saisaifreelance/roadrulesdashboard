export default [
  {
    title: 'ID',
    key: 'id',
    type: 'text',
    tableEditable: false,
    formEditable: false,
    required: true,
  },
  {
    title: 'Advertisement',
    key: 'text',
    type: 'text',
    tableEditable: true,
    formEditable: true,
    required: true,
  },
  {
    title: 'Date created',
    key: 'date_created',
    type: 'text',
  },
];

export default [
  {
    title: 'ID',
    key: 'id',
    type: 'text',
    tableEditable: false,
    formEditable: false,
    required: true,
  },
  {
    title: 'DrivingSchoolBooking',
    key: 'text',
    type: 'text',
  },
  {
    title: 'Date created',
    key: 'date_created',
    type: 'text',
  },
];

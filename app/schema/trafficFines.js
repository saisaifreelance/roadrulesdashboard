export default [
  {
    title: 'ID',
    key: 'id',
    type: 'text',
    tableEditable: false,
    formEditable: false,
    required: true,
  },
  {
    title: 'Penalty',
    key: 'description',
    type: 'text',
    tableEditable: true,
    formEditable: true,
    required: true,
  },
  {
    title: 'Traffic Fine',
    key: 'title',
    type: 'multi-text',
    tableEditable: true,
    formEditable: true,
    required: true,
    validate: (value) => {
      if (typeof value !== 'string' || !value) {
        return 'Penalty is required';
      }
      return null;
    },
  },
  {
    title: 'Explanation',
    key: 'text',
    type: 'multi-text',
    tableEditable: true,
    formEditable: true,
    required: true,
    validate: (value) => {
      if (typeof value !== 'string' || !value) {
        return 'Penalty is required';
      }
      return null;
    },
  },
];

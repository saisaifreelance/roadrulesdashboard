/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import WebFontLoader from 'webfontloader';
import {makeSelectIsAuthenticated, makeSelectIsFetching} from './selectors';
import Header from '../HeaderContainer';
import MainMenu from '../../components/MainMenu';
import Alerts from '../../components/Alerts';
import Footer from '../../components/Footer';
import AuthProcessing from '../../components/AuthProcessing';
import { notify } from '../../utils/notifications';

WebFontLoader.load({
  google: {
    families: ['Roboto:300,400,500,700', 'Material Icons'],
  },
});

class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
    isAuthenticated: React.PropTypes.bool.isRequired,
    isFetching: React.PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: null,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.toggleMessages = this.toggleMessages.bind(this);
    this.toggleAlerts = this.toggleAlerts.bind(this);
    this.toggleTasks = this.toggleTasks.bind(this);
    this.togglePane = this.togglePane.bind(this);
    this.closeSideBar = this.closeSideBar.bind(this);
  }
  componentDidMount() {
    if (this.props.isAuthenticated) {
      document.body.setAttribute('data-ma-header', 'orange');
    }
    else {
      document.body.setAttribute('data-ma-header', '');
    }
    // notify('Welcome back Mallinda Hollaway', 'inverse');
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.isAuthenticated && this.props.isAuthenticated) {
      document.body.setAttribute('data-ma-header', 'orange');
    }
    if (!this.props.isAuthenticated && prevProps.isAuthenticated) {
      document.body.setAttribute('data-ma-header', '');
    }
  }
  toggleMenu(e) {
    e.preventDefault();
    this.togglePane('main-menu');
  }
  toggleMessages(e) {
    e.preventDefault();
    this.togglePane('messages');
  }
  toggleAlerts(e) {
    e.preventDefault();
    this.togglePane('alerts');
  }
  toggleTasks(e) {
    e.preventDefault();
    this.togglePane('tasks');
  }
  togglePane(open) {
    if (this.state.open === open) {
      return this.setState({ open: null });
    }
    return this.setState({ open });
  }
  closeSideBar(e) {
    e.preventDefault();
    return this.setState({ open: null });
  }
  render() {
    const { isAuthenticated, isFetching } = this.props;
    if (isFetching) {
      return (
        <div className="login" data-lbg="orange">
          <AuthProcessing message={isFetching} />
        </div>
      );
    }
    if (!isAuthenticated) {
      return (
        <div className="login" data-lbg="orange">
          {React.Children.toArray(this.props.children)}
        </div>
      );
    }
    const sideBarBackdrop = this.state.open ? (
      <div onClick={this.closeSideBar} data-ma-action="sidebar-close" className="sidebar-backdrop animated fadeIn" />
    ) : null
    return (
      <div>
        <Header
          toggleMenu={this.toggleMenu}
          toggleTasks={this.toggleTasks}
          toggleMessages={this.toggleMessages}
          toggleAlerts={this.toggleAlerts}
          closeSideBar={this.closeSideBar}
        />
        <section id="main">
          <MainMenu
            location={this.props.location}
            params={this.props.params}
            open={this.state.open}
            toggleMenu={this.toggleMenu}
            toggleTasks={this.toggleTasks}
            toggleMessages={this.toggleMessages}
            toggleAlerts={this.toggleAlerts}
            closeSideBar={this.closeSideBar}
          />
          <Alerts
            open={this.state.open}
            toggleTasks={this.toggleTasks}
            toggleMessages={this.toggleMessages}
            toggleAlerts={this.toggleAlerts}
            closeSideBar={this.closeSideBar}
          />
          {React.Children.toArray(this.props.children)}
          <Footer />
          {sideBarBackdrop}
        </section>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  isAuthenticated: makeSelectIsAuthenticated(),
  isFetching: makeSelectIsFetching(),
});

export default connect(mapStateToProps)(App);

import { createSelector } from 'reselect';
import { pathToJS } from 'react-redux-firebase';

// makeSelectLocationState expects a plain JS object for the routing state
const makeSelectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

const makeSelectFirebase = () => (state) => state.get('firebase');

const makeSelectIsFetching = () => createSelector(
  makeSelectFirebase(),
  (firebase) => {
    const auth = firebase.get('auth');
    if (typeof auth === 'undefined') {
      return 'No data';
    }

    const isInitializing = firebase.get('isInitializing');
    if (isInitializing) {
      return 'Firebase initializing';
    }

    return null;
  }
);

const makeSelectIsAuthenticated = () => createSelector(
  makeSelectFirebase(),
  (firebase) => {
    const auth = pathToJS(firebase, 'auth');
    return !!auth;
  }
);

export {
  makeSelectLocationState,
  makeSelectIsAuthenticated,
  makeSelectIsFetching,
};

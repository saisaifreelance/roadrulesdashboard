
import { fromJS } from 'immutable';
import initialiseContainerReducer from '../reducer';

describe('initialiseContainerReducer', () => {
  it('returns the initial state', () => {
    expect(initialiseContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

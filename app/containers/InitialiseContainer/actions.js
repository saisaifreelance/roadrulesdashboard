/*
 *
 * InitialiseContainer actions
 *
 */

import {
  CLEAR_DATA,
  CLEAR_DATA_SUCCESS,
  CLEAR_DATA_FAIL,
  RESET_DATA,
  RESET_DATA_SUCCESS,
  RESET_DATA_FAIL,
  CLEAR_CLIENTS_DATA,
  CLEAR_CLIENTS_DATA_SUCCESS,
  CLEAR_CLIENTS_DATA_FAIL,
  RESET_CLIENTS_DATA,
  RESET_CLIENTS_DATA_FAIL,
  RESET_CLIENTS_DATA_SUCCESS,
} from './constants';

// Clear all data actions
export function clearDataAction() {
  return {
    type: CLEAR_DATA,
  };
}

export function clearDataSuccessAction() {
  return {
    type: CLEAR_DATA_SUCCESS,
  };
}

export function clearDataFailAction(error) {
  return {
    type: CLEAR_DATA_FAIL,
    error,
  };
}

// Reset all data actions
export function resetDataAction() {
  return {
    type: RESET_DATA,
  };
}

export function resetDataSuccessAction() {
  return {
    type: RESET_DATA_SUCCESS,
  };
}

export function resetDataFailAction(error) {
  return {
    type: RESET_DATA_FAIL,
    error
  };
}

// Clear all clients data actions
export function clearClientsDataAction() {
  return {
    type: CLEAR_CLIENTS_DATA,
  };
}

export function clearClientsDataSuccessAction() {
  return {
    type: CLEAR_CLIENTS_DATA_SUCCESS,
  };
}

export function clearClientsDataFailAction(error) {
  return {
    type: CLEAR_CLIENTS_DATA_FAIL,
    error,
  };
}

// Reset all clients data actions
export function resetClientsDataAction() {
  return {
    type: RESET_CLIENTS_DATA,
  };
}

export function resetClientsDataSuccessAction() {
  return {
    type: RESET_CLIENTS_DATA_SUCCESS,
  };
}

export function resetClientsDataFailAction(error) {
  return {
    type: RESET_CLIENTS_DATA_FAIL,
    error,
  };
}

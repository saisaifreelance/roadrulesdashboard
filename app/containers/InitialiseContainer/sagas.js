import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';

// JSON DATA
import clientsJson from '../../../other/clients.json';

import {
  CLEAR_DATA,
  CLEAR_DATA_SUCCESS,
  CLEAR_DATA_FAIL,
  RESET_DATA,
  RESET_DATA_SUCCESS,
  RESET_DATA_FAIL,
  CLEAR_CLIENTS_DATA,
  CLEAR_CLIENTS_DATA_SUCCESS,
  CLEAR_CLIENTS_DATA_FAIL,
  RESET_CLIENTS_DATA,
  RESET_CLIENTS_DATA_FAIL,
  RESET_CLIENTS_DATA_SUCCESS,
} from './constants';

import {
  clearDataSuccessAction,
  clearDataFailAction,
  resetDataSuccessAction,
  resetDataFailAction,
} from './actions';


const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

const defaultData = {
  clients: {},
  meta_clients: defaultPeriodStats,
  stats_monthly: {},
  stats_weekly: {},
  stats_daily: {},
  questions: {},
  meta_questions: {
    count: 0,
  },
  driving_school_bookings: {},
  meta_driving_school_bookings: {
    count: 0,
  },
  provisional_test_school_bookings: {},
  meta_provisional_test_school_bookings: {
    count: 0,
  },
  payments: {},
  meta_payments: {
    count: 0,
  },
  notes: {},
  meta_notes: {
    count: 0,
  },
  traffic_fines: {},
  meta_traffic_fines: {
    count: 0,
  },
  push_notifications: {},
  meta_push_notifications: {
    count: 0,
  },
  advertisements: {},
  meta_advertisements: {
    count: 0,
  },
  users: {},
  meta_users: {
    count: 0,
  },
};

export function mergeStats(source = {}, target = {}, ...targets) {
  if (targets.length > 2) {
    return mergeStats(
      source,
      target,
      ...targets.slice(0, targets.length - 3),
      mergeStats(
        targets[targets.length - 2],
        targets[targets.length - 1]
      )
    );
  }
  if (targets.length > 1) {
    return mergeStats(
      source,
      target,
      mergeStats(
        targets[targets.length - 2],
        targets[targets.length - 1]
      )
    );
  }
  if (targets.length > 0) {
    return mergeStats(
      source,
      mergeStats(
        target,
        targets[targets.length - 1]
      )
    );
  }

  // We have two objects only, recursively add all values in target to source
  const result = { ...source };
  const keys = Object.keys(target);
  for (let i = 0; i < keys.length; i += 1) {
    const key = keys[i]
    const targetType = typeof target[key];
    const sourceType = typeof result[key];

    if (target[key] && !(targetType === 'object' || targetType === 'number')) {
      throw new Error(`Data of invalid type ${targetType}`);
    }

    if (target[key] && result[key] && sourceType !== targetType) {
      throw new Error(`Data type of ${sourceType} is not compatible with ${targetType}`);
    }

    if (target[key] && targetType === 'object') {
      result[key] = mergeStats(typeof result[key] === 'object' && result[key] ? result[key] : {}, target[key]);
    }
    if (target[key] && targetType === 'number') {
      result[key] = (result[key] ? result[key] : 0) + target[key];
    }
    if (!target[key] && !result[key]) {
      result[key] = 0;
    }
  }
  return result;
}

function getMonthStartTime(time) {
  const date = new Date(time);
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

function getWeekStartTime(time) {
  const date = new Date(time);
  const diff = date.getDate() - date.getDay();
  date.setDate(diff);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

function getDayStartTime(time) {
  const date = new Date(time);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

function updateMonthlyStatistics(client, monthlyStatistics = {}) {
  let paidLearnerMonthDiff = {};
  let paidDriverMonthDiff = {};

  const downloadMonth = getMonthStartTime(client.date_created);
  const downloadMonthDiff = {
    [downloadMonth]: { ...defaultPeriodStats, count: 1 },
  }

  if (client.date_paid_learner) {
    const paidLearnerMonth = getMonthStartTime(client.date_paid_learner);
    paidLearnerMonthDiff = {
      [paidLearnerMonth]: {...defaultPeriodStats, paid_learner: 1},
    };
  }

  if (client.date_paid_driver) {
    const paidDriverMonth = getMonthStartTime(client.date_paid_driver);
    paidDriverMonthDiff = {
      [paidDriverMonth]: { ...defaultPeriodStats, paid_driver: 1 },
    };
  }

  const updatedMonthlyStatistics = mergeStats(monthlyStatistics, downloadMonthDiff, paidLearnerMonthDiff, paidDriverMonthDiff);
  return updatedMonthlyStatistics;
}

function updateWeeklyStatistics(client, monthlyStatistics = {}) {
  let paidLearnerWeekDiff = {};
  let paidDriverWeekDiff = {};

  const downloadWeek = getWeekStartTime(client.date_created);
  const downloadWeekDiff = {
    [downloadWeek]: { ...defaultPeriodStats, count: 1 },
  }

  if (client.date_paid_learner) {
    const paidLearnerWeek = getWeekStartTime(client.date_paid_learner);
    paidLearnerWeekDiff = {
      [paidLearnerWeek]: {...defaultPeriodStats, paid_learner: 1},
    };
  }

  if (client.date_paid_driver) {
    const paidDriverWeek = getWeekStartTime(client.date_paid_driver);
    paidDriverWeekDiff = {
      [paidDriverWeek]: { ...defaultPeriodStats, paid_driver: 1 },
    };
  }

  const updatedWeeklyStatistics = mergeStats(monthlyStatistics, downloadWeekDiff, paidLearnerWeekDiff, paidDriverWeekDiff);
  return updatedWeeklyStatistics;
}

function updateDailyStatistics(client, dailyStatistics = {}) {
  let paidLearnerDayDiff = {};
  let paidDriverDayDiff = {};

  const downloadDay = getDayStartTime(client.date_created);
  const downloadDayDiff = {
    [downloadDay]: { ...defaultPeriodStats, count: 1 },
  }

  if (client.date_paid_learner) {
    const paidLearnerDay = getDayStartTime(client.date_paid_learner);
    paidLearnerDayDiff = {
      [paidLearnerDay]: {...defaultPeriodStats, paid_learner: 1},
    };
  }

  if (client.date_paid_driver) {
    const paidDriverDay = getDayStartTime(client.date_paid_driver);
    paidDriverDayDiff = {
      [paidDriverDay]: { ...defaultPeriodStats, paid_driver: 1 },
    };
  }

  const updatedDailyStatistics = mergeStats(dailyStatistics, downloadDayDiff, paidLearnerDayDiff, paidDriverDayDiff);
  return updatedDailyStatistics;
}


function setAllDataToDefault() {
  const firebase = getFirebase();
  return firebase
    .set('/', defaultData)
    .catch((error) => error);
}


function resetAllDataFromJsonFiles() {
  const database = clientsJson.reduce((data, client, index) => {
    if (!(index % 100)) {
    }
    let formattedClient;
    {
      const { id, phone_number, firstname, lastname, location, imei, firebase_token, activation_code_learner, activation_code_driver, date_paid_learner, date_paid_driver, payment_learner, payment_driver, date_created, date_modified} = client;
      formattedClient = {
        id,
        phone_number,
        firstname,
        lastname,
        location,
        imei,
        firebase_token,
        activation_code_learner,
        activation_code_driver,
        payment_learner,
        payment_driver,
        payments: {},
      };
      formattedClient.date_created = date_created ? (new Date(date_created)).getTime() : null;
      formattedClient.date_modified = date_modified ? (new Date(date_modified)).getTime() : null;
      formattedClient.date_paid_learner = date_paid_learner ? (new Date(date_paid_learner)).getTime() : null;
      formattedClient.date_paid_driver = date_paid_driver ? (new Date(date_paid_driver)).getTime() : null;
    }

    const payments = { ...data.payments };
    if (formattedClient.date_paid_learner) {
      const id = `payment_${Object.keys(payments).length}`;
      payments[id] = {
        id,
        amount: 2,
        product: 'activation_learner',
        date_created: formattedClient.date_paid_learner,
        payment_provider: 'ecocash',
        merchant_code: '17993',
        merchant_verified: formattedClient.date_paid_learner,
        client_verified: formattedClient.date_paid_learner,
      };
      formattedClient.payments[id] = true;
    }
    if (formattedClient.date_paid_driver) {
      const id = `payment_${Object.keys(payments).length}`;
      payments[id] = {
        id,
        amount: 2,
        product: 'activation_driver',
        date_created: formattedClient.date_paid_driver,
        payment_provider: 'ecocash',
        merchant_code: '17993',
        merchant_verified: formattedClient.date_paid_driver,
        client_verified: formattedClient.date_paid_driver,
      };
      formattedClient.payments[id] = true;
    }

    const clients = {...data.clients, [`client_${formattedClient.id}`]: formattedClient};
    const meta_clients = {
      ...data.meta_clients,
      count: data.meta_clients.count + 1,
      paid_learner: formattedClient.date_paid_learner ? data.meta_clients.paid_learner + 1 : data.meta_clients.paid_learner,
      paid_driver: formattedClient.date_paid_driver ? data.meta_clients.paid_driver + 1 : data.meta_clients.paid_driver,
    };
    const stats_monthly = updateMonthlyStatistics(client, data.stats_monthly);
    const stats_weekly = updateWeeklyStatistics(client, data.stats_weekly);
    const stats_daily = updateDailyStatistics(client, data.stats_daily);
    return { ...data, payments, clients, meta_clients, stats_monthly, stats_weekly, stats_daily };
  }, defaultData);
  const firebase = getFirebase();
  // return Promise.resolve(null);
  return firebase
    .set('/', database)
    .then((error) => error)
    .catch((error) => error);
}

function* clearData() {
  try {
    const error = yield call(setAllDataToDefault);
    if (!error) {
      yield put(clearDataSuccessAction());
      return;
    }
    yield put(clearDataFailAction(error));
  } catch (e) {
    yield put(clearDataFailAction(e));
  }
}

function* resetData() {
  try {
    const error = yield call(resetAllDataFromJsonFiles);
    if (!error) {
      yield put(resetDataSuccessAction());
      return;
    }
    yield put(resetDataFailAction(error));
  } catch (e) {
    yield put(resetDataFailAction(e));
  }
}

function* clearClientsData() {

}

function* resetClientsData() {

}

// Individual exports for testing
export function* clearDataSaga() {
  yield* takeLatest(CLEAR_DATA, clearData);
}

export function* resetDataSaga() {
  yield* takeLatest(RESET_DATA, resetData);
}

export function* clearClientsDataSaga() {
  yield* takeLatest(CLEAR_CLIENTS_DATA, clearClientsData);
}

export function* resetClientsDataSaga() {
  yield* takeLatest(RESET_CLIENTS_DATA, resetClientsData);
}

// All sagas to be loaded
export default [
  clearDataSaga,
  resetDataSaga,
  clearClientsDataSaga,
  resetClientsDataSaga,
];

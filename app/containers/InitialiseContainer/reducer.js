/*
 *
 * InitialiseContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CLEAR_CLIENTS_DATA,
  RESET_CLIENTS_DATA,
} from './constants';

const initialState = fromJS({});

function initialiseContainerReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR_CLIENTS_DATA:
      return state;
    case RESET_CLIENTS_DATA:
      return state;
    default:
      return state;
  }
}

export default initialiseContainerReducer;

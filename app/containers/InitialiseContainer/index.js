/*
 *
 * InitialiseContainer
 *
 */

import React, { PropTypes } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { firebaseConnect } from 'react-redux-firebase';
import makeSelectInitialiseContainer from './selectors';
import { clearClientsDataAction, resetClientsDataAction, resetDataAction, clearDataAction } from './actions';

export class InitialiseContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div>
        <Helmet
          title="InitialiseContainer"
          meta={[
            { name: 'description', content: 'Description of InitialiseContainer' },
          ]}
        />
        <div onClick={this.props.clearData}>
          click here to clear the data
        </div>
        <div onClick={() => {
          alert('doing it');
          this.props.resetData();
        }}>
          click here to reset the data
        </div>
      </div>
    );
  }
}

InitialiseContainer.propTypes = {
  resetData: PropTypes.func.isRequired,
  resetClientsData: PropTypes.func.isRequired,
  clearClientsData: PropTypes.func.isRequired,
  clearData: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  InitialiseContainer: makeSelectInitialiseContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    clearClientsData: () => dispatch(clearClientsDataAction()),
    resetClientsData: () => dispatch(resetClientsDataAction()),
    resetData: () => dispatch(resetDataAction()),
    clearData: () => dispatch(clearDataAction()),
  };
}

export default compose(
  firebaseConnect([]),
  connect(mapStateToProps, mapDispatchToProps)
)(InitialiseContainer);

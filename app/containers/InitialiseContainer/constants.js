/*
 *
 * InitialiseContainer constants
 *
 */

export const CLEAR_DATA = 'app/InitialiseContainer/CLEAR_DATA';
export const CLEAR_DATA_SUCCESS = 'app/InitialiseContainer/CLEAR_DATA_SUCCESS';
export const CLEAR_DATA_FAIL = 'app/InitialiseContainer/CLEAR_DATA_FAIL';

export const RESET_DATA = 'app/InitialiseContainer/RESET_DATA';
export const RESET_DATA_SUCCESS = 'app/InitialiseContainer/RESET_DATA_SUCCESS';
export const RESET_DATA_FAIL = 'app/InitialiseContainer/RESET_DATA_FAIL';

export const CLEAR_CLIENTS_DATA = 'app/InitialiseContainer/CLEAR_CLIENTS_DATA';
export const CLEAR_CLIENTS_DATA_SUCCESS = 'app/InitialiseContainer/CLEAR_CLIENTS_DATA_SUCCESS';
export const CLEAR_CLIENTS_DATA_FAIL = 'app/InitialiseContainer/CLEAR_CLIENTS_DATA_FAIL';

export const RESET_CLIENTS_DATA = 'app/InitialiseContainer/RESET_CLIENTS_DATA';
export const RESET_CLIENTS_DATA_SUCCESS = 'app/InitialiseContainer/RESET_CLIENTS_DATA_SUCCESS';
export const RESET_CLIENTS_DATA_FAIL = 'app/InitialiseContainer/RESET_CLIENTS_DATA_FAIL';

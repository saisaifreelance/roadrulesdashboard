import { createSelector } from 'reselect';

/**
 * Direct selector to the initialiseContainer state domain
 */
const selectInitialiseContainerDomain = () => (state) => state.get('initialiseContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by InitialiseContainer
 */

const makeSelectInitialiseContainer = () => createSelector(
  selectInitialiseContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectInitialiseContainer;
export {
  selectInitialiseContainerDomain,
};

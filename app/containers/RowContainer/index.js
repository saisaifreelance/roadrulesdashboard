/*
 *
 * RowContainer
 *
 */

import React, { PropTypes } from 'react';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectRowContainer from './selectors';
import { setPathAction, editItemAction, editItemErrorAction, saveItemAction } from './actions';
import FormRow from '../../components/FormRow';
import Loading from '../../components/Loading';

export class RowContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.setPath(this.props.itemRoot, this.props.itemKey);
  }
  render() {
    if (this.props.RowContainer.isFetching) {
      return (
        <div>
          <Loading message={this.props.RowContainer.isFetching} />
        </div>
      );
    }
    if (!this.props.RowContainer.data) {
      return (
        <div>
          <Loading message={'No data'} />
        </div>
      );
    }
    return (
      <FormRow
        index={this.props.index}
        schema={this.props.schema}
        {...this.props.RowContainer}
        editItem={this.props.editItem}
        editItemError={this.props.editItemError}
        saveItem={this.props.saveItem}
      />
    );
  }
}

RowContainer.propTypes = {
  index: PropTypes.number.isRequired,
  schema: PropTypes.array.isRequired,
  itemRoot: PropTypes.string.isRequired,
  itemKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,

  RowContainer: PropTypes.shape({
    isFetching: PropTypes.string,
    path: PropTypes.string,
    data: PropTypes.object,
    dataErrors: PropTypes.object,
    form: PropTypes.object,
    formErrors: PropTypes.object,
  }).isRequired,

  editItem: PropTypes.func.isRequired,
  editItemError: PropTypes.func.isRequired,
  saveItem: PropTypes.func.isRequired,
  setPath: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  RowContainer: makeSelectRowContainer(),
});

function mapDispatchToProps(dispatch, props) {
  return {
    setPath: () => dispatch(setPathAction(props.itemRoot, props.itemKey)),
    editItem: (...args) => dispatch(editItemAction(...args)),
    editItemError: (errors) => dispatch(editItemErrorAction(errors)),
    saveItem: (form, path) => dispatch(saveItemAction(form, path)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => {
    if (props.RowContainer.path) {
      return ([
        {
          path: props.RowContainer.path,
        },
      ]);
    }
    return [];
  }),
)(RowContainer);

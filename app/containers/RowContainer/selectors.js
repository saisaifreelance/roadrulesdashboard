import { createSelector } from 'reselect';

/**
 * Direct selector to the rowContainer state domain
 */
const selectRowContainerDomain = () => (state) => state.get('rowContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by RowContainer
 */

const makeSelectRowContainer = () => createSelector(
  selectRowContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectRowContainer;
export {
  selectRowContainerDomain,
};

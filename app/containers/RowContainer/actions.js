/*
 *
 * RowContainer actions
 *
 */

import {
  EDIT_ITEM,
  EDIT_ITEM_ERROR,
  SAVE_ITEM,
  SAVE_ITEM_SUCCESS,
  SAVE_ITEM_FAIL,
  SET_PATH,
  START_ACTION,
  SET_ACTION,
} from './constants';

export function startAction(action) {
  return {
    type: START_ACTION,
    ...action,
  };
}

export function setAction(action) {
  return {
    type: SET_ACTION,
    ...action,
  };
}

export function setPathAction(root, key) {
  return {
    type: SET_PATH,
    root,
    key,
  };
}

// todo add path to all the actions
export function editItemAction(fieldName, newValue, newActiveIndex, event) {
  return {
    type: EDIT_ITEM,
    fieldName,
    newValue,
  };
}

export function editItemErrorAction(errors) {
  return {
    type: EDIT_ITEM_ERROR,
    errors,
  };
}

export function saveItemAction(form, path) {
  return {
    type: SAVE_ITEM,
    form,
    path,
  };
}

export function saveItemSuccessAction(form) {
  return {
    type: SAVE_ITEM_SUCCESS,
    form,
  };
}

export function saveItemFailAction(error) {
  return {
    type: SAVE_ITEM_FAIL,
    error,
  };
}

/*
 *
 * RowContainer reducer
 *
 */

import { fromJS } from 'immutable';
import { actionTypes } from 'react-redux-firebase';
import {
  EDIT_ITEM,
  EDIT_ITEM_ERROR,
  SAVE_ITEM,
  SAVE_ITEM_SUCCESS,
  SAVE_ITEM_FAIL,
  SET_PATH,
  START_ACTION,
  SET_ACTION,
} from './constants';

const initialState = fromJS({
  isFetching: null,
  path: null,
  data: null,
  dataErrors: {},
  form: {},
  formErrors: {},
});

function rowContainerReducer(state = initialState, action) {
  switch (action.type) {
    case SET_PATH:
      return state.set('path', `${action.root}/${action.key}`);
    case actionTypes.START: {
      const path = state.get('path');
      if (action.path === path) {
        return state.set('isFetching', 'Fetching data...');
      }
      return state;
    }
    case actionTypes.SET: {
      const path = state.get('path');
      if (action.path === path) {
        return state
          .set('isFetching', initialState.get('isFetching'))
          .set('data', action.data)
          .set('dataErrors', initialState.get('dataErrors'))
          .set('form', initialState.get('form'))
          .set('formErrors', initialState.get('formErrors'));
      }
      return state;
    }
    case EDIT_ITEM: {
      const { fieldName, newValue } = action;
      return state
        .setIn(['form', fieldName], newValue);
    }
    case EDIT_ITEM_ERROR: {
      const { errors } = action;
      return state.set('formErrors', errors);
    }
    case SAVE_ITEM: {
      return state.set('isFetching', 'Saving item...');
    }
    case SAVE_ITEM_SUCCESS: {
      return state
        .set('isFetching', initialState.get('isFetching'))
        .set('data', initialState.get('data'))
        .set('dataErrors', initialState.get('dataErrors'))
        .set('form', initialState.get('form'))
        .set('formErrors', initialState.get('formErrors'));
    }
    case SAVE_ITEM_FAIL: {
      return state
        .set('isFetching', 'Error saving item!');
    }
    default:
      return state;
  }
}

export default rowContainerReducer;

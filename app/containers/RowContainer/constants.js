/*
 *
 * RowContainer constants
 *
 */

export const EDIT_ITEM = 'app/RowContainer/EDIT_ITEM';
export const EDIT_ITEM_ERROR = 'app/RowContainer/EDIT_ITEM_ERROR';
export const SAVE_ITEM = 'app/RowContainer/SAVE_ITEM';
export const SAVE_ITEM_SUCCESS = 'app/RowContainer/SAVE_ITEM_SUCCESS';
export const SAVE_ITEM_FAIL = 'app/RowContainer/SAVE_ITEM_FAIL';

export const SET_PATH = 'app/RowContainer/SET_PATH';
export const START_ACTION = 'app/RowContainer/START_ACTION';
export const SET_ACTION = 'app/RowContainer/SET_ACTION';

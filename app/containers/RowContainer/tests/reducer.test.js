
import { fromJS } from 'immutable';
import rowContainerReducer from '../reducer';

describe('rowContainerReducer', () => {
  it('returns the initial state', () => {
    expect(rowContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

import { createSelector } from 'reselect';
import schema from './schema';

/**
 * Direct selector to the drivingSchoolsContainer state domain
 */
const selectDrivingSchoolsContainerDomain = (state) => state.get('drivingSchoolsContainer');

/**
 * Other specific selectors
 */
const selectTable = createSelector(
  selectDrivingSchoolsContainerDomain,
  (drivingSchoolsContainerDomain) => {
    const drivingSchoolsContainer = drivingSchoolsContainerDomain.toJS();
    const { metaData, data, isFetchingDrivingSchools, isFetchingMetaDrivingSchools, startAt, endAt, sorted, direction, pageSize, start } = drivingSchoolsContainer;
    const page = Math.floor(start / pageSize) + 1
    return {
      isFetching: isFetchingDrivingSchools || isFetchingMetaDrivingSchools,
      empty: !data || !data.length,
      count: metaData ? metaData.count : 0,
      data: data ? data.slice(0, pageSize).map((client) => {
        const {
          id,
          title,
          description,
          text,
        } = client
        return {
          id,
          title,
          description,
          text,
        };
      }) : data,
      page,
      startAt,
      endAt,
      schema,
      sorted,
      direction,
      pageSize,
      start,
    };
  }
);

const selectQueries = createSelector(
  selectTable,
  (table) => {
    const queryParams = [`orderByChild=${table.sorted}`];

    if (!table.endAt && !table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
    }

    if (table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
      queryParams.push(`startAt=${table.startAt}`);
    }

    if (table.endAt) {
      queryParams.push(`limitToLast=${table.pageSize + 1}`);
      queryParams.push(`endAt=${table.endAt}`);
    }
    return ([
      {
        path: '/driving_schools',
        queryParams,
      },
      {
        path: '/meta_driving_schools',
      },
    ]);
  }
);


/**
 * Default selector used by DrivingSchoolsContainer
 */

const makeSelectDrivingSchoolsContainer = () => createSelector(
  selectDrivingSchoolsContainerDomain,
  selectTable,
  selectQueries,
  (substate, table, queries) => ({
    table,
    queries,
  })
);

export default makeSelectDrivingSchoolsContainer;
export {
  selectDrivingSchoolsContainerDomain,
};


import { fromJS } from 'immutable';
import drivingSchoolsContainerReducer from '../reducer';

describe('drivingSchoolsContainerReducer', () => {
  it('returns the initial state', () => {
    expect(drivingSchoolsContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

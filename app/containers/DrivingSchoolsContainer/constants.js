/*
 *
 * DrivingSchoolsContainer constants
 *
 */

export const PAGINATE = 'app/DrivingSchoolsContainer/PAGINATE';
export const PAGINATE_SUCCESS = 'app/DrivingSchoolsContainer/PAGINATE_SUCCESS';
export const PAGINATE_FAIL = 'app/DrivingSchoolsContainer/PAGINATE_FAIL';
export const SORT = 'app/DrivingSchoolsContainer/SORT';

/*
 *
 * HeaderContainer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { selectForm, selectProfile } from './selectors';
import { logout, changeFieldAction, searchAction } from './actions';
import Dropdown from '../../components/Dropdown';
import classNames from 'classnames';

export class HeaderContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onChangeField = this.onChangeField.bind(this);
    this.onClearForm = this.onClearForm.bind(this);
    this.onFocusSearch = this.onFocusSearch.bind(this);
    this.onBlurSearch = this.onBlurSearch.bind(this);
    this.onSearchOpen = this.onSearchOpen.bind(this);
    this.state = {
      searchFocused: false,
      searchToggled: false,
    };
  }
  componentDidUpdate(prevProps) {
    // todo this is wrong
    if (prevProps.form.query.value && !this.props.form.query.value && this.state.searchFocused) {
      this.setState({ searchFocused: false });
    }
  }
  onSearchOpen(e) {
    e.preventDefault();
    this.setState({ searchFocused: true, searchToggled: true });
  }
  onFocusSearch(e) {
    this.setState({ searchFocused: true });
  }
  onBlurSearch(e) {
    if (!this.props.form.query.value) {
      this.setState({ searchFocused: false });
    }
  }
  onChangeField(e) {
    this.props.onChangeField('query', e.target.value);
  }
  onClearForm(e) {
    e.preventDefault();
    this.props.onChangeField('query', '');
    this.setState({ searchToggled: false });
  }
  render() {
    return (
      <header id="header" className="media">

        <div className="pull-left h-logo">
          <a href="index.html" className="hidden-xs">
            Road Rules
            <small>admin dash board</small>
          </a>

          <div className="menu-collapse" data-ma-action="sidebar-open" data-ma-target="main-menu" onClick={this.props.toggleMenu}>
            <div className="mc-wrap">
              <div className="mcw-line top palette-White bg"></div>
              <div className="mcw-line center palette-White bg"></div>
              <div className="mcw-line bottom palette-White bg"></div>
            </div>
          </div>
        </div>

        <ul className="pull-right h-menu">
          <li className="hm-search-trigger">
            <a href="" data-ma-action="search-open" onClick={this.onSearchOpen}>
              <i className="hm-icon zmdi zmdi-search"></i>
            </a>
          </li>

          <li
            className="hm-alerts"
            data-user-alert="sua-messages"
            data-ma-action="sidebar-open"
            data-ma-target="user-alerts"
          >
            <a href=""><i className="hm-icon zmdi zmdi-notifications"></i></a>
          </li>
          <Dropdown class="hm-profile" icon={{name: 'img/profile-pics/1.jpg', type: 'image', class: 'hm-icon' }} >
            <li>
              <a
                href=""
                onClick={(e) => {
                e.preventDefault();
                this.props.logout();
              }}
              ><i className="zmdi zmdi-time-restore"></i> Logout</a>
            </li>
          </Dropdown>
        </ul>

        <div className={classNames('media-body h-search', {focused: this.state.searchFocused}, {toggled: this.state.searchToggled})}>
          <form className="p-relative">
            <input
              onFocus={this.onFocusSearch}
              onBlur={this.onBlurSearch}
              value={this.props.form.query.value}
              onChange={this.onChangeField}
              type="text"
              className="hs-input"
              placeholder="Prepend search with client:, question:, traffic_fine:, note:, driving_school:..etc to search relevant category"
              ref={(input) => { this.input = input }}
            />
            <i className="zmdi zmdi-search hs-reset" data-ma-action="search-clear" onClick={this.onClearForm}></i>
          </form>
        </div>

      </header>
    );
  }
}

HeaderContainer.propTypes = {
  profile: React.PropTypes.shape({

  }).isRequired,
  form: React.PropTypes.shape({
    query: React.PropTypes.shape({
      value: React.PropTypes.string,
      error: React.PropTypes.string,
    }).isRequired,
    error: React.PropTypes.string,
  }).isRequired,
  onChangeField: React.PropTypes.func.isRequired,
  logout: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  form: selectForm(),
  profile: selectProfile(),
});

function mapDispatchToProps(dispatch) {
  return {
    logout: () => dispatch(logout()),
    onChangeField: (field, value) => dispatch(changeFieldAction(field, value)),
    search: (form) => dispatch(searchAction(form)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);

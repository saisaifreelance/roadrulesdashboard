/*
 *
 * HeaderContainer constants
 *
 */

export const CHANGE_FIELD = 'app/HeaderContainer/CHANGE_FIELD';
export const LOGOUT = 'app/HeaderContainer/LOGOUT';
export const LOGOUT_FAILED = 'app/HeaderContainer/LOGOUT_FAILED';
export const LOGOUT_SUCCEEDED = 'app/HeaderContainer/LOGOUT_SUCCEEDED ';
export const SEARCH = 'app/HeaderContainer/SEARCH';

import { createSelector } from 'reselect';
import { pathToJS } from 'react-redux-firebase';

/**
 * Direct selector to the headerContainer state domain
 */
const selectHeaderContainerDomain = () => (state) => state.get('headerContainer');

/**
 * Other specific selectors
 */
const selectFirebase = () => (state) => state.get('firebase');

const selectForm = () => createSelector(
  selectHeaderContainerDomain(),
  (substate) => {
    return {
      query: substate.get('query').toJS(),
      error: substate.get('error'),
    };
  }
);

const selectProfile =  () => createSelector(
  selectFirebase(),
  (firebase) => {
    return pathToJS(firebase, 'auth');
  }
);


/**
 * Default selector used by HeaderContainer
 */

const makeSelectHeaderContainer = () => createSelector(
  selectHeaderContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectHeaderContainer;
export {
  selectHeaderContainerDomain,
  selectProfile,
  selectForm,
};

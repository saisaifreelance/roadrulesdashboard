/*
 *
 * HeaderContainer actions
 *
 */

import {
  LOGOUT,
  LOGOUT_FAILED,
  LOGOUT_SUCCEEDED,
  CHANGE_FIELD,
  SEARCH,
} from './constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function logoutSucceeded() {
  return {
    type: LOGOUT_SUCCEEDED,
  };
}

export function logoutFailed(error) {
  return {
    type: LOGOUT_FAILED,
    error,
  };
}

export function changeFieldAction(fieldName, newValue) {
  return {
    type: CHANGE_FIELD,
    fieldName,
    newValue,
  };
}

export function searchAction(form) {
  return {
    type: SEARCH,
    form,
  };
}

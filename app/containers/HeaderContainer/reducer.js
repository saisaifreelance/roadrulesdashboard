/*
 *
 * HeaderContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_FIELD, LOGOUT, LOGOUT_FAILED, LOGOUT_SUCCEEDED,
} from './constants';

const initialState = fromJS({
  query: {
    value: '',
    error: '',
  },
  error: '',
});

function setError(state, action) {
  switch (action.error.code) {
    case 'auth/argument-error':
      return state.set('error', action.error.message);
    case 'auth/user-not-found':
      return state.set('error', action.error.message);
    case 'auth/invalid-email':
      return state.updateIn(['email', 'error'], () => action.error.message);
    default:
      break;
  }
  return state;
}

function headerContainerReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_FIELD:
      return state.setIn([action.fieldName, 'value'], action.newValue);
    case LOGOUT_FAILED:
      return setError(state, action);
    default:
      return state;
  }
}

export default headerContainerReducer;

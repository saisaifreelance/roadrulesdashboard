import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';
import {
  LOGOUT,
  LOGOUT_FAILED,
  LOGOUT_SUCCEEDED,
} from './constants';
import {
  logoutFailed,
  logoutSucceeded,
} from './actions';

export function firebaseLogoutRequestion() {
  return getFirebase().logout()
    .then((some, respo) => {
      return some;
    });
}

export function* firebaseLogout() {
  try {
    const user = yield call(firebaseLogoutRequestion);
    yield put(logoutSucceeded(user));
  } catch (e) {
    yield put(logoutFailed(e));
  }
}

// Individual exports for testing
export function* logoutSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(LOGOUT, firebaseLogout);
}

// All sagas to be loaded
export default [
  logoutSaga,
];

/*
 *
 * NotesContainer actions
 *
 */

import {
  PAGINATE, PAGINATE_SUCCESS, PAGINATE_FAIL, SORT,
} from './constants';

export function paginate(start, pageSize, currentPage) {
  return {
    type: PAGINATE,
    start,
    pageSize,
    currentPage,
  };
}

export function paginateSuccess(startArt, start, pageSize) {
  return {
    type: PAGINATE_SUCCESS,
    startArt,
    start,
    pageSize,
  };
}

export function paginateFail(startArt, start, pageSize) {
  return {
    type: PAGINATE_FAIL,
    startArt,
    start,
    pageSize,
  };
}

export function sort(sorted, direction) {
  return {
    type: SORT,
    sorted,
    direction,
  };
}

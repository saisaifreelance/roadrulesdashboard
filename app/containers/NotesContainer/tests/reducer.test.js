
import { fromJS } from 'immutable';
import notesContainerReducer from '../reducer';

describe('notesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(notesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

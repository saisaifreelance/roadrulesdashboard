import { createSelector } from 'reselect';
import schema from './schema';

/**
 * Direct selector to the notesContainer state domain
 */
const selectNotesContainerDomain = (state) => state.get('notesContainer');

/**
 * Other specific selectors
 */
const selectTable = createSelector(
  selectNotesContainerDomain,
  (notesContainerDomain) => {
    const notesContainer = notesContainerDomain.toJS();
    const { metaData, data, isFetchingNotes, isFetchingMetaNotes, startAt, endAt, sorted, direction, pageSize, start } = notesContainer;
    const page = Math.floor(start / pageSize) + 1
    return {
      isFetching: isFetchingNotes || isFetchingMetaNotes,
      empty: !data || !data.length,
      count: metaData ? metaData.count : 0,
      data: data ? data.slice(0, pageSize) : data,
      page,
      startAt,
      endAt,
      schema,
      sorted,
      direction,
      pageSize,
      start,
    };
  }
);

const selectQueries = createSelector(
  selectTable,
  (table) => {
    const queryParams = [`orderByChild=${table.sorted}`];

    if (!table.endAt && !table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
    }

    if (table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
      queryParams.push(`startAt=${table.startAt}`);
    }

    if (table.endAt) {
      queryParams.push(`limitToLast=${table.pageSize + 1}`);
      queryParams.push(`endAt=${table.endAt}`);
    }
    return ([
      {
        path: '/notes',
        queryParams,
      },
      {
        path: '/meta_notes',
      },
    ]);
  }
);


/**
 * Default selector used by NotesContainer
 */

const makeSelectNotesContainer = () => createSelector(
  selectNotesContainerDomain,
  selectTable,
  selectQueries,
  (substate, table, queries) => ({
    table,
    queries,
  })
);

export default makeSelectNotesContainer;
export {
  selectNotesContainerDomain,
};

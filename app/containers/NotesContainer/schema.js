export default [
  {
    title: 'ID',
    key: 'id',
    type: 'text',
    tableEditable: false,
    formEditable: false,
    required: true,
  },
  {
    title: 'Title',
    key: 'title',
    type: 'text',
    tableEditable: true,
    formEditable: true,
    required: true,
  },
  {
    title: 'Text',
    key: 'text',
    type: 'multi-text',
    tableEditable: true,
    formEditable: true,
    required: true,
  },
  {
    title: 'Views',
    key: 'views',
    type: 'text',
  },
];

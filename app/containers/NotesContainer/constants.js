/*
 *
 * NotesContainer constants
 *
 */

export const PAGINATE = 'app/NotesContainer/PAGINATE';
export const PAGINATE_SUCCESS = 'app/NotesContainer/PAGINATE_SUCCESS';
export const PAGINATE_FAIL = 'app/NotesContainer/PAGINATE_FAIL';
export const SORT = 'app/NotesContainer/SORT';

/*
 *
 * DailySalesContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SET_TARGET,
} from './constants';

const initialState = fromJS({
  target: 100,
});

function dailySalesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TARGET: {
      const target = parseInt(action.target);
      if (isNaN(target)) {
        return state;
      }
      return state.set('target', target);
    }
    default:
      return state;
  }
}

export default dailySalesContainerReducer;

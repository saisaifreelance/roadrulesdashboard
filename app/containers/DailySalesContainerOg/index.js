/*
 *
 * DailySalesContainer
 *
 */

import React, { PropTypes } from 'react';
import {compose} from 'redux';
import { connect } from 'react-redux';
import {firebaseConnect} from 'react-redux-firebase';
import { createStructuredSelector } from 'reselect';
import { setTargetAction } from './actions';
import makeSelectDailySalesContainer, { makeSelectDailySalesData, makeSelectDailySalesStatus, makeSelectOptions } from './selectors';
import ChartDownloads from '../../components/ChartDownloads';

export class DailySalesContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ChartDownloads
        title="Daily downloads"
        description="Daily downloads compared with sales"
        data={this.props.data}
        isFetching={this.props.isFetching}
        options={this.props.options}
        target={this.props.dailySalesContainer.target}
        setTarget={this.props.setTarget}
      />
    );
  }
}

DailySalesContainer.propTypes = {
  dailySalesContainer: PropTypes.object.isRequired,
  data: PropTypes.array,
  isFetching: PropTypes.string,
  options: PropTypes.object.isRequired,
  setTarget: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  dailySalesContainer: makeSelectDailySalesContainer(),
  data: makeSelectDailySalesData(),
  isFetching: makeSelectDailySalesStatus(),
  options: makeSelectOptions(),
});

function mapDispatchToProps(dispatch) {
  return {
    setTarget: (target) => dispatch(setTargetAction(target)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect(() => ([
    {
      path: '/stats_daily',
      queryParams: ['orderByKey', 'limitToLast=4'],
    },
  ]))
)(DailySalesContainer);

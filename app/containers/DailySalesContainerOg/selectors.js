import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS} from 'react-redux-firebase';

const options = {
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
    labelFormatter: (label, series) => {
      return label;
    },
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
}

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
}

/**
 * Direct selector to the dailySalesContainer state domain
 */
const selectDailySalesContainerDomain = () => (state) => state.get('dailySalesContainer');

/**
 * Other specific selectors
 */
const makeSelectFirebase = () => (state) => state.get('firebase');

/**
 * Default selector used by DailySalesContainer
 */

const getLast7days = () => {
  const date = new Date();
  date.setHours(0, 0, 0, 0);
  const days = [date.getTime()];
  for (let i = 0; i < 6; i += 1) {
    date.setDate(date.getDate() - 1);
    days.push(date.getTime());
  }
  return days;
}

const makeSelectDailySalesContainer = () => createSelector(
  selectDailySalesContainerDomain(),
  (substate) => substate.toJS()
);


const makeSelectDailySalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'stats_daily')
);

const makeSelectOptions = () => createSelector(
  getLast7days,
  (last7Days) => {
    const ticks = last7Days.reverse().map((day, index) => [index, (new Date(day)).toDateString()])
    const xaxis = { ...options.xaxis, ticks };
    return { ...options, xaxis };
  }
);

const makeSelectDailySalesData = () => createSelector(
  makeSelectFirebase(),
  makeSelectDailySalesStatus(),
  makeSelectDailySalesContainer(),
  (firebase, isFetching, dailySalesContainer) => {
    if (isFetching) {
      return null;
    }
    const statsDaily = dataToJS(firebase, 'stats_daily');

    return getLast7days().reverse().reduce((dailySalesData, day, index) => {
      const dataForDay = statsDaily[day] ? statsDaily[day] : defaultPeriodStats;
      dailySalesData[0].data.push([
        index,
        dailySalesContainer.target,
      ])
      dailySalesData[1].data.push([
        index,
        dataForDay.count,
      ])
      dailySalesData[2].data.push([
        index,
        dataForDay.paid_learner,
      ])
      dailySalesData[3].data.push([
        index,
        dataForDay.paid_driver,
      ])
      return dailySalesData;
    }, [
      { data: [], lines: { show: true, fill: 0.98 }, label: 'Daily Target', stack: true, color: '#FF0000' },
      { data: [], lines: { show: true, fill: 0.98 }, label: 'Total Downloads', stack: true, color: '#e3e3e3' },
      { data: [], lines: { show: true, fill: 0.98 }, label: 'Total Paid for learner', stack: true, color: '#f1dd2c' },
      { data: [], lines: { show: true, fill: 0.98 }, label: 'Total Paid for driver', stack: true, color: '#8BC34A' },
    ]);
  }
);

export default makeSelectDailySalesContainer;
export {
  selectDailySalesContainerDomain,
  makeSelectDailySalesContainer,
  makeSelectDailySalesStatus,
  makeSelectDailySalesData,
  makeSelectOptions,
};

/*
 *
 * DailySalesContainer actions
 *
 */

import {
  SET_TARGET,
} from './constants';

export function setTargetAction(target) {
  return {
    type: SET_TARGET,
    target,
  };
}

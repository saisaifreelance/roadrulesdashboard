import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS} from 'react-redux-firebase';

const options = {
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
    labelFormatter: (label, series) => {
      return label;
    },
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  grid: {
    borderWidth: 0,
    labelMargin: 10,
    hoverable: true,
    clickable: true,
    mouseActiveRadius: 6,
    show: true,
    markings: [
      {color: '#00FF00', lineWidth: 10, xaxis: {from: 0, to: 12}, yaxis: {from: 2500, to: 3000}},
    ],
  },
}

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

const getRandomStats = () => ({
  count: Math.floor(Math.random() * 4000),
  paid_learner: Math.floor(Math.random() * 2000),
  paid_driver: Math.floor(Math.random() * 1000),
})

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
};

/**
 * Direct selector to the dailySalesContainer state domain
 */
const selectDailySalesContainerDomain = () => (state) => state.get('dailySalesContainer');

/**
 * Other specific selectors
 */
const makeSelectFirebase = () => (state) => state.get('firebase');

/**
 * Default selector used by DailySalesContainer
 */

const getLast7Days = () => {
  const date = new Date();
  date.setHours(0, 0, 0, 0);
  const days = [date.getTime() + 7200000];
  for (let i = 0; i < 6; i += 1) {
    date.setDate(date.getDate() - 1);
    days.push(date.getTime() + 7200000);
  }
  return days;
}

const makeSelectDailySalesContainer = () => createSelector(
  selectDailySalesContainerDomain(),
  (substate) => substate.toJS()
);


const makeSelectDailySalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'stats_daily')
);

const makeSelectDailyTargets = () => createSelector(
  makeSelectDailySalesContainer(),
  makeSelectFirebase(),
  (DailySalesContainer, firebase) => {
    let targets = dataToJS(firebase, '/targets/daily');
    if (isEmpty(targets) || !isLoaded(targets) || !Object.keys(targets).length) {
      targets = {};
    }
    const requiredTargets = ['downloads', 'paidLearner', 'paidDriver'];
    for (let i = 0; i < requiredTargets.length; i += 1) {
      if (!targets[requiredTargets[i]]) {
        targets[requiredTargets[i]] = 0;
      }
    }
    targets = {...targets, ...DailySalesContainer.targets};
    return Object.keys(targets).reduce((accumulator, key) => ({...accumulator, [key]: parseInt(targets[key], 10)}), {});
  }
);

const makeSelectOptions = () => createSelector(
  getLast7Days,
  makeSelectDailySalesContainer(),
  makeSelectDailyTargets(),
  (last4Days, dailySalesContainer, dailyTargets) => {
    const ticks = last4Days.reverse().map((day, index) => [index, (new Date(day)).toDateString()])
    const xaxis = { ...options.xaxis, ticks };
    const markings = (axes) => {
      const { ymax, ymin, yaxis: { box: { height } } } = axes;
      const lineWidth = 2
      const scaledLineWidth = (ymax / height) * lineWidth;
      if (dailySalesContainer.invert) {
        return { ...options, xaxis };
      }
      return [
        {
          color: '#ababab',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: dailyTargets.downloads, to: dailyTargets.downloads + scaledLineWidth}
        },
        {
          color: '#d5c32a',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: dailyTargets.paidLearner, to: dailyTargets.paidLearner + scaledLineWidth}
        },
        {
          color: '#304b24',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: dailyTargets.paidDriver, to: dailyTargets.paidDriver + scaledLineWidth}
        },
      ];
    }
    const grid = {...options.grid, markings}
    return {...options, xaxis, grid};
  }
);

const makeSelectDailySalesData = () => createSelector(
  makeSelectDailySalesContainer(),
  makeSelectFirebase(),
  makeSelectDailySalesStatus(),
  makeSelectDailyTargets(),
  (dailySalesContainer, firebase, isFetching, dailyTargets) => {
    if (isFetching) {
      return null;
    }
    const statsDaily = dataToJS(firebase, 'stats_daily');

    const defaultData = [];
    if (dailySalesContainer.shown.downloads) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Downloads',
        stack: true,
        color: '#e3e3e3',
      });
    }
    if (dailySalesContainer.shown.paidLearner) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Paid for learner',
        stack: true,
        color: '#f1dd2c',
      });
    }
    if (dailySalesContainer.shown.paidDriver) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Paid for driver',
        stack: true,
        color: '#8BC34A',
      });
    }

    return getLast7Days().reverse().reduce((dailySalesData, day, index) => {
      const dataForDay = statsDaily[day] ? statsDaily[day] : defaultPeriodStats;
      let ind = 0;
      if (dailySalesContainer.invert) {
        let downloads = 0;
        let paidLearner = 0;
        let paidDriver = 0;
        if (dailyTargets.downloads) {
          downloads = parseInt(dailyTargets.downloads, 10);
          downloads = isNaN(downloads) ? 0 : dailyTargets.downloads - dataForDay.count;
        }
        if (dailyTargets.paidLearner) {
          paidLearner = parseInt(dailyTargets.paidLearner, 10);
          paidLearner = isNaN(paidLearner) ? 0 : dailyTargets.paidLearner - dataForDay.paid_learner;
        }
        if (dailyTargets.paidDriver) {
          paidDriver = parseInt(dailyTargets.paidDriver, 10);
          paidDriver = isNaN(paidDriver) ? 0 : dailyTargets.paidDriver - dataForDay.paid_driver;
        }
        if (dailySalesContainer.shown.downloads) {
          dailySalesData[ind].data.push([
            index,
            downloads,
          ])
          ind += 1;
        }
        if (dailySalesContainer.shown.paidLearner) {
          dailySalesData[ind].data.push([
            index,
            paidLearner,
          ]);
          ind += 1;
        }
        if (dailySalesContainer.shown.paidDriver) {
          dailySalesData[ind].data.push([
            index,
            paidDriver,
          ]);
          ind += 1;
        }
        return dailySalesData;
      }

      if (dailySalesContainer.shown.downloads) {
        dailySalesData[ind].data.push([
          index,
          dataForDay.count,
        ]);
        ind += 1;
      }
      if (dailySalesContainer.shown.paidLearner) {
        dailySalesData[ind].data.push([
          index,
          dataForDay.paid_learner,
        ]);
        ind += 1;
      }
      if (dailySalesContainer.shown.paidDriver) {
        dailySalesData[ind].data.push([
          index,
          dataForDay.paid_driver,
        ]);
        ind += 1;
      }
      return dailySalesData;
    }, defaultData);
  }
);

export default makeSelectDailySalesContainer;
export {
  makeSelectDailySalesContainer,
  makeSelectDailySalesStatus,
  makeSelectDailySalesData,
  makeSelectOptions,
  makeSelectDailyTargets,
};

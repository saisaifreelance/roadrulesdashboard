/*
 *
 * DailySalesContainer constants
 *
 */

export const SET_TARGET = 'app/DailySalesContainer/SET_TARGET';
export const SET_TARGET_SUCCESS = 'app/DailySalesContainer/SET_TARGET_SUCCESS';
export const SET_TARGET_FAIL = 'app/DailySalesContainer/SET_TARGET_FAIL';

export const SET_INVERT = 'app/DailySalesContainer/SET_INVERT';
export const SET_SHOWN = 'app/DailySalesContainer/SET_SHOWN';

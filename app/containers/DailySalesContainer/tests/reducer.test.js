
import { fromJS } from 'immutable';
import dailySalesContainerReducer from '../reducer';

describe('dailySalesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(dailySalesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

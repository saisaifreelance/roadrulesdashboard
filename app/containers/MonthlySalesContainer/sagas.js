import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';
import {
  SET_TARGET,
} from './constants';
import {
  setTargetSuccessAction,
  setTargetFailAction,
} from './actions';

function updateFirebase(updates) {
  return getFirebase().ref().update(updates).then(
    (error) => {
      if (!error) {
        return updates;
      }
      throw new Error(error);
    });
}

function* updateTarget(action) {
  try {
    yield call(updateFirebase.bind(undefined, action.updates));
    yield put(setTargetSuccessAction(action.targets, action.updates));
  } catch (e) {
    yield put(setTargetFailAction(action.targets, action.updates, e));
  }
}

// Individual exports for testing
export function* updateTargetSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(SET_TARGET, updateTarget);
}

// All sagas to be loaded
export default [
  updateTargetSaga,
];

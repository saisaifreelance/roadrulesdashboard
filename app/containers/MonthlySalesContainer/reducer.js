/*
 *
 * MonthlySalesContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SET_TARGET,
  SET_INVERT,
  SET_SHOWN,
} from './constants';

const initialState = fromJS({
  shown: {
    downloads: true,
    paidLearner: true,
    paidDriver: true,
  },
  targets: {},
  invert: false,
});

function monthlySalesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case SET_TARGET: {
      const targets = Object.keys(action.targets).reduce((accumulator, key) => ({ ...accumulator, [key]: parseInt(action.targets[key], 10) }), {});
      return state.set('targets', targets);
    }
    case SET_INVERT: {
      return state.set('invert', !!action.invert);
    }
    case SET_SHOWN: {
      return state.set('shown', fromJS(action.shown));
    }
    default:
      return state;
  }
}

export default monthlySalesContainerReducer;

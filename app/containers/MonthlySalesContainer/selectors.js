import { createSelector } from 'reselect';
import { firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS } from 'react-redux-firebase';

const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const options = {
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
    labelFormatter: (label) => label,
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  grid: {
    borderWidth: 0,
    labelMargin: 10,
    hoverable: true,
    clickable: true,
    mouseActiveRadius: 6,
    show: true,
    markings: [
      {color: '#00FF00', lineWidth: 10, xaxis: {from: 0, to: 12}, yaxis: {from: 2500, to: 3000}},
    ],
  },
}

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

const getRandomStats = () => ({
  count: Math.floor(Math.random() * 4000),
  paid_learner: Math.floor(Math.random() * 2000),
  paid_driver: Math.floor(Math.random() * 1000),
})

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
};

/**
 * Direct selector to the monthlySalesContainer state domain
 */
const selectMonthlySalesContainerDomain = () => (state) => state.get('monthlySalesContainer');

/**
 * Other specific selectors
 */
const makeSelectFirebase = () => (state) => state.get('firebase');

/**
 * Default selector used by MonthlySalesContainer
 */

const getLast12Months = () => {
  const date = new Date();
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  const months = [date.getTime() + 7200000];
  for (let i = 0; i < 11; i += 1) {
    date.setMonth(date.getMonth() - 1);
    months.push(date.getTime() + 7200000);
  }
  return months;
}

const makeSelectMonthlySalesContainer = () => createSelector(
  selectMonthlySalesContainerDomain(),
  (substate) => substate.toJS()
);


const makeSelectMonthlySalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'stats_monthly')
);

const makeSelectMonthlyTargets = () => createSelector(
  makeSelectMonthlySalesContainer(),
  makeSelectFirebase(),
  (MonthlySalesContainer, firebase) => {
    let targets = dataToJS(firebase, '/targets/monthly');
    if (isEmpty(targets) || !isLoaded(targets) || !Object.keys(targets).length) {
      targets = {};
    }
    const requiredTargets = ['downloads', 'paidLearner', 'paidDriver'];
    for (let i = 0; i < requiredTargets.length; i += 1) {
      if (!targets[requiredTargets[i]]) {
        targets[requiredTargets[i]] = 0;
      }
    }
    targets = {...targets, ...MonthlySalesContainer.targets};
    return Object.keys(targets).reduce((accumulator, key) => ({...accumulator, [key]: parseInt(targets[key], 10)}), {});
  }
);

const makeSelectOptions = () => createSelector(
  getLast12Months,
  makeSelectMonthlySalesContainer(),
  makeSelectMonthlyTargets(),
  (last12Months, monthlySalesContainer, monthlyTargets) => {
    const ticks = last12Months.reverse().map((month, index) => [index, monthNames[(new Date(month).getMonth())]])
    const xaxis = { ...options.xaxis, ticks };
    const markings = (axes) => {
      const { ymax, ymin, yaxis: { box: { height } } } = axes;
      const lineWidth = 2
      const scaledLineWidth = (ymax / height) * lineWidth;
      if (monthlySalesContainer.invert) {
        return { ...options, xaxis };
      }
      return [
        {
          color: '#ababab',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: monthlyTargets.downloads, to: monthlyTargets.downloads + scaledLineWidth}
        },
        {
          color: '#d5c32a',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: monthlyTargets.paidLearner, to: monthlyTargets.paidLearner + scaledLineWidth}
        },
        {
          color: '#304b24',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: monthlyTargets.paidDriver, to: monthlyTargets.paidDriver + scaledLineWidth}
        },
      ];
    }
    const grid = {...options.grid, markings}
    return {...options, xaxis, grid};
  }
);

const makeSelectMonthlySalesData = () => createSelector(
  makeSelectMonthlySalesContainer(),
  makeSelectFirebase(),
  makeSelectMonthlySalesStatus(),
  makeSelectMonthlyTargets(),
  (monthlySalesContainer, firebase, isFetching, monthlyTargets) => {
    if (isFetching) {
      return null;
    }
    const statsMonthly = dataToJS(firebase, 'stats_monthly');

    const defaultData = [];
    if (monthlySalesContainer.shown.downloads) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Downloads',
        stack: true,
        color: '#e3e3e3'
      });
    }
    if (monthlySalesContainer.shown.paidLearner) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Paid for learner',
        stack: true,
        color: '#f1dd2c'
      });
    }
    if (monthlySalesContainer.shown.paidDriver) {
      defaultData.push({
        data: [],
        lines: {show: true, fill: 0.98},
        label: 'Total Paid for driver',
        stack: true,
        color: '#8BC34A',
      });
    }

    return getLast12Months().reverse().reduce((monthlySalesData, month, index) => {
      const dataForMonth = statsMonthly[month] ? statsMonthly[month] : defaultPeriodStats;
      let ind = 0;
      if (monthlySalesContainer.invert) {
        let downloads = 0;
        let paidLearner = 0;
        let paidDriver = 0;
        if (monthlyTargets.downloads) {
          downloads = parseInt(monthlyTargets.downloads, 10);
          downloads = isNaN(downloads) ? 0 : monthlyTargets.downloads - dataForMonth.count;
        }
        if (monthlyTargets.paidLearner) {
          paidLearner = parseInt(monthlyTargets.paidLearner, 10);
          paidLearner = isNaN(paidLearner) ? 0 : monthlyTargets.paidLearner - dataForMonth.paid_learner;
        }
        if (monthlyTargets.paidDriver) {
          paidDriver = parseInt(monthlyTargets.paidDriver, 10);
          paidDriver = isNaN(paidDriver) ? 0 : monthlyTargets.paidDriver - dataForMonth.paid_driver;
        }
        if (monthlySalesContainer.shown.downloads) {
          monthlySalesData[ind].data.push([
            index,
            downloads,
          ])
          ind += 1;
        }
        if (monthlySalesContainer.shown.paidLearner) {
          monthlySalesData[ind].data.push([
            index,
            paidLearner,
          ]);
          ind += 1;
        }
        if (monthlySalesContainer.shown.paidDriver) {
          monthlySalesData[ind].data.push([
            index,
            paidDriver,
          ]);
          ind += 1;
        }
        return monthlySalesData;
      }

      if (monthlySalesContainer.shown.downloads) {
        monthlySalesData[ind].data.push([
          index,
          dataForMonth.count,
        ]);
        ind += 1;
      }
      if (monthlySalesContainer.shown.paidLearner) {
        monthlySalesData[ind].data.push([
          index,
          dataForMonth.paid_learner,
        ]);
        ind += 1;
      }
      if (monthlySalesContainer.shown.paidDriver) {
        monthlySalesData[ind].data.push([
          index,
          dataForMonth.paid_driver,
        ]);
        ind += 1;
      }
      return monthlySalesData;
    }, defaultData);
  }
);

export default makeSelectMonthlySalesContainer;
export {
  makeSelectMonthlySalesContainer,
  makeSelectMonthlySalesStatus,
  makeSelectMonthlySalesData,
  makeSelectOptions,
  makeSelectMonthlyTargets,
};

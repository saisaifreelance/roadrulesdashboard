
import { fromJS } from 'immutable';
import monthlySalesContainerReducer from '../reducer';

describe('monthlySalesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(monthlySalesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

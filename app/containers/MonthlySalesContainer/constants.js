/*
 *
 * MonthlySalesContainer constants
 *
 */

export const SET_TARGET = 'app/MonthlySalesContainer/SET_TARGET';
export const SET_TARGET_SUCCESS = 'app/MonthlySalesContainer/SET_TARGET_SUCCESS';
export const SET_TARGET_FAIL = 'app/MonthlySalesContainer/SET_TARGET_FAIL';

export const SET_INVERT = 'app/MonthlySalesContainer/SET_INVERT';
export const SET_SHOWN = 'app/MonthlySalesContainer/SET_SHOWN';

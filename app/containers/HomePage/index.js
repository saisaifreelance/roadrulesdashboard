/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { userIsAdmin, userIsAuthenticated } from '../../auth';
import MonthlySalesContainer from '../MonthlySalesContainer';
import WeeklySalesContainer from '../WeeklySalesContainer';
import DailySalesContainer from '../DailySalesContainer';
import PaidOverallContainer from '../PaidOverallContainer';
import PaidMonthContainer from '../PaidMonthContainer';
import ChartDynamic from '../../components/ChartDynamic';
import ChartSpark from '../../components/ChartSpark';
import DataGrid from '../../components/DataGrid';

class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount(){
    require('../../../vendors/bower_components/salvattore/dist/salvattore.js');
  }
  render() {
    return (
      <section id="content">
        <div className="container">
          <MonthlySalesContainer />
          <div id="c-grid" className="clearfix" data-columns>
            <WeeklySalesContainer />

            <DailySalesContainer />

            <PaidOverallContainer />

            <PaidMonthContainer />

            <ChartDynamic />

            <ChartSpark />

            <DataGrid />

            <div className="card c-dark palette-Amber bg">
              <div className="card-header p-b-0">
                <h2>For the past 30 days
                  <small>Tortor Magna Parturient</small>
                </h2>
                <ul className="actions a-alt">
                  <li className="dropdown">
                    <a href="" data-toggle="dropdown">
                      <i className="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul className="dropdown-menu dropdown-menu-right">
                      <li>
                        <a href="">Change Date Range</a>
                      </li>
                      <li>
                        <a href="">Change Graph Type</a>
                      </li>
                      <li>
                        <a href="">Other Settings</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div className="card-body">
                <div className="chart-edge">
                  <div className="ns-chart flot-chart m-b-20" id="number-stats-chart"></div>
                </div>

                <div className="list-group lg-alt lg-even-white">
                  <div className="list-group-item media">
                    <div className="pull-right hidden-sm">
                      <div className="sparkline-bar-1"></div>
                    </div>

                    <div className="media-body ns-item">
                      <small>Page Views</small>
                      <h3>47,896,536</h3>
                    </div>
                  </div>

                  <div className="list-group-item media">
                    <div className="pull-right hidden-sm">
                      <div className="sparkline-bar-2"></div>
                    </div>

                    <div className="media-body ns-item">
                      <small>Site Visitors</small>
                      <h3>24,456,799</h3>
                    </div>
                  </div>

                  <div className="list-group-item media">
                    <div className="pull-right hidden-sm">
                      <div className="sparkline-bar-3"></div>
                    </div>

                    <div className="media-body ns-item">
                      <small>Total Clicks</small>
                      <h3>13,965</h3>
                    </div>
                  </div>
                </div>

                <div className="p-5"></div>
              </div>
            </div>

            <div className="card c-dark palette-Grey bg recent-signups">
              <div className="card-header p-b-0">
                <h2>Most Recent Signups
                  <small>Magna Cursus Malesuada</small>
                </h2>
                <ul className="actions a-alt">
                  <li className="dropdown">
                    <a href="" data-toggle="dropdown">
                      <i className="zmdi zmdi-more-vert"></i>
                    </a>

                    <ul className="dropdown-menu dropdown-menu-right">
                      <li>
                        <a href="">Change Date Range</a>
                      </li>
                      <li>
                        <a href="">Change Graph Type</a>
                      </li>
                      <li>
                        <a href="">Other Settings</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>

              <div className="card-body">
                <div className="sparkline-1 p-30"></div>

                <ul className="rs-list">
                  <li>
                    <a href="">
                      <div className="avatar-char">B</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/5.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">L</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">A</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/4.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">Z</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">I</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">S</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">C</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">W</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/3.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">A</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/9.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">N</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">X</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">V</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/7.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/6.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <img className="avatar-img" src="img/profile-pics/8.jpg" alt=""/>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">F</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">E</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">A</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">A</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">M</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">O</div>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <div className="avatar-char">I</div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

          </div>
        </div>
      </section>
    );
  }
}

export default userIsAuthenticated(HomePage);

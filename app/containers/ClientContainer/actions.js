/*
 *
 * ClientContainer actions
 *
 */

import {
  START_ACTION,
  SET_ACTION,
  SET_PATH,

  EDIT_PAYMENT,
  EDIT_PAYMENT_ERROR,
  SAVE_PAYMENT,
  SAVE_PAYMENT_SUCCESS,
  SAVE_PAYMENT_FAIL,

  EDIT_CLIENT,
  EDIT_CLIENT_ERROR,
  SAVE_CLIENT,
  SAVE_CLIENT_SUCCESS,
  SAVE_CLIENT_FAIL,

  ACTIVATE,
  ACTIVATE_SUCCESS,
  ACTIVATE_FAIL,
} from './constants';

export function startAction(action) {
  return {
    type: START_ACTION,
    ...action,
  };
}

export function setAction(action) {
  return {
    type: SET_ACTION,
    ...action,
  };
}

export function setPath(path) {
  return {
    type: SET_PATH,
    path,
  };
}

export function editPayment(fieldName, newValue, newActiveIndex, event) {
  return {
    type: EDIT_PAYMENT,
    fieldName,
    newValue,
  };
}

export function editPaymentError(errors) {
  return {
    type: EDIT_PAYMENT_ERROR,
    errors,
  };
}

export function savePayment(payment, clientId) {
  return {
    type: SAVE_PAYMENT,
    payment,
    clientId,
  };
}

export function savePaymentSuccess(payment) {
  return {
    type: SAVE_PAYMENT_SUCCESS,
    payment,
  };
}

export function savePaymentFail(error) {
  return {
    type: SAVE_PAYMENT_FAIL,
    error,
  };
}

export function activate(id, sendCode) {
  return {
    type: ACTIVATE,
    id,
    sendCode,
  };
}

export function activateSuccess() {
  return {
    type: ACTIVATE_SUCCESS,
  };
}

export function activateFail(error) {
  return {
    type: ACTIVATE_FAIL,
    error,
  };
}

export function editClient(fieldName, newValue, newActiveIndex, event) {
  return {
    type: EDIT_CLIENT,
    fieldName,
    newValue,
  };
}

export function editClientError(errors) {
  return {
    type: EDIT_CLIENT_ERROR,
    errors,
  };
}

export function saveClient(client) {
  return {
    type: SAVE_CLIENT,
    client,
  };
}

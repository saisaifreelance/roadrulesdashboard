/*
 *
 * ClientContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {actionTypes } from 'react-redux-firebase';
import {
  START_ACTION,
  SET_ACTION,
  SET_PATH,

  EDIT_PAYMENT,
  EDIT_PAYMENT_ERROR,
  SAVE_PAYMENT,

  EDIT_CLIENT,
  EDIT_CLIENT_ERROR,
  SAVE_CLIENT, SAVE_PAYMENT_SUCCESS, SAVE_PAYMENT_FAIL,
} from './constants';

const initialState = fromJS({
  isFetching: null,
  isFetchingPayment: null,
  path: null,
  data: null,
  data_errors: {},
});

function clientContainerReducer(state = initialState, action) {
  switch (action.type) {
    case SET_PATH:
      return state.set('path', action.path);
    case actionTypes.START: {
      const path = state.get('path');
      if (action.path === path || action.path === `/learners/${path}`) {
        return state.set('isFetching', 'Fetching client...');
      }
      return state;
    }
    case actionTypes.SET: {
      const path = state.get('path');
      if (action.path === path || action.path === `/learners/${path}`) {
        return state.set('isFetching', null);
      }
      return state;
    }
    case EDIT_PAYMENT: {
      const { fieldName, newValue } = action;
      return state.setIn(['payment', fieldName], newValue);
    }
    case EDIT_PAYMENT_ERROR: {
      const { errors } = action;
      return state.set('payment_errors', errors);
    }
    case EDIT_CLIENT: {
      const {fieldName, newValue} = action;
      return state.setIn(['client', fieldName], newValue);
    }
    case EDIT_CLIENT_ERROR: {
      const {errors} = action;
      return state.set('client_errors', errors);
    }
    default:
      return state;
  }
}

export default clientContainerReducer;

import {call, put} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import { actionTypes, reactReduxFirebase, getFirebase } from 'react-redux-firebase';

import {
  activateSuccess,
  activateFail,
} from './actions';

import {
  ACTIVATE,
} from './constants';

function firebaseUpdate(updates) {
  console.log('>>>> UPDATES')
  console.log(updates)
  return getFirebase().ref().update(updates).then((error) => {
    console.log('>>>> ERROR')
    console.log(error)
    if (!error) {
      return updates;
    }
    throw new Error(error);
  });
}

function sendSMS(from, to, text) {

}

function* saveActivation({ id, sendCode }) {
  const updates = {
    [`/learners/${id}/activatedAt`]: Date.now(),
  }
  try {
    const response = yield call(firebaseUpdate, updates)
    yield put(activateSuccess(response));
    if (sendCode) {
      sendSMS('Road Rules App', '+263774108008', 'Your actication code is ${sendCode}');
    }
  } catch (e) {
    yield put(activateFail(e));
  }
}

export function* activateSaga() {
  yield* takeLatest(ACTIVATE, saveActivation);
}

// All sagas to be loaded
export default [
  activateSaga,
];

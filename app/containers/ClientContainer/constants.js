/*
 *
 * ClientContainer constants
 *
 */

export const DEFAULT_ACTION = 'app/ClientContainer/DEFAULT_ACTION';

export const EDIT_PAYMENT = 'app/ClientContainer/EDIT_PAYMENT';
export const EDIT_PAYMENT_ERROR = 'app/ClientContainer/EDIT_PAYMENT_ERROR';
export const SAVE_PAYMENT = 'app/ClientContainer/SAVE_PAYMENT';
export const SAVE_PAYMENT_SUCCESS = 'app/ClientContainer/SAVE_PAYMENT_SUCCESS';
export const SAVE_PAYMENT_FAIL = 'app/ClientContainer/SAVE_PAYMENT_FAIL';

export const EDIT_CLIENT = 'app/ClientContainer/EDIT_CLIENT';
export const EDIT_CLIENT_ERROR = 'app/ClientContainer/EDIT_CLIENT_ERROR';
export const SAVE_CLIENT = 'app/ClientContainer/SAVE_CLIENT';
export const SAVE_CLIENT_SUCCESS = 'app/ClientContainer/SAVE_CLIENT_SUCCESS';
export const SAVE_CLIENT_FAIL = 'app/ClientContainer/SAVE_CLIENT_FAIL';

export const SET_PATH = 'app/ClientContainer/SET_PATH';
export const START_ACTION = 'app/ClientContainer/START_ACTION';
export const SET_ACTION = 'app/ClientContainer/SET_ACTION';

export const ACTIVATE = 'app/ClientContainer/ACTIVATE';
export const ACTIVATE_SUCCESS = 'app/ClientContainer/ACTIVATE_SUCCESS';
export const ACTIVATE_FAIL = 'app/ClientContainer/ACTIVATE_FAIL';


export const populates = [
  { child: 'payments', root: 'payments' },
];

/*
 *
 * ClientContainer
 *
 */

import React, { PropTypes } from 'react';
import { compose } from 'redux';
import { firebaseConnect } from 'react-redux-firebase';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectClientContainer from './selectors';
import { setPath, editClient, editClientError, saveClient, activate } from './actions';
import FormClient from '../../components/FormClient';
import Loading from '../../components/Loading';

export class ClientContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.setPath(this.props.clientId);
  }
  componentDidUpdate(prevProps) {
    if (prevProps.clientId !== this.props.clientId) {
      this.props.setPath(this.props.clientId);
    }
  }
  render() {
    if (this.props.clientContainer.isFetching) {
      return (
        <div>
          <Loading message={this.props.clientContainer.isFetching} />
        </div>
      );
    }
    if (!this.props.clientContainer.data) {
      return (
        <div>
          <Loading message={'No data'} />
        </div>
      );
    }
    return (
      <FormClient
        index={this.props.index}
        client={this.props.clientContainer.data}
        client_errors={this.props.clientContainer.data_errors}
        onEditClient={this.props.editClient}
        onEditClientError={this.props.editClientError}
        onActivate={this.props.onActivate}
      />
    );
  }
}

ClientContainer.propTypes = {
  editClientError: PropTypes.func.isRequired,
  onActivate: PropTypes.func.isRequired,
  editClient: PropTypes.func.isRequired,
  setPath: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  clientId: PropTypes.string.isRequired,
  clientContainer: PropTypes.shape({
    isFetching: PropTypes.string,
    path: PropTypes.string,
    data: PropTypes.object,
    data_errors: PropTypes.object,
  }).isRequired,
};

const mapStateToProps = createStructuredSelector({
  clientContainer: makeSelectClientContainer(),
});

function mapDispatchToProps(dispatch, ownProps) {
  return {
    editClient: (...args) => dispatch(editClient(...args)),
    editClientError: (errors) => dispatch(editClientError(errors)),
    onActivate: (sendCode) => dispatch(activate(sendCode)),
    saveClient: (id, client) => dispatch(saveClient(id, client)),
    setPath: () => dispatch(setPath(ownProps.clientId)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => {
    if (props.clientContainer.path) {
      return ([
        {
          path: `/learners/${props.clientContainer.path}`,
          storeAs: `${props.clientContainer.path}`,
        },
      ]);
    }
    return [];
  }),
)(ClientContainer);

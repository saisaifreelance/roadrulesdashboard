import { createSelector } from 'reselect';
import { dataToJS } from 'react-redux-firebase';

/**
 * Direct selector to the clientContainer state domain
 */
const selectClientContainerDomain = () => (state) => state.get('clientContainer');

/**
 * Other specific selectors
 */
const selectFirebase = () => (state) => state.get('firebase');

/**
 * Default selector used by ClientContainer
 */

const makeSelectClientContainer = () => createSelector(
  selectClientContainerDomain(),
  selectFirebase(),
  (substate, firebase) => {
    const clientContainer = substate.toJS();
    const data = dataToJS(firebase, `/learners/${clientContainer.path}`)
    return {
      ...clientContainer,
      data,
    };
  }
);

export default makeSelectClientContainer;
export {
  selectClientContainerDomain,
};

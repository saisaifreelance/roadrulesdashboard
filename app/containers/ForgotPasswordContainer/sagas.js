import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';

import {
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_FAILED,
  FORGOT_PASSWORD_SUCCEEDED,
} from './constants';
import {
  forgotPasswordFailed,
  forgotPasswordSucceeded,
} from './actions';

export function firebaseLoginRequestion(form) {
  return getFirebase().login(form)
    .then((some, respo) => {
      return some;
    });
}

export function* firebaseLogin(action) {
  try {
    const user = yield call(firebaseLoginRequestion.bind(undefined, action.form));
    yield put(loginSucceeded(user));
  } catch (e) {
    yield put(loginFailed(e));
  }
}

// Individual exports for testing
export function* forgotPasswordSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(FORGOT_PASSWORD, firebaseLogin);
}

// All sagas to be loaded
export default [
  forgotPasswordSaga,
];


import { fromJS } from 'immutable';
import forgotPasswordContainerReducer from '../reducer';

describe('forgotPasswordContainerReducer', () => {
  it('returns the initial state', () => {
    expect(forgotPasswordContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

import { createSelector } from 'reselect';

/**
 * Direct selector to the forgotPasswordContainer state domain
 */
const selectForgotPasswordContainerDomain = () => (state) => state.get('forgotPasswordContainer');

/**
 * Other specific selectors
 */
const selectForm = () => createSelector(
  selectForgotPasswordContainerDomain(),
  (substate) => {
    return {
      email: substate.get('email').toJS(),
      error: substate.get('error'),
    };
  }
);


/**
 * Default selector used by ForgotPasswordContainer
 */

const makeSelectForgotPasswordContainer = () => createSelector(
  selectForgotPasswordContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectForgotPasswordContainer;
export {
  selectForgotPasswordContainerDomain,
  selectForm,
};

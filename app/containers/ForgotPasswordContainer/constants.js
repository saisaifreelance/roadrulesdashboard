/*
 *
 * ForgotPasswordContainer constants
 *
 */

export const CHANGE_FIELD = 'app/ForgotPasswordContainer/CHANGE_FIELD';
export const FORGOT_PASSWORD = 'app/ForgotPasswordContainer/FORGOT_PASSWORD';
export const FORGOT_PASSWORD_SUCCEEDED = 'app/ForgotPasswordContainer/FORGOT_PASSWORD_SUCCEEDED';
export const FORGOT_PASSWORD_FAILED = 'app/ForgotPasswordContainer/FORGOT_PASSWORD_FAILED';

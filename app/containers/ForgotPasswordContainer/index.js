/*
 *
 * ForgotPasswordContainer
 *
 */

import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import { Link } from 'react-router';
import {createStructuredSelector} from 'reselect';
import makeSelectForgotPasswordContainer, {selectForm} from './selectors';
import {forgotPassword, changeField} from './actions';
import Input from '../../components/Input';

export class ForgotPasswordContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onSubmit(event) {
    event.preventDefault();
    this.props.forgotPassword(this.props.form);
  }
  render() {
    const error = this.props.form.error ? (
      <div className="palette-Red-500 text">
        {this.props.form.error}
      </div>
    ) : null
    return (
      <div className="l-block toggled" id="l-forget-password">
        <Helmet
          title="ForgotPasswordContainer"
          meta={[
            {name: 'description', content: 'Description of ForgotPasswordContainer'},
          ]}
        />
        <div className="lb-header palette-Orange bg">
          <i className="zmdi zmdi-account-circle"></i>
          Forgot Password?
        </div>

        <div className="lb-body">
          <p className="m-b-30">Lorem ipsum dolor fringilla enim feugiat commodo sed ac lacus.</p>
          {error}

          <Input
            name="email"
            type="email"
            label="Email Address"
            placeholder="Email Address"
            value={this.props.form.email.value}
            onChange={this.props.changeField}
            error={this.props.form.email.error}
          />

          <button className="btn palette-Orange bg" onClick={this.onSubmit}>Create Account</button>

          <div className="m-t-30">
            <Link to="/login" data-block="#l-login" data-bg="orange" className="palette-Orange text d-block m-b-5" href="">Already have
              an account?</Link>
            <Link to="/register" data-block="#l-register" data-bg="blue" href="" className="palette-Orange text">Create an account</Link>
          </div>
        </div>
      </div>
    );
  }
}

ForgotPasswordContainer.propTypes = {
  changeField: React.PropTypes.func.isRequired,
  forgotPassword: React.PropTypes.func.isRequired,
  form: React.PropTypes.shape({
    email: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    error: React.PropTypes.string.isRequired,
  }),
};

const mapStateToProps = createStructuredSelector({
  ForgotPasswordContainer: makeSelectForgotPasswordContainer(),
  form: selectForm(),
});

function mapDispatchToProps(dispatch) {
  return {
    forgotPassword: (form) => {
      return dispatch(forgotPassword(form.email.value));
    },
    changeField: (field, value) => dispatch(changeField(field, value)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordContainer);

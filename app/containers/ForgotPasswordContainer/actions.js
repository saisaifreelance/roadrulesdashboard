/*
 *
 * ForgotPasswordContainer actions
 *
 */

import {
  CHANGE_FIELD, FORGOT_PASSWORD, FORGOT_PASSWORD_SUCCEEDED, FORGOT_PASSWORD_FAILED,
} from './constants';

export function changeField(field, value) {
  return {
    type: CHANGE_FIELD,
    field,
    value,
  };
}

export function forgotPassword(form) {
  return {
    type: FORGOT_PASSWORD,
    form
  };
}

export function forgotPasswordSucceeded() {
  return {
    type: FORGOT_PASSWORD_SUCCEEDED,
  };
}

export function forgotPasswordFailed(error) {
  return {
    type: FORGOT_PASSWORD_FAILED,
    error,
  };
}

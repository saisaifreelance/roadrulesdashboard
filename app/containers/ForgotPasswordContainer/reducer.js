/*
 *
 * ForgotPasswordContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_FIELD, FORGOT_PASSWORD, FORGOT_PASSWORD_FAILED, FORGOT_PASSWORD_SUCCEEDED,
} from './constants';

const initialState = fromJS({
  email: {
    value: '',
    error: '',
  },
  error: '',
});

function setError(state, action) {
  switch (action.error.code) {
    case 'auth/argument-error':
      return state.set('error', action.error.message);
    case 'auth/user-not-found':
      return state.set('error', action.error.message);
    case 'auth/invalid-email':
      return state.updateIn(['email', 'error'], () => action.error.message);
    default:
      break;
  }
  return state;
}

function forgotPasswordContainerReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_FIELD:
      return state.updateIn([action.field, 'value'], () => action.value).updateIn([action.field, 'error'], () => '');
    case FORGOT_PASSWORD_FAILED:
      return setError(state, action);
    default:
      return state;
  }
}

export default forgotPasswordContainerReducer;

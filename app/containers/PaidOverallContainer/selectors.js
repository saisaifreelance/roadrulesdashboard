import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS} from 'react-redux-firebase';

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
};

/**
 * Direct selector to the paidOverallContainer state domain
 */
const selectPaidOverallContainerDomain = () => (state) => state.get('paidOverallContainer');

/**
 * Other specific selectors
 */

const makeSelectFirebase = () => (state) => state.get('firebase');


/**
 * Default selector used by PaidOverallContainer
 */

const makeSelectPaidOverallContainer = () => createSelector(
  selectPaidOverallContainerDomain(),
  (substate) => substate.toJS()
);

const makeSelectOverallSalesData = () => createSelector(
  makeSelectFirebase(),
  makeSelectOverallSalesStatus(),
  (firebase, isFetching) => {
    if (isFetching) {
      return null;
    }
    const statsPaidOverall = dataToJS(firebase, 'meta_learners');

    console.log('>>STATA PAID OVERALL');
    console.log(statsPaidOverall);

    if (!statsPaidOverall || typeof statsPaidOverall !== 'object') {
      return null;
    }

    const data = [
      { data: statsPaidOverall.count, color: '#00FF00', label: 'Total Downloaded' },
      { data: statsPaidOverall.paid_learner, color: '#03A9F4', label: 'Total Paid For Learner' },
      { data: statsPaidOverall.paid_driver, color: '#F44336', label: 'Total Paid For Driver' },
    ];
    console.log(data);
    return data;
  }
);

const makeSelectOverallSalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'meta_learners')
);

const makeSelectOptions = () => createSelector(
  () => ({
    series: {
      pie: {
        show: true,
        stroke: {
          width: 2,
        },
      },
    },
    legend: {
      container: '.flc-pie',
      backgroundOpacity: 0.5,
      noColumns: 0,
      backgroundColor: 'white',
      lineWidth: 0,
    },
    grid: {
      hoverable: true,
      clickable: true,
    },
    tooltip: true,
    tooltip: {
      show: true,
      content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
      shifts: {
        x: 20,
        y: 0,
      },
      defaultTheme: false,
      cssClass: 'flot-tooltip',
    },
  })
);

export default makeSelectPaidOverallContainer;
export {
  selectPaidOverallContainerDomain,
  makeSelectOverallSalesData,
  makeSelectOverallSalesStatus,
  makeSelectOptions,
};

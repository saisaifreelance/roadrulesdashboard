
import { fromJS } from 'immutable';
import paidOverallContainerReducer from '../reducer';

describe('paidOverallContainerReducer', () => {
  it('returns the initial state', () => {
    expect(paidOverallContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

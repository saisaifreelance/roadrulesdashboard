
import { fromJS } from 'immutable';
import weeklySalesContainerReducer from '../reducer';

describe('weeklySalesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(weeklySalesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

/*
 *
 * WeeklySalesContainer constants
 *
 */

export const SET_TARGET = 'app/WeeklySalesContainer/SET_TARGET';
export const SET_TARGET_SUCCESS = 'app/WeeklySalesContainer/SET_TARGET_SUCCESS';
export const SET_TARGET_FAIL = 'app/WeeklySalesContainer/SET_TARGET_FAIL';

export const SET_INVERT = 'app/WeeklySalesContainer/SET_INVERT';
export const SET_SHOWN = 'app/WeeklySalesContainer/SET_SHOWN';

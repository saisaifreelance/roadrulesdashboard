/*
 *
 * WeeklySalesContainer
 *
 */

import React, { PropTypes } from 'react';
import {compose} from 'redux';
import { connect } from 'react-redux';
import {firebaseConnect} from 'react-redux-firebase';
import { createStructuredSelector } from 'reselect';
import { setTargetAction, setInvertAction, setShownAction } from './actions';
import makeSelectWeeklySalesContainer, { makeSelectWeeklySalesData, makeSelectWeeklySalesStatus, makeSelectOptions, makeSelectWeeklyTargets } from './selectors';
import ChartDownloads from '../../components/ChartDownloads';

export class WeeklySalesContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <ChartDownloads
        title="Weekly downloads"
        description="Weekly downloads compared with sales"
        data={this.props.data}
        isFetching={this.props.isFetching}
        options={this.props.options}
        targets={this.props.targets}
        setTarget={this.props.setTarget}
        invert={this.props.weeklySalesContainer.invert}
        setInvert={this.props.setInvert}
        setShown={this.props.setShown}
        shown={this.props.weeklySalesContainer.shown}
      />
    );
  }
}

WeeklySalesContainer.propTypes = {
  weeklySalesContainer: PropTypes.shape({
    shown: PropTypes.object.isRequired,
    targets: PropTypes.object.isRequired,
    invert: PropTypes.bool.isRequired,
  }).isRequired,
  data: PropTypes.array,
  isFetching: PropTypes.string,
  options: PropTypes.object.isRequired,
  setTarget: PropTypes.func.isRequired,
  setInvert: PropTypes.func.isRequired,
  setShown: PropTypes.func.isRequired,
  targets: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  weeklySalesContainer: makeSelectWeeklySalesContainer(),
  data: makeSelectWeeklySalesData(),
  isFetching: makeSelectWeeklySalesStatus(),
  options: makeSelectOptions(),
  targets: makeSelectWeeklyTargets(),
});

function mapDispatchToProps(dispatch) {
  return {
    setTarget: (targets) => dispatch(setTargetAction(targets)),
    setInvert: (invert) => dispatch(setInvertAction(invert)),
    setShown: (shown) => dispatch(setShownAction(shown)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect(() => ([
    {
      path: '/stats_weekly',
      queryParams: ['orderByKey', 'limitToLast=12'],
    },
    {
      path: '/targets/weekly',
    },
  ]))
)(WeeklySalesContainer);

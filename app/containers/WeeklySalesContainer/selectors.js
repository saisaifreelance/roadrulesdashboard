import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS} from 'react-redux-firebase';

const options = {
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
    labelFormatter: (label, series) => {
      return label;
    },
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  grid: {
    borderWidth: 0,
    labelMargin: 10,
    hoverable: true,
    clickable: true,
    mouseActiveRadius: 6,
    show: true,
    markings: [
      {color: '#00FF00', lineWidth: 10, xaxis: {from: 0, to: 12}, yaxis: {from: 2500, to: 3000}},
    ],
  },
}

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

const getRandomStats = () => ({
  count: Math.floor(Math.random() * 4000),
  paid_learner: Math.floor(Math.random() * 2000),
  paid_driver: Math.floor(Math.random() * 1000),
})

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
};

/**
 * Direct selector to the weeklySalesContainer state domain
 */
const selectWeeklySalesContainerDomain = () => (state) => state.get('weeklySalesContainer');

/**
 * Other specific selectors
 */
const makeSelectFirebase = () => (state) => state.get('firebase');

/**
 * Default selector used by WeeklySalesContainer
 */

const getLast4Weeks = () => {
  const date = new Date();
  const diff = date.getDate() - date.getDay();
  date.setDate(diff);
  date.setHours(0, 0, 0, 0);
  const weeks = [date.getTime() + 7200000];
  for (let i = 0; i < 3; i += 1) {
    date.setDate(date.getDate() - 7);
    weeks.push(date.getTime() + 7200000);
  }
  return weeks;
}

const makeSelectWeeklySalesContainer = () => createSelector(
  selectWeeklySalesContainerDomain(),
  (substate) => substate.toJS()
);


const makeSelectWeeklySalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'stats_weekly')
);

const makeSelectWeeklyTargets = () => createSelector(
  makeSelectWeeklySalesContainer(),
  makeSelectFirebase(),
  (WeeklySalesContainer, firebase) => {
    let targets = dataToJS(firebase, '/targets/weekly');
    if (isEmpty(targets) || !isLoaded(targets) || !Object.keys(targets).length) {
      targets = {};
    }
    const requiredTargets = ['downloads', 'paidLearner', 'paidDriver'];
    for (let i = 0; i < requiredTargets.length; i += 1) {
      if (!targets[requiredTargets[i]]) {
        targets[requiredTargets[i]] = 0;
      }
    }
    targets = {...targets, ...WeeklySalesContainer.targets};
    return Object.keys(targets).reduce((accumulator, key) => ({...accumulator, [key]: parseInt(targets[key], 10)}), {});
  }
);

const makeSelectOptions = () => createSelector(
  getLast4Weeks,
  makeSelectWeeklySalesContainer(),
  makeSelectWeeklyTargets(),
  (last4Weeks, weeklySalesContainer, weeklyTargets) => {
    const ticks = last4Weeks.reverse().map((week, index) => [index, (new Date(week)).toDateString()])
    const xaxis = { ...options.xaxis, ticks };
    const markings = (axes) => {
      const { ymax, ymin, yaxis: { box: { height } } } = axes;
      const lineWidth = 2
      const scaledLineWidth = (ymax / height) * lineWidth;
      if (weeklySalesContainer.invert) {
        return { ...options, xaxis };
      }
      return [
        {
          color: '#ababab',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: weeklyTargets.downloads, to: weeklyTargets.downloads + scaledLineWidth}
        },
        {
          color: '#d5c32a',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: weeklyTargets.paidLearner, to: weeklyTargets.paidLearner + scaledLineWidth}
        },
        {
          color: '#304b24',
          lineWidth: 2,
          xaxis: {from: 0, to: 12},
          yaxis: {from: weeklyTargets.paidDriver, to: weeklyTargets.paidDriver + scaledLineWidth}
        },
      ];
    }
    const grid = {...options.grid, markings}
    return {...options, xaxis, grid};
  }
);

const makeSelectWeeklySalesData = () => createSelector(
  makeSelectWeeklySalesContainer(),
  makeSelectFirebase(),
  makeSelectWeeklySalesStatus(),
  makeSelectWeeklyTargets(),
  (weeklySalesContainer, firebase, isFetching, weeklyTargets) => {
    if (isFetching) {
      return null;
    }
    const statsWeekly = dataToJS(firebase, 'stats_weekly');

    const defaultData = [];
    if (weeklySalesContainer.shown.downloads) {
      defaultData.push({
        data: [],
        lines: { show: true, fill: 0.98 },
        label: 'Total Downloads',
        stack: true,
        color: '#e3e3e3',
      });
    }
    if (weeklySalesContainer.shown.paidLearner) {
      defaultData.push({
        data: [],
        lines: { show: true, fill: 0.98 },
        label: 'Total Paid for learner',
        stack: true,
        color: '#f1dd2c',
      });
    }
    if (weeklySalesContainer.shown.paidDriver) {
      defaultData.push({
        data: [],
        lines: { show: true, fill: 0.98 },
        label: 'Total Paid for driver',
        stack: true,
        color: '#8BC34A',
      });
    }

    return getLast4Weeks().reverse().reduce((weeklySalesData, week, index) => {
      const dataForWeek = statsWeekly[week] ? statsWeekly[week] : defaultPeriodStats;

      let ind = 0;
      if (weeklySalesContainer.invert) {
        let downloads = 0;
        let paidLearner = 0;
        let paidDriver = 0;
        if (weeklyTargets.downloads) {
          downloads = parseInt(weeklyTargets.downloads, 10);
          downloads = isNaN(downloads) ? 0 : weeklyTargets.downloads - dataForWeek.count;
        }
        if (weeklyTargets.paidLearner) {
          paidLearner = parseInt(weeklyTargets.paidLearner, 10);
          paidLearner = isNaN(paidLearner) ? 0 : weeklyTargets.paidLearner - dataForWeek.paid_learner;
        }
        if (weeklyTargets.paidDriver) {
          paidDriver = parseInt(weeklyTargets.paidDriver, 10);
          paidDriver = isNaN(paidDriver) ? 0 : weeklyTargets.paidDriver - dataForWeek.paid_driver;
        }
        if (weeklySalesContainer.shown.downloads) {
          weeklySalesData[ind].data.push([
            index,
            downloads,
          ])
          ind += 1;
        }
        if (weeklySalesContainer.shown.paidLearner) {
          weeklySalesData[ind].data.push([
            index,
            paidLearner,
          ]);
          ind += 1;
        }
        if (weeklySalesContainer.shown.paidDriver) {
          weeklySalesData[ind].data.push([
            index,
            paidDriver,
          ]);
          ind += 1;
        }
        return weeklySalesData;
      }

      if (weeklySalesContainer.shown.downloads) {
        weeklySalesData[ind].data.push([
          index,
          dataForWeek.count,
        ]);
        ind += 1;
      }
      if (weeklySalesContainer.shown.paidLearner) {
        weeklySalesData[ind].data.push([
          index,
          dataForWeek.paid_learner,
        ]);
        ind += 1;
      }
      if (weeklySalesContainer.shown.paidDriver) {
        weeklySalesData[ind].data.push([
          index,
          dataForWeek.paid_driver,
        ]);
        ind += 1;
      }
      return weeklySalesData;
    }, defaultData);
  }
);

export default makeSelectWeeklySalesContainer;
export {
  makeSelectWeeklySalesContainer,
  makeSelectWeeklySalesStatus,
  makeSelectWeeklySalesData,
  makeSelectOptions,
  makeSelectWeeklyTargets,
};

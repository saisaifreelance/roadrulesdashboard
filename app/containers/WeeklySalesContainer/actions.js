/*
 *
 * WeeklySalesContainer actions
 *
 */

import {
  SET_TARGET,
  SET_TARGET_SUCCESS,
  SET_TARGET_FAIL,
  SET_INVERT,
  SET_SHOWN,
} from './constants';

export function setTargetAction(targets) {
  const updates = Object.keys(targets).reduce((accumulator, key) => ({ ...accumulator, [`/targets/weekly/${key}`]: targets[key] }), {})
  return {
    type: SET_TARGET,
    targets,
    updates,
  };
}

export function setTargetSuccessAction(targets, updates) {
  return {
    type: SET_TARGET_SUCCESS,
    targets,
    updates,
  };
}

export function setTargetFailAction(targets, updates, error) {
  return {
    type: SET_TARGET_FAIL,
    targets,
    updates,
    error,
  };
}

export function setInvertAction(invert) {
  return {
    type: SET_INVERT,
    invert,
  };
}

export function setShownAction(shown) {
  return {
    type: SET_SHOWN,
    shown,
  };
}

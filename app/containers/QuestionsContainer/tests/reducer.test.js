
import { fromJS } from 'immutable';
import questionsContainerReducer from '../reducer';

describe('questionsContainerReducer', () => {
  it('returns the initial state', () => {
    expect(questionsContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

import { createSelector } from 'reselect';
import schema from './schema';

/**
 * Direct selector to the questionsContainer state domain
 */
const selectQuestionsContainerDomain = (state) => state.get('questionsContainer');

/**
 * Other specific selectors
 */
const selectTable = createSelector(
  selectQuestionsContainerDomain,
  (questionsContainerDomain) => {
    const questionsContainer = questionsContainerDomain.toJS();
    const { metaData, data, isFetchingQuestions, isFetchingMetaQuestions, startAt, endAt, sorted, direction, pageSize, start } = questionsContainer;
    const page = Math.floor(start / pageSize) + 1
    return {
      isFetching: isFetchingQuestions || isFetchingMetaQuestions,
      empty: !data || !data.length,
      count: metaData ? metaData.count : 0,
      data: data ? data.slice(0, pageSize) : data,
      page,
      startAt,
      endAt,
      schema,
      sorted,
      direction,
      pageSize,
      start,
    };
  }
);

const selectQueries = createSelector(
  selectTable,
  (table) => {
    const queryParams = [`orderByChild=${table.sorted}`];

    if (!table.endAt && !table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
    }

    if (table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
      queryParams.push(`startAt=${table.startAt}`);
    }

    if (table.endAt) {
      queryParams.push(`limitToLast=${table.pageSize + 1}`);
      queryParams.push(`endAt=${table.endAt}`);
    }
    return ([
      {
        path: '/questions',
        queryParams,
      },
      {
        path: '/meta_questions',
      },
    ]);
  }
);


/**
 * Default selector used by QuestionsContainer
 */

const makeSelectQuestionsContainer = () => createSelector(
  selectQuestionsContainerDomain,
  selectTable,
  selectQueries,
  (substate, table, queries) => ({
    table,
    queries,
  })
);

export default makeSelectQuestionsContainer;
export {
  selectQuestionsContainerDomain,
};

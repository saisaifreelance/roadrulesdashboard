/*
 *
 * QuestionsContainer
 *
 */

import React, { PropTypes } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import {firebaseConnect } from 'react-redux-firebase';
import Helmet from 'react-helmet';
import Card from 'react-md/lib/Cards/Card';
import makeSelectQuestionsContainer from './selectors';
import { sort, paginate } from './actions';
import DataTableComponent from '../../components/DataTableComponent';
import CardHeader from '../../components/CardHeader';
import TableActions from '../../components/TableActions';
import EditRowsContainer from '../../containers/EditRowsContainer';
import '../../../node_modules/react-md/dist/react-md.amber-blue.min.css';
import { userIsAuthenticated } from '../../auth';

export class QuestionsContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      selectedRows: {},
      count: 0,
      dialogVisible: false,
    };
    this.handleRowToggle = this.handleRowToggle.bind(this);
    this.openAddRowDialog = this.openAddRowDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }
  handleRowToggle(row, toggled, count) {
    if (row === -1) {
      if (toggled) {
        const selectedRows = {};
        for (let i = 0; i < this.props.table.pageSize; i += 1) {
          selectedRows[`question_${this.props.table.data[i].id}`] = true;
        }
        return this.setState({ selectedRows });
      }

      const selectedRows = {};
      return this.setState({ selectedRows });
    }

    const { selectedRows: oldSelectedRows } = this.state;
    const item = this.props.table.data[row];
    if (toggled) {
      const selectedRows = Object.assign({}, oldSelectedRows, { [`question_${item.id}`]: true });
      return this.setState({ selectedRows });
    }
    const selectedRows = Object.assign({}, oldSelectedRows);
    delete selectedRows[`question_${item.id}`];
    return this.setState({ selectedRows });
  }
  reset() {

  }
  removeSelected() {}
  openAddRowDialog() {
    this.setState({ dialogVisible: true });
  }
  closeDialog() {
    this.setState({ dialogVisible: false });
  }
  render() {
    return (
      <section id="content">
        <Helmet
          title="Questions"
          meta={[
            { name: 'description', content: 'Description of Questions' },
          ]}
        />
        <div className="container">
          <div className="c-header">
            <h2>Questions Data Table</h2>
          </div>
          <Card tableCard>
            <TableActions
              title="Questions"
              mobile={this.props.mobile}
              count={Object.keys(this.state.selectedRows).length}
              reset={this.reset}
              removeSelected={this.removeSelected}
              openAddRowDialog={this.openAddRowDialog}
            />
            <DataTableComponent onRowToggle={this.handleRowToggle} {...this.props.table} sort={this.props.sort} paginate={this.props.paginate} />
            <EditRowsContainer
              schema={this.props.table.schema}
              root="/questions"
              keys={this.state.selectedRows}
              closeDialog={this.closeDialog}
              visible={this.state.dialogVisible}
              mobile={this.props.mobile}
            />
          </Card>
        </div>
      </section>
    );
  }
}

QuestionsContainer.propTypes = {
  queries: React.PropTypes.array.isRequired,
  table: React.PropTypes.shape({
    data: React.PropTypes.Array,
    loaded: React.PropTypes.bool,
    empty: React.PropTypes.bool,
    schema: React.PropTypes.Array,
    sorted: React.PropTypes.string,
    direction: React.PropTypes.oneOf(['asc', 'desc']),
    count: React.PropTypes.number,
    pageSize: React.PropTypes.number,
    page: React.PropTypes.number,
    start: React.PropTypes.number,
    startAt: React.PropTypes.number,
    endAt: React.PropTypes.string,
  }),
  sort: React.PropTypes.func.isRequired,
  paginate: React.PropTypes.func.isRequired,
  mobile: React.PropTypes.bool,
};

QuestionsContainer.defaultProps = {
  mobile: false,
};

const mapStateToProps = makeSelectQuestionsContainer();

function mapDispatchToProps(dispatch, props) {
  return {
    sort: (sorted, direction) => dispatch(sort(sorted, direction)),
    paginate: (start, pageSize, currentPage) => dispatch(paginate(start, pageSize, currentPage)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => props.queries),
  userIsAuthenticated
)(QuestionsContainer);

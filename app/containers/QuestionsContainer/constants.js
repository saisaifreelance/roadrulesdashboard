/*
 *
 * QuestionsContainer constants
 *
 */

export const PAGINATE = 'app/QuestionsContainer/PAGINATE';
export const PAGINATE_SUCCESS = 'app/QuestionsContainer/PAGINATE_SUCCESS';
export const PAGINATE_FAIL = 'app/QuestionsContainer/PAGINATE_FAIL';
export const SORT = 'app/QuestionsContainer/SORT';

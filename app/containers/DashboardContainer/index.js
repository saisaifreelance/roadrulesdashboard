/*
 *
 * DashboardContainer
 *
 */

import React, {PropTypes} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {createStructuredSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty} from 'react-redux-firebase';
import Masonry from 'react-masonry-component';
import '../../../node_modules/react-md/dist/react-md.amber-blue.min.css';
import makeSelectDashboardContainer from './selectors';
import {userIsAdmin, userIsAuthenticated} from '../../auth';
import MonthlySalesContainer from '../MonthlySalesContainer';
import WeeklySalesContainer from '../WeeklySalesContainer';
import DailySalesContainer from '../DailySalesContainer';
import PaidOverallContainer from '../PaidOverallContainer';
import PaidMonthContainer from '../PaidMonthContainer';
import ChartDynamic from '../../components/ChartDynamic';
import ChartSpark from '../../components/ChartSpark';
import DataGrid from '../../components/DataGrid';

export class DashboardContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
  }

  render() {
    return (
      <section id="content">
        <Helmet
          title="Road Rules|Home"
          meta={[
            {name: 'description', content: 'Home page of the container'},
          ]}
        />
        <div className="container">
          <MonthlySalesContainer />
          <Masonry
            id="c-grid"
            className={'clearfix'} // default ''
            options={{
              itemSelector: '.grid-item',
              columnWidth: '.grid-sizer',
              percentPosition: true,
            }} // default {}
            disableImagesLoaded={false} // default false
            updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
          >
            <div className="grid-sizer"></div>
            <div className="grid-item">
              <WeeklySalesContainer />
            </div>
            <div className="grid-item">
              <DailySalesContainer />
            </div>

            <div className="grid-item">
              <PaidOverallContainer />
            </div>

            <div className="grid-item">
              <PaidMonthContainer />
            </div>

            <div className="grid-item">
              <ChartDynamic />
            </div>

            <div className="grid-item">
              <ChartSpark />
            </div>
          </Masonry>
        </div>
      </section>
    );
  }
}

DashboardContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  DashboardContainer: makeSelectDashboardContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default compose(
  firebaseConnect((props, firebase) => {
    return ([]);
  }),
  connect(mapStateToProps, mapDispatchToProps),
  userIsAuthenticated,
)(DashboardContainer);

import { take, call, put, select } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';
import { getFirebase } from 'react-redux-firebase';

import {
  ADD_ITEM,
  SAVE_ITEM,
} from './constants';

import {
  saveItemSuccessAction,
  saveItemFailAction,
  addItemSuccessAction,
  addItemFailAction,
} from './actions';

function saveToFirebase(updates) {
  return getFirebase().ref().update(updates).then((error) => {
    if (!error) {
      return error;
    }
    throw new Error(error);
  });
}

function* saveItem(action) {
  try {
    const error = yield call(saveToFirebase.bind(undefined, action.data));
    if (error) {
      throw new Error(error);
    }
    yield put(saveItemSuccessAction(action.paths, action.data));
  } catch (e) {
    yield put(saveItemFailAction(action.paths, action.data, e));
  }
}

function* addItem(action) {
  try {
    const key = getFirebase().ref(action.root).push().key;
    yield put(addItemSuccessAction(`${action.root}/${key}`, key));
  } catch (e) {
    yield put(addItemFailAction(e));
  }
}

// Individual exports for testing
export function* saveItemSaga() {
  yield* takeLatest(SAVE_ITEM, saveItem);
}

export function* addItemSaga() {
  yield* takeLatest(ADD_ITEM, addItem);
}

// All sagas to be loaded
export default [
  saveItemSaga,
  addItemSaga,
];


import { fromJS } from 'immutable';
import editRowsContainerReducer from '../reducer';

describe('editRowsContainerReducer', () => {
  it('returns the initial state', () => {
    expect(editRowsContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

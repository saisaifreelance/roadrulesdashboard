/*
 *
 * EditRowsContainer actions
 *
 */

import {
  UPLOAD_ITEM_IMAGE,
  UPLOAD_ITEM_IMAGE_SUCCESS,
  UPLOAD_ITEM_IMAGE_FAIL,
  EDIT_ITEM,
  EDIT_ITEM_ERROR,
  SAVE_ITEM,
  SAVE_ITEM_SUCCESS,
  SAVE_ITEM_FAIL,
  SET_PATH,
  START_ACTION,
  SET_ACTION,
  CLEAR_NOTIFICATION,
  ADD_ITEM,
  ADD_ITEM_SUCCESS,
  ADD_ITEM_FAIL,
} from './constants';

export function startAction(action) {
  return {
    type: START_ACTION,
    ...action,
  };
}

export function setAction(action) {
  return {
    type: SET_ACTION,
    ...action,
  };
}

export function setPathAction(root, keys) {
  return {
    type: SET_PATH,
    root,
    keys,
  };
}

export function addItemAction(root) {
  return {
    type: ADD_ITEM,
    root,
  };
}

export function addItemSuccessAction(path, key) {
  return {
    type: ADD_ITEM_SUCCESS,
    path,
    key,
  };
}

export function addItemFailAction(error) {
  return {
    type: ADD_ITEM_FAIL,
    error,
  };
}

// todo add path to all the actions
export function editItemAction(path, fieldName, newValue, newActiveIndex, event) {
  return {
    type: EDIT_ITEM,
    path,
    fieldName,
    newValue,
  };
}

export function editItemErrorAction(errors) {
  return {
    type: EDIT_ITEM_ERROR,
    errors,
  };
}
export function uploadItemImageAction(path, schema, file, blob) {
  const { name, size, type, lastModifiedDate } = file;
  return {
    type: UPLOAD_ITEM_IMAGE,
  };
}

export function saveItemAction(schema, pathsData) {
  const errors = {};
  const data = {};
  const deltaPaths = {};

  const paths = Object.keys(pathsData);
  for (let i = 0; i < paths.length; i += 1) {
    const path = paths[i];
    const pathData = pathsData[path];
    const pathFields = Object.keys(pathData.form);
    if (pathFields.length) {
      for (let j = 0; j < pathFields.length; j += 1) {
        const pathSchema = schema.find((field) => pathFields[j] === field.key);
        if (pathSchema.required && !( pathData.form[pathFields[j]] || pathData.data[pathFields[j]])) {
          errors[path] = errors[path] ? { ...errors[path], [pathFields[j]]: `${pathSchema.title} field is required` } : { [pathFields[j]]: `${pathSchema.title} field is required` };
        }
        else if (typeof pathSchema.validate !== 'function') {
          data[`${path}/${pathFields[j]}`] = pathData.form[pathFields[j]];
          deltaPaths[path] = true;
        }
        else {
          const error = pathSchema.validate(pathData.form[pathFields[j]]);
          if (error) {
            errors[path] = errors[path] ? { ...errors[path], [pathFields[j]]: error } : { [pathFields[j]]: error };
          }
          else {
            data[`${path}/${pathFields[j]}`] = pathData.form[pathFields[j]];
            deltaPaths[path] = true;
          }
        }
      }
    }
  }
  if (Object.keys(errors).length) {
    return editItemErrorAction(errors);
  }
  return {
    type: SAVE_ITEM,
    data,
    paths: deltaPaths,
  };
}

export function saveItemSuccessAction(path, form) {
  return {
    type: SAVE_ITEM_SUCCESS,
    path,
    form,
  };
}

export function saveItemFailAction(path, error) {
  return {
    type: SAVE_ITEM_FAIL,
    path,
    error,
  };
}

export function clearNotificationAction(notification) {
  return {
    type: CLEAR_NOTIFICATION,
    notification,
  };
}

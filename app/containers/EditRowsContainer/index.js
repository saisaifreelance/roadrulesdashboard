/*
 *
 * EditRowsContainer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import Button from 'react-md/lib/Buttons/Button';
import Dialog from 'react-md/lib/Dialogs';
import Toolbar from 'react-md/lib/Toolbars';
import makeSelectEditRowsContainer from './selectors';
import { setPathAction, editItemAction, editItemErrorAction, saveItemAction, clearNotificationAction, addItemAction, uploadItemImageAction } from './actions';
import FormRow from '../../components/FormRow';
import { notify } from '../../utils/notifications';

export class EditRowsContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.closeDialog = this.closeDialog.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    const keys = Object.keys(this.props.keys).join('');
    if (keys) {
      this.props.setPath();
    }
  }
  componentDidUpdate(prevProps) {
    const oldKeys = Object.keys(prevProps.keys).join('');
    const newKeys = Object.keys(this.props.keys).join('');
    if (oldKeys !== newKeys) {
      this.props.setPath();
    }

    const oldNotifications = Object.keys(prevProps.EditRowsContainer.notifications);
    const newNotifications = Object.keys(this.props.EditRowsContainer.notifications);
    if (newNotifications.length && newNotifications.join('') !== oldNotifications.join('')) {
      notify(this.props.EditRowsContainer.notifications[newNotifications[0]], 'inverse');
      this.props.clearNotification(newNotifications[0]);
    }

    if (this.props.visible && !prevProps.visible && !newKeys) {
      this.props.addItem();
    }
  }
  closeDialog() {
    this.props.closeDialog();
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.saveItem(this.props.EditRowsContainer.pathsData);
  }
  render() {
    const submit = <Button type="submit" label="Submit" flat />;
    let content = null
    if (!this.props.keys || typeof this.props.keys !== 'object') {
      content = <h2>No keys selected</h2>;
    }
    const paths = Object.keys(this.props.EditRowsContainer.paths);
    if (!content && !paths.length) {
      content = <h2>No paths set</h2>;
    }
    if (this.props.visible && !content && paths.length) {
      content = paths.map((path, index) => (
        <FormRow
          key={path}
          path={path}
          index={index}
          schema={this.props.schema}
          editItem={this.props.editItem}
          editItemError={this.props.editItemError}
          saveItem={this.props.saveItem}
          pathData={this.props.EditRowsContainer.pathsData[path]}
        />
      ));
    }
    return (
      <Dialog
        id="add-row-dialog"
        aria-label="Edit clients data"
        visible={this.props.visible}
        onHide={this.closeDialog}
        fullPage
      >
        <CSSTransitionGroup
          transitionName="md-cross-fade"
          transitionEnterTimeout={300}
          transitionLeave={false}
          component="form"
          onSubmit={this.handleSubmit}
          className="md-text-container md-toolbar-relative"
          id="new-nutrition-row-form"
        >
          <Toolbar
            nav={<Button icon onClick={this.closeDialog}>arrow_back</Button>}
            title="Create new row"
            fixed
            colored
            actions={submit}
          />
          {content}
        </CSSTransitionGroup>
        <Button floating fixed onClick={this.addGroup} primary>add</Button>
      </Dialog>
    );
  }
}

EditRowsContainer.propTypes = {
  schema: PropTypes.array.isRequired,
  root: PropTypes.string.isRequired,
  keys: PropTypes.object.isRequired,
  visible: React.PropTypes.bool.isRequired,
  closeDialog: React.PropTypes.func.isRequired,
  clearNotification: React.PropTypes.func.isRequired,

  EditRowsContainer: PropTypes.shape({
    notifications: PropTypes.object.isRequired,
    paths: PropTypes.object.isRequired,
    pathsData: PropTypes.objectOf(
      PropTypes.shape({
        isFetching: PropTypes.string,
        path: PropTypes.string,
        data: PropTypes.object,
        dataErrors: PropTypes.object,
        form: PropTypes.object,
        formErrors: PropTypes.object,
      })
    ),
  }).isRequired,

  addItem: PropTypes.func.isRequired,
  editItem: PropTypes.func.isRequired,
  editItemError: PropTypes.func.isRequired,
  saveItem: PropTypes.func.isRequired,
  setPath: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  EditRowsContainer: makeSelectEditRowsContainer(),
});

function mapDispatchToProps(dispatch, props) {
  return {
    setPath: () => {
      return dispatch(setPathAction(props.root, props.keys));
    },
    uploadItemImage: (...args) => dispatch(uploadItemImageAction(...args)),
    addItem: () => dispatch(addItemAction(props.root)),
    editItem: (...args) => dispatch(editItemAction(...args)),
    editItemError: (errors) => dispatch(editItemErrorAction(errors)),
    saveItem: (pathsData) => dispatch(saveItemAction(props.schema, pathsData)),
    clearNotification: (notification) => dispatch(clearNotificationAction(notification)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps, null, { withRef: true })(EditRowsContainer);

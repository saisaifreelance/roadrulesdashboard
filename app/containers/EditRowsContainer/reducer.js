/*
 *
 * EditRowsContainer reducer
 *
 */

import { fromJS } from 'immutable';
import { actionTypes } from 'react-redux-firebase';
import uuid from 'uuid/v4';
import {
  ADD_ITEM,
  ADD_ITEM_SUCCESS,
  ADD_ITEM_FAIL,
  LOAD_ITEM_IMAGE,
  LOAD_ITEM_IMAGE_SUCCESS,
  LOAD_ITEM_IMAGE_FAIL,
  UPLOAD_ITEM_IMAGE,
  UPLOAD_ITEM_IMAGE_SUCCESS,
  UPLOAD_ITEM_IMAGE_FAIL,
  EDIT_ITEM,
  EDIT_ITEM_ERROR,
  SAVE_ITEM,
  SAVE_ITEM_SUCCESS,
  SAVE_ITEM_FAIL,
  SET_PATH,
  START_ACTION,
  SET_ACTION,
  CLEAR_NOTIFICATION,
} from './constants';

const initialPathState = fromJS({
  isFetching: null,
  path: null,
  data: null,
  dataErrors: {},
  form: {},
  formErrors: {},
});
const initialState = fromJS({
  notifications: {},
  paths: {},
  pathsData: {},
});

function editRowsContainerReducer(state = initialState, action) {
  switch (action.type) {
    case SET_PATH: {
      const paths = Object
        .keys(action.keys)
        .reduce((accumulator, key) => ({ ...accumulator, [`${action.root}/${key}`]: true }), {});
      return state
        .set('paths', fromJS(paths))
        .set('pathsData', initialState.get('pathsData'));
    }
    case actionTypes.START: {
      const paths = state.get('paths').toJS();
      if (!paths[action.path]) {
        return state;
      }

      const pathData = state.getIn(['pathsData', action.path]);
      if (pathData) {
        return state
          .setIn(['pathsData', action.path, 'data'], initialPathState.get('data'))
          .setIn(['pathsData', action.path, 'dataErrors'], initialPathState.get('dataErrors'))
          .setIn(['pathsData', action.path, 'isFetching'], 'Fetching data...');
      }

      return state
        .setIn(['pathsData', action.path], initialPathState)
        .setIn(['pathsData', action.path, 'isFetching'], 'Fetching data...');
    }
    case ADD_ITEM_SUCCESS: {
      return state
        .setIn(['paths', action.path], true)
        .setIn(['pathsData', action.path], initialPathState)
        .setIn(['pathsData', action.path, 'data'], fromJS({ id: action.key }))
        .setIn(['pathsData', action.path, 'form'], fromJS({ id: action.key }));
    }
    case actionTypes.SET: {
      const paths = state.get('paths').toJS();
      if (!paths[action.path]) {
        return state;
      }

      const pathData = state.getIn(['pathsData', action.path]);
      if (pathData) {
        return state
          .setIn(['pathsData', action.path, 'isFetching'], initialPathState.get('isFetching'))
          .setIn(['pathsData', action.path, 'data'], action.data)
          .setIn(['pathsData', action.path, 'dataErrors'], initialPathState.get('dataErrors'));
      }

      return state
        .setIn(['pathsData', action.path], initialPathState)
        .setIn(['pathsData', action.path, 'data'], action.data);
    }
    case EDIT_ITEM: {
      const { path, fieldName, newValue } = action;
      return state
        .setIn(['pathsData', path, 'form', fieldName], newValue);
    }
    case EDIT_ITEM_ERROR: {
      const { errors } = action;
      let returnState = state
      const paths = Object.keys(errors);
      for (let i = 0; i < paths.length; i += 1) {
        returnState = state.setIn(['pathsData', paths[i], 'formErrors'], fromJS(errors[paths[i]]));
      }
      return returnState;
    }
    case SAVE_ITEM: {
      const paths = Object.keys(action.paths);
      let returnState = state
      for (let i = 0; i < paths.length; i += 1) {
        returnState = state.setIn(['pathsData', paths[i], 'isFetching'], 'Saving item...');
      }
      return returnState;
    }
    case SAVE_ITEM_SUCCESS: {
      return state
        .setIn(['notifications', uuid()], 'Successfully updated records!');
    }
    case SAVE_ITEM_FAIL: {
      return state
        .setIn(['notifications', uuid()], 'Error updating records, please try again!')
        .set('isFetching', null);
    }
    case CLEAR_NOTIFICATION: {
      return state.deleteIn(['notifications', action.notification]);
    }
    case UPLOAD_ITEM_IMAGE: {
      const { path, schema, file, blob } = action;
      const { name, size, type, lastModifiedDate } = file;

      const files = Object.assign({}, this.state.files);
      files[name] = {
        name,
        type,
        size,
        lastModified: new Date(lastModifiedDate),
        blob,
      };
      return state;
    }
    default:
      return state;
  }
}

export default editRowsContainerReducer;

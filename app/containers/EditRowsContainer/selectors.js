import { createSelector } from 'reselect';

/**
 * Direct selector to the editRowsContainer state domain
 */
const selectEditRowsContainerDomain = () => (state) => state.get('editRowsContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EditRowsContainer
 */

const makeSelectEditRowsContainer = () => createSelector(
  selectEditRowsContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEditRowsContainer;
export {
  selectEditRowsContainerDomain,
};

/*
 *
 * EditRowsContainer constants
 *
 */

export const ADD_ITEM = 'app/EditRowsContainer/ADD_ITEM';
export const ADD_ITEM_SUCCESS = 'app/EditRowsContainer/ADD_ITEM_SUCCESS';
export const ADD_ITEM_FAIL = 'app/EditRowsContainer/ADD_ITEM_FAIL';

export const LOAD_ITEM_IMAGE = 'app/EditRowsContainer/LOAD_ITEM_IMAGE';
export const LOAD_ITEM_IMAGE_SUCCESS = 'app/EditRowsContainer/LOAD_ITEM_IMAGE_SUCCESS';
export const LOAD_ITEM_IMAGE_FAIL = 'app/EditRowsContainer/LOAD_ITEM_IMAGE_FAIL';
export const UPLOAD_ITEM_IMAGE = 'app/EditRowsContainer/UPLOAD_ITEM_IMAGE';
export const UPLOAD_ITEM_IMAGE_SUCCESS = 'app/EditRowsContainer/UPLOAD_ITEM_IMAGE_SUCCESS';
export const UPLOAD_ITEM_IMAGE_FAIL = 'app/EditRowsContainer/UPLOAD_ITEM_IMAGE_FAIL';

export const EDIT_ITEM = 'app/EditRowsContainer/EDIT_ITEM';
export const EDIT_ITEM_ERROR = 'app/EditRowsContainer/EDIT_ITEM_ERROR';

export const SAVE_ITEM = 'app/EditRowsContainer/SAVE_ITEM';
export const SAVE_ITEM_SUCCESS = 'app/EditRowsContainer/SAVE_ITEM_SUCCESS';
export const SAVE_ITEM_FAIL = 'app/EditRowsContainer/SAVE_ITEM_FAIL';

export const SET_PATH = 'app/EditRowsContainer/SET_PATH';
export const START_ACTION = 'app/EditRowsContainer/START_ACTION';
export const SET_ACTION = 'app/EditRowsContainer/SET_ACTION';

export const CLEAR_NOTIFICATION = 'app/EditRowsContainer/CLEAR_NOTIFICATION_ACTION';


import { fromJS } from 'immutable';
import emergencyServicesContainerReducer from '../reducer';

describe('emergencyServicesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(emergencyServicesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

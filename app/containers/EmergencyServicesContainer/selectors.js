import { createSelector } from 'reselect';

/**
 * Direct selector to the emergencyServicesContainer state domain
 */
const selectEmergencyServicesContainerDomain = () => (state) => state.get('emergencyServicesContainer');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EmergencyServicesContainer
 */

const makeSelectEmergencyServicesContainer = () => createSelector(
  selectEmergencyServicesContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEmergencyServicesContainer;
export {
  selectEmergencyServicesContainerDomain,
};

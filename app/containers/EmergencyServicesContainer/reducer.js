/*
 *
 * EmergencyServicesContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
} from './constants';

const initialState = fromJS({
  data: [{
    "id": "cat1",
    "title": "TOWING",
    "text": "",
    "items": [{
      "id": "item1",
      "image": "aa_zimbabwe.png",
      "title": "AA ZIMBABWE EMERGENCY ROAD RESCUE SERVICE",
      "text": "By telephoning the AAZ 24 Hour Helpline +263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide.\n\n    This service comprises road rescue officer’s in Harare, Bulawayo, Gweru and Mutare providing a FREE repair (excluding spares) or a FREE get – you – home service. Yet another valuable saving, which could recoup the entire annual subscription, many times over.\n\n**BREAKDOWN SERVICE:**\n\nThe AAZ has exclusive Service Provider contracts with specially selected and reputable breakdown firms throughout the country to give priority 24 hour service to AAZ members. Members pay the contractor in full and claim a refund from the AAZ at prescribed rates.\n\n**AAZ 24 HOUR HELPLINE**\n\nBy telephoning the AAZ 24 Hour Helpline + 263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide, managed from start to finish by the Helpline Operator. The Operator will dispatch either a Road Rescue Officer (Harare 24hrs) or a Service Provider.\n\n**UNLOCKING SERVICE:**\n\nMembers who inadvertently lock their vehicle with keys inside may obtain the service of a Locksmith to open the vehicle and, within certain limits, claim a refund from the AAZ for the Locksmith’s charges.",
      "item_raw": "**AA ZIMBABWE EMERGENCY ROAD RESCUE SERVICE**\n\nBy telephoning the AAZ 24 Hour Helpline +263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide.\n\n    This service comprises road rescue officer’s in Harare, Bulawayo, Gweru and Mutare providing a FREE repair (excluding spares) or a FREE get – you – home service. Yet another valuable saving, which could recoup the entire annual subscription, many times over.\n\n**BREAKDOWN SERVICE:**\n\nThe AAZ has exclusive Service Provider contracts with specially selected and reputable breakdown firms throughout the country to give priority 24 hour service to AAZ members. Members pay the contractor in full and claim a refund from the AAZ at prescribed rates.\n\n**AAZ 24 HOUR HELPLINE**\n\nBy telephoning the AAZ 24 Hour Helpline + 263 04 776760, members will receive 24 Hours Emergency Roadside assistance country-wide, managed from start to finish by the Helpline Operator. The Operator will dispatch either a Road Rescue Officer (Harare 24hrs) or a Service Provider.\n\n**UNLOCKING SERVICE:**\n\nMembers who inadvertently lock their vehicle with keys inside may obtain the service of a Locksmith to open the vehicle and, within certain limits, claim a refund from the AAZ for the Locksmith’s charges."
    }, {
      "id": "item2",
      "image": "city_tour.jpg",
      "title": "CITY TOUR ROADSIDE ASSITANCE",
      "text": "CTR offers a variety of towing services in addition to road service, including automobile transport, law enforcement towing, insurance salvage towing and roadside assistance.\n\nWe are Zimbabwe's leader in terms of Roadside Assistance. Our services include towing Services, Fuel Delivery, Jump Starting, 24 Hour Towing & Roadside Services as well as emergency roadside services for autos, vans, SUVs, light trucks, motor homes, motor vehicle trade calls, (delivery and collection of vehicles for mechanical services, etc.). Mobile car keys recovery, standard vehicle tyre changes, local & long-distance towing panel beating and spray painting.",
      "item_raw": "**CITY TOUR ROADSIDE ASSITANCE**\n\nCTR offers a variety of towing services in addition to road service, including automobile transport, law enforcement towing, insurance salvage towing and roadside assistance.\n\nWe are Zimbabwe's leader in terms of Roadside Assistance. Our services include towing Services, Fuel Delivery, Jump Starting, 24 Hour Towing & Roadside Services as well as emergency roadside services for autos, vans, SUVs, light trucks, motor homes, motor vehicle trade calls, (delivery and collection of vehicles for mechanical services, etc.). Mobile car keys recovery, standard vehicle tyre changes, local & long-distance towing panel beating and spray painting."
    }]
  }, {
    "id": "cat2",
    "title": "POLICE",
    "text": "",
    "items": [{
      "id": "item1",
      "image": null,
      "title": "ZIMBABWE REPUBLIC POLICE",
      "text": "The** **Zimbabwe Republic Police (**ZRP**) is the national police force of Zimbabwe, and is head-quartered in Harare at the Police General Headquarters (**PGHQ**).\n\nThe force is organised by province, and comprises uniformed national police referred to as Duty Uniform Branch (**DUB**), the plain clothes comprised Criminal Investigation Department ( CID ) and Police Internal Security Intelligence (**PISI** ), and traffic police (part of DUB).\n\nTo date, there are seventeen (17) known provinces which are headed by a Senior Assistant Commissioner. It also includes specialist support units including the (paramilitary) Police Support Unit and riot police and the Canine units.\n\nOverall command of the force is exercised by the Commissioner General Dr. Augustine Chihuri deputised by four Deputy Commissioner Generals who form part of the Central Planning Committee (CPC), a decision passing body in the ZRP. The deputy commissioner generals are also deputised by five commissioners. This structure makes the Commissioner General, a five star general.",
      "item_raw": "**ZIMBABWE REPUBLIC POLICE**\n\nThe** **Zimbabwe Republic Police (**ZRP**) is the national police force of Zimbabwe, and is head-quartered in Harare at the Police General Headquarters (**PGHQ**).\n\nThe force is organised by province, and comprises uniformed national police referred to as Duty Uniform Branch (**DUB**), the plain clothes comprised Criminal Investigation Department ( CID ) and Police Internal Security Intelligence (**PISI** ), and traffic police (part of DUB).\n\nTo date, there are seventeen (17) known provinces which are headed by a Senior Assistant Commissioner. It also includes specialist support units including the (paramilitary) Police Support Unit and riot police and the Canine units.\n\nOverall command of the force is exercised by the Commissioner General Dr. Augustine Chihuri deputised by four Deputy Commissioner Generals who form part of the Central Planning Committee (CPC), a decision passing body in the ZRP. The deputy commissioner generals are also deputised by five commissioners. This structure makes the Commissioner General, a five star general."
    }, {
      "id": "item2",
      "image": null,
      "title": "Chitungwiza District (0270) 30571",
      "text": "Chitungwiza (0270) 22006\n\nZengeza (0270) 30399\n\nSt Mary’s (0270) 21928\n\nHatfield (04) 570949\n\nAirport (04) 575366\n\nEpworth (04) 293741",
      "item_raw": "**Chitungwiza District (0270) 30571**\n\nChitungwiza (0270) 22006\n\nZengeza (0270) 30399\n\nSt Mary’s (0270) 21928\n\nHatfield (04) 570949\n\nAirport (04) 575366\n\nEpworth (04) 293741"
    }, {
      "id": "item3",
      "image": null,
      "title": "Mbare District (04) 754 377",
      "text": "Mbare (04) 774770\n\nBraeside (04) 743020\n\nMatapi (04) 756115\n\nStodart (04) 665568\n\nWaterfalls (04) 665395",
      "item_raw": "**Mbare District (04) 754 377**\n\nMbare (04) 774770\n\nBraeside (04) 743020\n\nMatapi (04) 756115\n\nStodart (04) 665568\n\nWaterfalls (04) 665395"
    }, {
      "id": "item4",
      "image": null,
      "title": "Harare Suburban District (04) 777 639",
      "text": "Avondale (04) 336 361-2\n\nBorrowdale (04) 860 148\n\nHighlands (04) 496 767\n\nMalbereign (04) 305 651\n\nMabvuku (04) 491 069\n\nMarlborough (04) 301 802\n\nRhodesville (04) 495 753 / 481 111\n\nRuwa 0733 233 686",
      "item_raw": "**Harare Suburban District (04) 777 639**\n\nAvondale (04) 336 361-2\n\nBorrowdale (04) 860 148\n\nHighlands (04) 496 767\n\nMalbereign (04) 305 651\n\nMabvuku (04) 491 069\n\nMarlborough (04) 301 802\n\nRhodesville (04) 495 753 / 481 111\n\nRuwa 0733 233 686"
    }, {
      "id": "item5",
      "image": null,
      "title": "Harare Central District (04) 777 635",
      "text": "Harare Central (04) 777 777\n\nMilton Park (04) 708 113",
      "item_raw": "**Harare Central District (04) 777 635**\n\nHarare Central (04) 777 777\n\nMilton Park (04) 708 113"
    }, {
      "id": "item6",
      "image": null,
      "title": "Masvingo Province Community Relations Office (039) 264 421 or 0775 996 945",
      "text": "Masvingo West District (0337) 729 705 or 0772 259 403\n\nMashava (035) 2582\n\nChivi (037) 225\n\nNgundu (036) 221\n\nMwenezi (014) 321",
      "item_raw": "**Masvingo Province Community Relations Office (039) 264 421 or 0775 996 945**\n\nMasvingo West District (0337) 729 705 or 0772 259 403\n\nMashava (035) 2582\n\nChivi (037) 225\n\nNgundu (036) 221\n\nMwenezi (014) 321"
    }, {
      "id": "item7",
      "image": null,
      "title": "Masvingo East District (0338) 724",
      "text": "Bikita (0338) 222\n\nMashoko (034) 22705\n\nChatsworth (0308) 223\n\nGutu (030) 2222\n\nZaka (034) 2222\n\nBasera (030) 3179",
      "item_raw": "**Masvingo East District (0338) 724**\n\nBikita (0338) 222\n\nMashoko (034) 22705\n\nChatsworth (0308) 223\n\nGutu (030) 2222\n\nZaka (034) 2222\n\nBasera (030) 3179"
    }, {
      "id": "item8",
      "image": null,
      "title": "Masvingo Central (039) 66446 or 0777 557 815",
      "text": "Masvingo Rural (039) 263729\n\nChikato (039) 262137\n\nRenko (036) 222281\n\nMuchakata (039) 266944\n\nRujeko (039) 293015",
      "item_raw": "**Masvingo Central (039) 66446 or 0777 557 815**\n\nMasvingo Rural (039) 263729\n\nChikato (039) 262137\n\nRenko (036) 222281\n\nMuchakata (039) 266944\n\nRujeko (039) 293015"
    }, {
      "id": "item9",
      "image": null,
      "title": "Chiredzi District (031) 2456 or 0773 508 216",
      "text": "Chiredzi (031) 2297/2333\n\nChikombedzi (014) 3607\n\nMkwasini (031) 2535\n\nTriangle (033) 6511 / 6237",
      "item_raw": "**Chiredzi District (031) 2456 or 0773 508 216**\n\nChiredzi (031) 2297/2333\n\nChikombedzi (014) 3607\n\nMkwasini (031) 2535\n\nTriangle (033) 6511 / 6237"
    }, {
      "id": "item10",
      "image": null,
      "title": "Bulawayo Province Community Relations Office (09)-60358 / 0776 097 122",
      "text": "Bulawayo Central District (09) 69699\n\nBulawayo Central (09)71515\n\nLicense Inspectorate (09)74318",
      "item_raw": "**Bulawayo Province Community Relations Office (09)-60358 / 0776 097 122**\n\nBulawayo Central District (09) 69699\n\nBulawayo Central (09)71515\n\nLicense Inspectorate (09)74318"
    }, {
      "id": "item11",
      "image": null,
      "title": "Bulawayo West District\t(09) 886 370",
      "text": "Mzilikazi (09) 77619\n\nNjube (09) 412096\n\nWestern Commonage (09) 406 775\n\nEntumbane (09) 418 243",
      "item_raw": "**Bulawayo West District\t(09) 886 370**\n\nMzilikazi (09) 77619\n\nNjube (09) 412096\n\nWestern Commonage (09) 406 775\n\nEntumbane (09) 418 243"
    }, {
      "id": "item12",
      "image": null,
      "title": "Nkulumane District (09) 881536",
      "text": "Nkulumane (09) 48145\n\nLuveve (09) 520 803\n\nMagwegwe (09) 424 771\n\nPumula (09) 422 907\n\nTshabalala (09) 489 564",
      "item_raw": "**Nkulumane District (09) 881536**\n\nNkulumane (09) 48145\n\nLuveve (09) 520 803\n\nMagwegwe (09) 424 771\n\nPumula (09) 422 907\n\nTshabalala (09) 489 564"
    }, {
      "id": "item13",
      "image": null,
      "title": "Bulawayo Suburban District (09) 65891",
      "text": "Hillside (09) 242426\n\nDonnington (09) 468 520\n\nSauerstown (09) 200 960 \n\nQueenspark (09) 226 414\n\nAirport (09) 296 589",
      "item_raw": "**Bulawayo Suburban District (09) 65891**\n\nHillside (09) 242426\n\nDonnington (09) 468 520\n\nSauerstown (09) 200 960 \n\nQueenspark (09) 226 414\n\nAirport (09) 296 589"
    }, {
      "id": "item14",
      "image": null,
      "title": "Midlands Province Community Relations Office (054) 221 073 or 0782 722 722",
      "text": "",
      "item_raw": "**Midlands Province Community Relations Office (054) 221 073 or 0782 722 722**"
    }, {
      "id": "item15",
      "image": null,
      "title": "Kwekwe District (055) 24547",
      "text": "Amaveni (055) 22808\n\nMbizo (055) 40446\n\nSilobela (0558) 322\n\nZhombe (059) 20070 / 20069\n\nRedcliff (059) 68601\n\nKwekwe Rural (059) 24546\n\nKwekwe Central (059) 24541-2",
      "item_raw": "**Kwekwe District (055) 24547**\n\nAmaveni (055) 22808\n\nMbizo (055) 40446\n\nSilobela (0558) 322\n\nZhombe (059) 20070 / 20069\n\nRedcliff (059) 68601\n\nKwekwe Rural (059) 24546\n\nKwekwe Central (059) 24541-2"
    }, {
      "id": "item16",
      "image": null,
      "title": "Gokwe District (059) 2281 / 2221",
      "text": "Nembudziya (059)2412\n\nGokwe\t(059) 2222 / 2223",
      "item_raw": "**Gokwe District (059) 2281 / 2221**\n\nNembudziya (059)2412\n\nGokwe\t(059) 2222 / 2223"
    }, {
      "id": "item17",
      "image": null,
      "title": "Zvishavane District (051) 2121-4",
      "text": "Zvishavane (051) 2044\n\nMberengwa (051) 221\n\nMataga\t(051) 237 / 655\n\nBuchwa (051) 4097",
      "item_raw": "**Zvishavane District (051) 2121-4**\n\nZvishavane (051) 2044\n\nMberengwa (051) 221\n\nMataga\t(051) 237 / 655\n\nBuchwa (051) 4097"
    }, {
      "id": "item18",
      "image": null,
      "title": "Gweru Urban District (054) 223 770",
      "text": "Nehanda (054) 256 698\n\nGweru Central\t (054) 222 121-5\n\nMkoba\t(054) 250 016\n\nSenga (054) 260 411\n\nMutapa\t (054) 222 727",
      "item_raw": "**Gweru Urban District (054) 223 770**\n\nNehanda (054) 256 698\n\nGweru Central\t (054) 222 121-5\n\nMkoba\t(054) 250 016\n\nSenga (054) 260 411\n\nMutapa\t (054) 222 727"
    }, {
      "id": "item19",
      "image": null,
      "title": "Gweru Rural District (054) 226454",
      "text": "Gweru Rural (054) 222 184\n\nMaboleni (054) 229 372\n\nShurugwi (052) 6279/6277\n\nTongogara (054) 6545\n\nMvuma (032) 214-5/ 228\n\nLalapanzi (054) 8338 or 301-2\n\nCharandura (038) 217 /3905",
      "item_raw": "**Gweru Rural District (054) 226454**\n\nGweru Rural (054) 222 184\n\nMaboleni (054) 229 372\n\nShurugwi (052) 6279/6277\n\nTongogara (054) 6545\n\nMvuma (032) 214-5/ 228\n\nLalapanzi (054) 8338 or 301-2\n\nCharandura (038) 217 /3905"
    }, {
      "id": "item20",
      "image": null,
      "title": "Police Protection Unit Community Relations Office (04) 735 751-6 or 0785 004 917",
      "text": "",
      "item_raw": "**Police Protection Unit Community Relations Office (04) 735 751-6 or 0785 004 917**"
    }, {
      "id": "item21",
      "image": null,
      "title": "Criminal Investigation Department HQ Community Relations Office (04) 700 171-6",
      "text": "",
      "item_raw": "**Criminal Investigation Department HQ Community Relations Office (04) 700 171-6**"
    }, {
      "id": "item22",
      "image": null,
      "title": "Support Unit Province Community Relations Office (04)497334-6 or 0783 822 619",
      "text": "",
      "item_raw": "**Support Unit Province Community Relations Office (04)497334-6 or 0783 822 619**"
    }, {
      "id": "item23",
      "image": null,
      "title": "National Traffic Community Relations Office (04) 754 333",
      "text": "",
      "item_raw": "**National Traffic Community Relations Office (04) 754 333**"
    }, {
      "id": "item24",
      "image": null,
      "title": "Border Control Community Relations Office (04)2901152-4 or 0773 006 737",
      "text": "",
      "item_raw": "**Border Control Community Relations Office (04)2901152-4 or 0773 006 737**"
    }, {
      "id": "item25",
      "image": null,
      "title": "Bulawayo Border Control 0785 957 764 ",
      "text": "",
      "item_raw": "**Bulawayo Border Control 0785 957 764 **"
    }, {
      "id": "item26",
      "image": null,
      "title": "Bulawayo Border Control Community Relations Office (04) 700 931-5 or 0777 966 416",
      "text": "",
      "item_raw": "**Bulawayo Border Control Community Relations Office (04) 700 931-5 or 0777 966 416**"
    }, {
      "id": "item27",
      "image": null,
      "title": "Matebeleland North Province (0281)30259",
      "text": "",
      "item_raw": "**Matebeleland North Province (0281)30259**"
    }, {
      "id": "item28",
      "image": null,
      "title": "Mat North Community Relations Office (0281) 32739\t",
      "text": "Hwange (0281) 32750\n\nDete (018) 212\n\nBinga (015) 301\n\nLusulu 0775 580 868\n\nKamativi (018) 463\n\nSiyabuwa (015) 354",
      "item_raw": "**Mat North Community Relations Office (0281) 32739\t** \n\nHwange (0281) 32750\n\nDete (018) 212\n\nBinga (015) 301\n\nLusulu 0775 580 868\n\nKamativi (018) 463\n\nSiyabuwa (015) 354"
    }, {
      "id": "item29",
      "image": null,
      "title": "Lupane District (09) 60132",
      "text": "Lupane\t (0389)249\n\nTsholotsho (0878) 267\n\nJotsholo (0289) 284\n\nSipepa\t(0898) 442\n\nInsuza\t(087) 245\n\nNyamadhlovu (0287) 304",
      "item_raw": "**Lupane District (09) 60132**\n\nLupane\t (0389)249\n\nTsholotsho (0878) 267\n\nJotsholo (0289) 284\n\nSipepa\t(0898) 442\n\nInsuza\t(087) 245\n\nNyamadhlovu (0287) 304"
    }, {
      "id": "item30",
      "image": null,
      "title": "Victoria Falls District (013)41571",
      "text": "Victoria Falls 0773 029 967\n\nJambezi  (013) 44681\n\nKazungula (0558) 559\n\nNkayi District (0558) 221\n\nNkayi 0717 101 606\n\nMbembesi 0775 288 650\n\nInyati 0713 009 344",
      "item_raw": "**Victoria Falls District (013)41571**\n\nVictoria Falls 0773 029 967\n\nJambezi  (013) 44681\n\nKazungula (0558) 559\n\nNkayi District (0558) 221\n\nNkayi 0717 101 606\n\nMbembesi 0775 288 650\n\nInyati 0713 009 344"
    }, {
      "id": "item31",
      "image": null,
      "title": "Mash Central Province Community Relations Office (0271) 7276 or 0778 682 035",
      "text": "Guruve District (058)2400\n\nMvurwi\t (058)2316\n\n**",
      "item_raw": "**Mash Central Province Community Relations Office (0271) 7276 or 0778 682 035**\n\nGuruve District (058)2400\n\nMvurwi\t (058)2316\n\n**"
    }, {
      "id": "item32",
      "image": null,
      "title": "\n\nBindura Central (0271) 6323 or 6745\n\nBindura Rural\t(0271) 7686\n\nShamva (0371) 357 or 317\n\nConcession (0375) 2212-5\n\nMazowe (0275) 2613 or 2221\n\nGlendale (0376) 2660 or 2849\n\nChombira (0277) 2252\n\nChiwaridzo (0271) 6741 784",
      "text": "indura District (0271) 6264**\n\nBindura Central (0271) 6323 or 6745\n\nBindura Rural\t(0271) 7686\n\nShamva (0371) 357 or 317\n\nConcession (0375) 2212-5\n\nMazowe (0275) 2613 or 2221\n\nGlendale (0376) 2660 or 2849\n\nChombira (0277) 2252\n\nChiwaridzo (0271) 6741 7841",
      "item_raw": "Bindura District (0271) 6264**\n\nBindura Central (0271) 6323 or 6745\n\nBindura Rural\t(0271) 7686\n\nShamva (0371) 357 or 317\n\nConcession (0375) 2212-5\n\nMazowe (0275) 2613 or 2221\n\nGlendale (0376) 2660 or 2849\n\nChombira (0277) 2252\n\nChiwaridzo (0271) 6741 7841"
    }, {
      "id": "item33",
      "image": null,
      "title": "Mt Darwin District (0276) 3016",
      "text": "Mt Darwin (0276) 2394 or 3110\n\nRushinga (0276) 2391\n\nMadziwa (0271) 2560\n\nMukumbura (0271) 2357\n\nDotito (0271) 2395",
      "item_raw": "**Mt Darwin District (0276) 3016**\n\nMt Darwin (0276) 2394 or 3110\n\nRushinga (0276) 2391\n\nMadziwa (0271) 2560\n\nMukumbura (0271) 2357\n\nDotito (0271) 2395"
    }, {
      "id": "item34",
      "image": null,
      "title": "Matebeleland South Province (0284) 22834",
      "text": "",
      "item_raw": "**Matebeleland South Province (0284) 22834**"
    }, {
      "id": "item35",
      "image": null,
      "title": "Mat South Community Relations Office (0284) 22810\tor 0771 443 373",
      "text": "Gwanda District (0284) 21432\n\nGwanda Urban\t (0284) 22860\n\nGwanda Rural\t(0284) 23481\n\nEsigodini (0288) 357\n\nKezi (0282) 334\n\nFilabusi (017)2 47\n\nFortrixon (0288) 628\n\nFun – Yeit-Sen (082) 279\n\nColleenbawn (0284) 20454\n\nWest Nickleson (016) 452\n\nGuyu (0284) 23225",
      "item_raw": "**Mat South Community Relations Office (0284) 22810\tor 0771 443 373**\n\nGwanda District (0284) 21432\n\nGwanda Urban\t (0284) 22860\n\nGwanda Rural\t(0284) 23481\n\nEsigodini (0288) 357\n\nKezi (0282) 334\n\nFilabusi (017)2 47\n\nFortrixon (0288) 628\n\nFun – Yeit-Sen (082) 279\n\nColleenbawn (0284) 20454\n\nWest Nickleson (016) 452\n\nGuyu (0284) 23225"
    }, {
      "id": "item36",
      "image": null,
      "title": "Beitbridge District\t(0286) 23060",
      "text": "Beitbridge Urban (0286) 22554\n\nBeitbridge Rural (0286) 23722\n\nTuli (0286) 3358\n\nZezani\t0775 256 613",
      "item_raw": "**Beitbridge District\t(0286) 23060**\n\nBeitbridge Urban (0286) 22554\n\nBeitbridge Rural (0286) 23722\n\nTuli (0286) 3358\n\nZezani\t0775 256 613"
    }, {
      "id": "item37",
      "image": null,
      "title": "Bulilimamangwe District (019) 3276",
      "text": "Plumtree (019) 2670\n\nMatopo\t (0383) 225\n\nFigtree\t (0283) 246\n\nMadlambuzi (019)3 013 or 0776 133 875\n\nMangwe (019) 25209\n\nMphoengs (019)2669 or 0776 270 510\n\nMayombodo 0775 870 464",
      "item_raw": "**Bulilimamangwe District (019) 3276**\n\nPlumtree (019) 2670\n\nMatopo\t (0383) 225\n\nFigtree\t (0283) 246\n\nMadlambuzi (019)3 013 or 0776 133 875\n\nMangwe (019) 25209\n\nMphoengs (019)2669 or 0776 270 510\n\nMayombodo 0775 870 464"
    }, {
      "id": "item38",
      "image": null,
      "title": "Manicaland Province (020) 68733",
      "text": "Community Relations Office (020) 66637 or 0783 410 710\n\nMutare Rural District\t (020) 66354\n\nPenhalonga (020) 22212\n\nMutare Rural (020) 64545\n\nMarange 0716 348 850\n\nOdzi (0204) 2223",
      "item_raw": "**Manicaland Province (020) 68733**\n\nCommunity Relations Office (020) 66637 or 0783 410 710\n\nMutare Rural District\t (020) 66354\n\nPenhalonga (020) 22212\n\nMutare Rural (020) 64545\n\nMarange 0716 348 850\n\nOdzi (0204) 2223"
    }, {
      "id": "item39",
      "image": null,
      "title": "Mutare Central District (020) 69157",
      "text": "Mutare Central (020) 64717\n\nChikanga (020) 10299\n\nSakubva (020) 64717\n\nDangamvura (020) 30240",
      "item_raw": "**Mutare Central District (020) 69157**\n\nMutare Central (020) 64717\n\nChikanga (020) 10299\n\nSakubva (020) 64717\n\nDangamvura (020) 30240"
    }, {
      "id": "item40",
      "image": null,
      "title": "Rusape District (025) 2696",
      "text": "Rusape Urban\t(025)2359\n\nRusape Rural (025) 3723\n\nHeadlands (025) 822360\n\nInyati (025) 822444\n\nNyazura (025) 83246\n\nMayo (025) 8223243",
      "item_raw": "**Rusape District (025) 2696**\n\nRusape Urban\t(025)2359\n\nRusape Rural (025) 3723\n\nHeadlands (025) 822360\n\nInyati (025) 822444\n\nNyazura (025) 83246\n\nMayo (025) 8223243"
    }, {
      "id": "item41",
      "image": null,
      "title": "Buhera (021) 2177",
      "text": "Murambinda\t(021) 2314 / 2116\n\nDorowa 0782 104 795\n\nMuzokomba 0772 192 963",
      "item_raw": "**Buhera (021) 2177**\n\nMurambinda\t(021) 2314 / 2116\n\nDorowa 0782 104 795\n\nMuzokomba 0772 192 963"
    }, {
      "id": "item42",
      "image": null,
      "title": "Chipinge District (0227) 3368",
      "text": "Chipinge Urban (022) 72412-3\n\nChipinge Rural\t (022) 72416\n\nChimanimani (026) 2535\n\nChisumbanje (031) 7223\n\nMiddle Sabi (024) 322\n\nNyanyadzi (026) 2338",
      "item_raw": "**Chipinge District (0227) 3368**\n\nChipinge Urban (022) 72412-3\n\nChipinge Rural\t (022) 72416\n\nChimanimani (026) 2535\n\nChisumbanje (031) 7223\n\nMiddle Sabi (024) 322\n\nNyanyadzi (026) 2338"
    }, {
      "id": "item43",
      "image": null,
      "title": "Nyanga District (029) 8851",
      "text": "Nyanga\t (029) 8211\n\nRuda (028) 2524\n\nMutasa\t(028) 2586",
      "item_raw": "**Nyanga District (029) 8851**\n\nNyanga\t (029) 8211\n\nRuda (028) 2524\n\nMutasa\t(028) 2586"
    }, {
      "id": "item44",
      "image": null,
      "title": "Mash East Province (0279) 23306",
      "text": "Community Relations Office** **(0271) 7727\n\n(0279) 27142\n\n0783 189 906",
      "item_raw": "**Mash East Province (0279) 23306**\n\nCommunity Relations Office** **(0271) 7727\n\n(0279) 27142\n\n0783 189 906"
    }, {
      "id": "item45",
      "image": null,
      "title": "Marondera District (0279) 23517 or 26508",
      "text": "Marondera Central (0279) 24419 or 22706\n\nMarondera Rural (0279) 23572 or 22706\n\nDombotombo (0379) 312-5\n\nMacheke 0772 290 395 or 0779 192 239\n\nDema (0279) 25300\n\nMahusekwa (0274) 2213 or 2666\n\nGoromomzi (0222) 22313-5",
      "item_raw": "**Marondera District (0279) 23517 or 26508**\n\nMarondera Central (0279) 24419 or 22706\n\nMarondera Rural (0279) 23572 or 22706\n\nDombotombo (0379) 312-5\n\nMacheke 0772 290 395 or 0779 192 239\n\nDema (0279) 25300\n\nMahusekwa (0274) 2213 or 2666\n\nGoromomzi (0222) 22313-5"
    }, {
      "id": "item46",
      "image": null,
      "title": "Murehwa District (0278) 22075 or 0734 100 967 or 0773 386 771",
      "text": "Makosa\t (0278) 2255\n\nMurehwa (0272) 2517\n\nNyamapanda 0773 514 992 or 0772 952 827\n\nChinamhora (0274) 22182\n\nJuru (0274)2323 or 2222\n\nMutoko\t 0772 339 235 or 0773 520 357\n\nMtawatawa (0272) 2595 or 0736 847 684",
      "item_raw": "**Murehwa District (0278) 22075 or 0734 100 967 or 0773 386 771**\n\nMakosa\t (0278) 2255\n\nMurehwa (0272) 2517\n\nNyamapanda 0773 514 992 or 0772 952 827\n\nChinamhora (0274) 22182\n\nJuru (0274)2323 or 2222\n\nMutoko\t 0772 339 235 or 0773 520 357\n\nMtawatawa (0272) 2595 or 0736 847 684"
    }, {
      "id": "item47",
      "image": null,
      "title": "Chivhu District (056) 2244 or (056) 412",
      "text": "Chivhu (056) 3052\n\nFeatherstone (0222) 2369\n\nSadza (065) 212-4\n\nBeatrice 0772 736 797 or 0773 686 225",
      "item_raw": "**Chivhu District (056) 2244 or (056) 412**\n\nChivhu (056) 3052\n\nFeatherstone (0222) 2369\n\nSadza (065) 212-4\n\nBeatrice 0772 736 797 or 0773 686 225"
    }, {
      "id": "item48",
      "image": null,
      "title": "Mashonaland West Province (067) 24193",
      "text": "Community Relations Office (067) 25505 or 0777 480 585\n\nMakonde District (067) 23711\n\nChinhoyi Central (067) 23200\n\nChinhoyi Rural\t (067) 25397\n\nChemagamba (067) 23773\n\nBanket (066) 2608\n\nZvimba (0678) 2210\n\nKutama (0678) 2180\n\nMhangura (067) 5520\n\nMutorashanga\t(0668) 213\n\nMurereka 0772 288 802\n\nKenzamba 0774 072 401",
      "item_raw": "**Mashonaland West Province (067) 24193**\n\nCommunity Relations Office (067) 25505 or 0777 480 585\n\nMakonde District (067) 23711\n\nChinhoyi Central (067) 23200\n\nChinhoyi Rural\t (067) 25397\n\nChemagamba (067) 23773\n\nBanket (066) 2608\n\nZvimba (0678) 2210\n\nKutama (0678) 2180\n\nMhangura (067) 5520\n\nMutorashanga\t(0668) 213\n\nMurereka 0772 288 802\n\nKenzamba 0774 072 401"
    }, {
      "id": "item49",
      "image": null,
      "title": "Manyame District (062) 3059",
      "text": "Norton Urban\t(062) 2120\n\nNorton traffic\t(062) 3530\n\nNorton Rural (062) 3089\n\nNyabira (069) 259\n\nDarwendale (069)(383) 212\n\nSaruhwe (0628) 44213\n\nMubaira (065) (200) (400) 475",
      "item_raw": "**Manyame District (062) 3059**\n\nNorton Urban\t(062) 2120\n\nNorton traffic\t(062) 3530\n\nNorton Rural (062) 3089\n\nNyabira (069) 259\n\nDarwendale (069)(383) 212\n\nSaruhwe (0628) 44213\n\nMubaira (065) (200) (400) 475"
    }, {
      "id": "item50",
      "image": null,
      "title": "Hurungwe District\t(064) 8015",
      "text": "Karoi urban (064) 6456\n\nKaroi rural (064) 7166\n\nMagunje (064) 6804\n\nTengwe (064) 7082\n\nSiakobva (061) 2748",
      "item_raw": "**Hurungwe District\t(064) 8015**\n\nKaroi urban (064) 6456\n\nKaroi rural (064) 7166\n\nMagunje (064) 6804\n\nTengwe (064) 7082\n\nSiakobva (061) 2748"
    }, {
      "id": "item51",
      "image": null,
      "title": "Kariba District (061) 3010",
      "text": "Kariba\t(061) 3132 or 2704\n\nChirundu(063) 7642\n\nMakuti\t(063) 517",
      "item_raw": "**Kariba District (061) 3010**\n\nKariba\t(061) 3132 or 2704\n\nChirundu(063) 7642\n\nMakuti\t(063) 517"
    }, {
      "id": "item52",
      "image": null,
      "title": "Kadoma District (068) 26525",
      "text": "Kadoma Central (068) 24057\n\nKadoma Rural\t(068) 26111\n\nRimuka (068) 23740\n\nBattlefield (0557) 435\n\nEffle Flats (068)23025\n\nSanyati\t (0687) 2551",
      "item_raw": "**Kadoma District (068) 26525**\n\nKadoma Central (068) 24057\n\nKadoma Rural\t(068) 26111\n\nRimuka (068) 23740\n\nBattlefield (0557) 435\n\nEffle Flats (068)23025\n\nSanyati\t (0687) 2551"
    }, {
      "id": "item53",
      "image": null,
      "title": "Chegutu District (053) 3403",
      "text": "Chegutu (053) 2490\n\nPfupajena (053) 2207\n\nMamina (068) 32102\n\nChakari (068) 8237\n\nChingondo 0782 102 900",
      "item_raw": "**Chegutu District (053) 3403**\n\nChegutu (053) 2490\n\nPfupajena (053) 2207\n\nMamina (068) 32102\n\nChakari (068) 8237\n\nChingondo 0782 102 900"
    }]
  }],
  schema: [
    {
      title: "ID",
      key: "id",
      type: "numeric",
    },
    {
      title: "title",
      key: "title",
      type: "string",
    },
    {
      title: "text",
      key: "text",
      type: "markdown",
    },
    {
      title: "items",
      key: "items",
      type: "array",
      schema: [
        {
          title: "ID",
          key: "id",
          type: "numeric",
        },
        {
          title: "image",
          key: "image",
          type: "string",
        },
        {
          title: "title",
          key: "title",
          type: "string",
        },
        {
          title: "text",
          key: "text",
          type: "markdown",
        },
        {
          title: "item_raw",
          key: "item_raw",
          type: "markdown",
        },
      ],
    },
  ],
  sorted: "id",
});

function emergencyServicesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default emergencyServicesContainerReducer;

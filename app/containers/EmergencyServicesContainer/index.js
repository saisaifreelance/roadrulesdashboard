/*
 *
 * EmergencyServicesContainer
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import makeSelectEmergencyServicesContainer from './selectors';

export class EmergencyServicesContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <section id="content">
        <Helmet
          title="EmergencyServicesContainer"
          meta={[
            { name: 'description', content: 'Description of EmergencyServicesContainer' },
          ]}
        />
      </section>
    );
  }
}

EmergencyServicesContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  EmergencyServicesContainer: makeSelectEmergencyServicesContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EmergencyServicesContainer);

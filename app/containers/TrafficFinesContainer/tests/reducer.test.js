
import { fromJS } from 'immutable';
import trafficFinesContainerReducer from '../reducer';

describe('trafficFinesContainerReducer', () => {
  it('returns the initial state', () => {
    expect(trafficFinesContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

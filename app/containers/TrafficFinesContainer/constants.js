/*
 *
 * TrafficFinesContainer constants
 *
 */

export const PAGINATE = 'app/TrafficFinesContainer/PAGINATE';
export const PAGINATE_SUCCESS = 'app/TrafficFinesContainer/PAGINATE_SUCCESS';
export const PAGINATE_FAIL = 'app/TrafficFinesContainer/PAGINATE_FAIL';
export const SORT = 'app/TrafficFinesContainer/SORT';

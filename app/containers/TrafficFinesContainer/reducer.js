/*
 *
 * TrafficFinesContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {actionTypes} from 'react-redux-firebase';
import {
  PAGINATE, SORT,
} from './constants';

const initialState = fromJS({
  metaData: null,
  data: null,
  isFetchingTrafficFines: null,
  isFetchingMetaTrafficFines: null,
  startAt: null,
  endAt: null,
  sorted: 'id',
  direction: 'desc',
  pageSize: 10,
  start: 0,
});

function trafficFinesContainerReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET:
      if (action.path === '/traffic_fines') {
        return state
          .set('isFetchingTrafficFines', null)
          .set('data', action.ordered);
      }
      if (action.path === '/meta_traffic_fines') {
        return state
          .set('isFetchingMetaTrafficFines', null)
          .set('metaData', action.data);
      }
      return state;
    case actionTypes.NO_VALUE:
      return state;
    case actionTypes.START:
      if (action.path === '/traffic_fines') {
        return state.set('isFetchingTrafficFines', 'Fetching trafficFines...');
      }
      if (action.path === '/meta_traffic_fines') {
        return state.set('isFetchingMetaTrafficFines', 'Fetching trafficFines...');
      }
      return state;
    case PAGINATE: {
      if (action.start === 0) {
        return state
          .set('start', 0)
          .set('startAt', null)
          .set('endAt', null)
          .set('pageSize', action.pageSize);
      }

      const {start, pageSize, sorted, data, direction, endAt, startAt} = state.toJS();
      if (action.start > start) {
        const nextStart = start + pageSize
        const nextStartAt = data[data.length - 1][sorted];
        return state
          .set('start', nextStart)
          .set('startAt', nextStartAt)
          .set('endAt', null)
          .set('pageSize', pageSize);
      }

      if (action.start < start) {
        const nextStart = (start - pageSize) > 0 ? (start - pageSize) : 0
        const nextEndAt = nextStart > 0 ? data[0][sorted] : null;
        return state
          .set('start', nextStart)
          .set('startAt', null)
          .set('endAt', nextEndAt)
          .set('pageSize', pageSize);
      }

      if (action.start === start) {
        return state;
      }
      return state;
    }
    case SORT:
      return state
        .set('sorted', action.sorted)
        .set('direction', action.direction)
        .set('startAt', null)
        .set('endAt', null)
        .set('start', 0)
        .set('data', null);
    default:
      return state;
  }
}

export default trafficFinesContainerReducer;

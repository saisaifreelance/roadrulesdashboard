import { createSelector } from 'reselect';
import schema from './schema';

/**
 * Direct selector to the trafficFinesContainer state domain
 */
const selectTrafficFinesContainerDomain = (state) => state.get('trafficFinesContainer');

/**
 * Other specific selectors
 */
const selectTable = createSelector(
  selectTrafficFinesContainerDomain,
  (trafficFinesContainerDomain) => {
    const trafficFinesContainer = trafficFinesContainerDomain.toJS();
    const { metaData, data, isFetchingTrafficFines, isFetchingMetaTrafficFines, startAt, endAt, sorted, direction, pageSize, start } = trafficFinesContainer;
    const page = Math.floor(start / pageSize) + 1
    return {
      isFetching: isFetchingTrafficFines || isFetchingMetaTrafficFines,
      empty: !data || !data.length,
      count: metaData ? metaData.count : 0,
      data: data ? data.slice(0, pageSize).map((client) => {
        const {
          id,
          title,
          description,
          text,
        } = client
        return {
          id,
          title,
          description,
          text,
        };
      }) : data,
      page,
      startAt,
      endAt,
      schema,
      sorted,
      direction,
      pageSize,
      start,
    };
  }
);

const selectQueries = createSelector(
  selectTable,
  (table) => {
    const queryParams = [`orderByChild=${table.sorted}`];

    if (!table.endAt && !table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
    }

    if (table.startAt) {
      queryParams.push(`limitToFirst=${table.pageSize + 1}`);
      queryParams.push(`startAt=${table.startAt}`);
    }

    if (table.endAt) {
      queryParams.push(`limitToLast=${table.pageSize + 1}`);
      queryParams.push(`endAt=${table.endAt}`);
    }
    return ([
      {
        path: '/traffic_fines',
        queryParams,
      },
      {
        path: '/meta_traffic_fines',
      },
    ]);
  }
);


/**
 * Default selector used by TrafficFinesContainer
 */

const makeSelectTrafficFinesContainer = () => createSelector(
  selectTrafficFinesContainerDomain,
  selectTable,
  selectQueries,
  (substate, table, queries) => ({
    table,
    queries,
  })
);

export default makeSelectTrafficFinesContainer;
export {
  selectTrafficFinesContainerDomain,
};


import { fromJS } from 'immutable';
import registerContainerReducer from '../reducer';

describe('registerContainerReducer', () => {
  it('returns the initial state', () => {
    expect(registerContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

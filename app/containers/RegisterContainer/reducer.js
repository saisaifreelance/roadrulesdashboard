/*
 *
 * RegisterContainer reducer
 *
 */

import { fromJS } from 'immutable';
import {
  CHANGE_FIELD, REGISTER, REGISTER_FAILED, REGISTER_SUCCEEDED,
} from './constants';

const initialState = fromJS({
  name: {
    value: '',
    error: '',
  },
  email: {
    value: '',
    error: '',
  },
  password: {
    value: '',
    error: '',
  },
  error: '',
  process: '',
});

function setError(state, action) {
  switch (action.error.code) {
    case 'auth/argument-error':
      return state.set('error', action.error.message);
    case 'auth/user-not-found':
      return state.set('error', action.error.message);
    case 'auth/invalid-email':
      return state.updateIn(['email', 'error'], () => action.error.message);
    default:
      return state.updateIn(['email', 'error'], () => action.error.message);
  }
}

function registerContainerReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_FIELD:
      return state.updateIn([action.field, 'value'], () => action.value).updateIn([action.field, 'error'], () => '');
    case REGISTER:
      return state.set('process', 'registering user...');
    case REGISTER_FAILED:
      return setError(state, action).set('process', '');
    default:
      return state;
  }
}

export default registerContainerReducer;

/*
 *
 * RegisterContainer
 *
 */

import React, {PropTypes} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Link} from 'react-router';
import {createStructuredSelector} from 'reselect';
import makeSelectRegisterContainer, {selectForm} from './selectors';
import {register, changeField} from './actions';
import Input from '../../components/Input';
import AuthProcessing from '../../components/AuthProcessing';
import { userIsNotAuthenticated } from '../../auth';

export class RegisterContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.register(this.props.form);
  }

  render() {
    const error = this.props.form.error ? (
      <div className="palette-Red-500 text">
        {this.props.form.error}
      </div>
    ) : null
    if (this.props.RegisterContainer.process) {
      return (
        <AuthProcessing message={this.props.RegisterContainer.process}/>
      );
    }
    return (
      <div className="l-block toggled" id="l-register">
        <Helmet
          title="RegisterContainer"
          meta={[
            {name: 'description', content: 'Description of RegisterContainer'},
          ]}
        />
        <div className="lb-header palette-Orange bg">
          <i className="zmdi zmdi-account-circle"></i>
          Create an account
        </div>

        <div className="lb-body">
          {error}

          <Input
            name="name"
            type="text"
            label="Name"
            placeholder="Name"
            value={this.props.form.name.value}
            onChange={this.props.changeField}
            error={this.props.form.name.error}
          />
          <Input
            name="email"
            type="email"
            label="Email Address"
            placeholder="Email Address"
            value={this.props.form.email.value}
            onChange={this.props.changeField}
            error={this.props.form.email.error}
          />
          <Input
            name="password"
            type="password"
            label="Password"
            placeholder="Password"
            value={this.props.form.password.value}
            onChange={this.props.changeField}
            error={this.props.form.password.error}
          />

          <button className="btn palette-Orange bg" onClick={this.onSubmit}>Create Account</button>

          <div className="m-t-30">
            <Link to="login" data-block="#l-login" data-bg="orange" className="palette-Orange text d-block m-b-5"
                  href="">Already have an
              account?</Link>
            <Link to="forgot" data-block="#l-forget-password" data-bg="purple" href="" className="palette-Orange text">Forgot
              password?</Link>
          </div>
        </div>
      </div>
    );
  }
}

RegisterContainer.propTypes = {
  RegisterContainer: React.PropTypes.object.isRequired,
  changeField: React.PropTypes.func.isRequired,
  register: React.PropTypes.func.isRequired,
  form: React.PropTypes.shape({
    email: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    password: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    name: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    error: React.PropTypes.string.isRequired,
  }),
};

const mapStateToProps = createStructuredSelector({
  RegisterContainer: makeSelectRegisterContainer(),
  form: selectForm(),
});

function mapDispatchToProps(dispatch) {
  return {
    register: (form) => {
      const credentials = {
        email: form.email.value,
        password: form.password.value,
        name: form.name.value,
      }
      return dispatch(register(credentials));
    },
    changeField: (field, value) => dispatch(changeField(field, value)),
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  userIsNotAuthenticated
)(RegisterContainer);

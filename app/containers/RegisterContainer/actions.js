/*
 *
 * RegisterContainer actions
 *
 */

import {
  CHANGE_FIELD, REGISTER, REGISTER_SUCCEEDED, REGISTER_FAILED,
} from './constants';

export function changeField(field, value) {
  return {
    type: CHANGE_FIELD,
    field,
    value,
  };
}

export function register(form) {
  return {
    type: REGISTER,
    form
  };
}

export function registerSucceeded() {
  return {
    type: REGISTER_SUCCEEDED,
  };
}

export function registerFailed(error) {
  return {
    type: REGISTER_FAILED,
    error,
  };
}

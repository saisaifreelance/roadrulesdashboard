import { createSelector } from 'reselect';

/**
 * Direct selector to the registerContainer state domain
 */
const selectRegisterContainerDomain = () => (state) => state.get('registerContainer');

/**
 * Other specific selectors
 */

const selectForm = () => createSelector(
  selectRegisterContainerDomain(),
  (substate) => {
    return {
      name: substate.get('name').toJS(),
      email: substate.get('email').toJS(),
      password: substate.get('password').toJS(),
      error: substate.get('error'),
    };
  }
);


/**
 * Default selector used by RegisterContainer
 */

const makeSelectRegisterContainer = () => createSelector(
  selectRegisterContainerDomain(),
  (substate) => substate.toJS()
);

export default makeSelectRegisterContainer;
export {
  selectRegisterContainerDomain,
  selectForm,
};

import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';
import {
  REGISTER,
  REGISTER_FAILED,
  REGISTER_SUCCEEDED,
} from './constants';
import {
  registerFailed,
  registerSucceeded,
} from './actions';

export function firebaseCreateUserRequest(credentials, profile) {
  return getFirebase().createUser(credentials, profile)
    .then((some, respo) => {
      return some;
    });
  // .catch((error) => {
  //   return error;
  // });
}

export function* firebaseCreateUser(action) {
  const credentials = {
    email: action.form.email,
    password: action.form.password,
    signIn: true,
  }
  const profile = {
    name: action.form.name,
    email: action.form.email,
    username: action.form.email,
  }
  try {
    const user = yield call(firebaseCreateUserRequest.bind(undefined, credentials, profile));
    yield put(registerSucceeded(user));
  } catch (e) {
    yield put(registerFailed(e));
  }
}

// Individual exports for testing
export function* registerSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(REGISTER, firebaseCreateUser);
}

// All sagas to be loaded
export default [
  registerSaga,
];

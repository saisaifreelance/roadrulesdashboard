/*
 *
 * RegisterContainer constants
 *
 */

export const CHANGE_FIELD = 'app/RegisterContainer/CHANGE_FIELD';
export const REGISTER = 'app/RegisterContainer/REGISTER';
export const REGISTER_SUCCEEDED = 'app/RegisterContainer/REGISTER_SUCCEEDED';
export const REGISTER_FAILED = 'app/RegisterContainer/REGISTER_FAILED';

/*
 *
 * LoginContainer constants
 *
 */

export const CHANGE_FIELD = 'app/LoginContainer/CHANGE_FIELD';
export const LOGIN = 'app/LoginContainer/LOGIN';
export const LOGIN_SUCCEEDED = 'app/LoginContainer/LOGIN_SUCCEEDED';
export const LOGIN_FAILED = 'app/LoginContainer/LOGIN_FAILED';

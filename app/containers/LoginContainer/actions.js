/*
 *
 * LoginContainer actions
 *
 */

import {
  CHANGE_FIELD, LOGIN, LOGIN_SUCCEEDED, LOGIN_FAILED,
} from './constants';

export function changeField(field, value) {
  return {
    type: CHANGE_FIELD,
    field,
    value,
  };
}

export function login(form) {
  return {
    type: LOGIN,
    form
  };
}

export function loginSucceeded() {
  return {
    type: LOGIN_SUCCEEDED,
  };
}

export function loginFailed(error) {
  return {
    type: LOGIN_FAILED,
    error,
  };
}

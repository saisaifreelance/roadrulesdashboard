/*
 *
 * LoginContainer
 *
 */

import React, {PropTypes} from 'react';
import {compose} from 'redux';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {Link} from 'react-router';
import {createStructuredSelector} from 'reselect';
import makeSelectLoginContainer, {selectForm} from './selectors';
import {login, changeField} from './actions';
import Input from '../../components/Input';
import AuthProcessing from '../../components/AuthProcessing';
import {userIsNotAuthenticated} from '../../auth';

export class LoginContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    this.props.login(this.props.form);
  }

  render() {
    const error = this.props.form.error ? (
      <div className="palette-Red-500 text">
        {this.props.form.error}
      </div>
    ) : null
    if (this.props.LoginContainer.process) {
      return (
        <AuthProcessing message={this.props.LoginContainer.process}/>
      );
    }
    return (
      <div className="l-block toggled" id="l-login">
        <Helmet
          title="LoginContainer"
          meta={[
            {name: 'description', content: 'Description of LoginContainer'},
          ]}
        />
        <div className="lb-header palette-Orange bg">
          <i className="zmdi zmdi-account-circle"></i>
          Hi there! Please Sign in
        </div>

        <div className="lb-body">
          {error}
          <Input
            name="email"
            type="text"
            label="Email Address"
            placeholder="Email Address"
            value={this.props.form.email.value}
            onChange={this.props.changeField}
            error={this.props.form.email.error}
          />

          <Input
            name="password"
            type="password"
            label="Password"
            placeholder="Password"
            value={this.props.form.password.value}
            onChange={this.props.changeField}
            error={this.props.form.password.error}
          />

          <button className="btn palette-Orange bg" onClick={this.onSubmit}>Sign in</button>

          <div className="m-t-20">
            <Link
              to="/register"
              data-block="#l-register"
              data-bg="blue"
              className="palette-Orange text d-block m-b-5"
              href=""
            >Create an account</Link>
            <Link
              to="/forgot"
              data-block="#l-forget-password"
              data-bg="purple"
              href=""
              className="palette-Orange text"
            >Forgot
              password?</Link>
          </div>
        </div>
      </div>
    );
  }
}

LoginContainer.propTypes = {
  LoginContainer: React.PropTypes.object.isRequired,
  changeField: React.PropTypes.func.isRequired,
  login: React.PropTypes.func.isRequired,
  form: React.PropTypes.shape({
    email: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    password: React.PropTypes.shape({
      value: React.PropTypes.string.isRequired,
      error: React.PropTypes.string.isRequired,
    }).isRequired,
    error: React.PropTypes.string.isRequired,
  }),
};

const mapStateToProps = createStructuredSelector({
  LoginContainer: makeSelectLoginContainer(),
  form: selectForm(),
});

function mapDispatchToProps(dispatch) {
  return {
    login: (form) => {
      const credentials = {
        email: form.email.value,
        password: form.password.value,
      }
      return dispatch(login(credentials));
    },
    changeField: (field, value) => dispatch(changeField(field, value)),
  };
}

export default compose(
  userIsNotAuthenticated,
  connect(mapStateToProps, mapDispatchToProps),
)(LoginContainer);

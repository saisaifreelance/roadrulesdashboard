import {take, call, put, select} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {getFirebase} from 'react-redux-firebase';
import {
  LOGIN,
  LOGIN_FAILED,
  LOGIN_SUCCEEDED,
} from './constants';
import {
  loginFailed,
  loginSucceeded,
} from './actions';

export function firebaseLoginRequestion(form) {
  return getFirebase().login(form)
    .then((some, respo) => {
      return some;
    });
    // .catch((error) => {
    //   return error;
    // });
}

export function* firebaseLogin(action) {
  try {
    const user = yield call(firebaseLoginRequestion.bind(undefined, action.form));
    yield put(loginSucceeded(user));
  } catch (e) {
    yield put(loginFailed(e));
  }
}

// Individual exports for testing
export function* loginSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(LOGIN, firebaseLogin);
}

// All sagas to be loaded
export default [
  loginSaga,
];

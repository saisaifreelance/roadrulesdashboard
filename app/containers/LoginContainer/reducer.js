/*
 *
 * LoginContainer reducer
 *
 */

import {fromJS} from 'immutable';
import {
  CHANGE_FIELD, LOGIN, LOGIN_FAILED, LOGIN_SUCCEEDED,
} from './constants';

const initialState = fromJS({
  email: {
    value: '',
    error: '',
  },
  password: {
    value: '',
    error: '',
  },
  error: '',
  process: '',
});

function setError(state, action) {
  switch (action.error.code) {
    case 'auth/argument-error':
      return state.set('error', action.error.message);
    case 'auth/user-not-found':
      return state.set('error', action.error.message);
    case 'auth/invalid-email':
      return state.updateIn(['email', 'error'], () => action.error.message);
    default:
      return state.updateIn(['email', 'error'], () => action.error.message);
  }
}

function loginContainerReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_FIELD:
      return state.updateIn([action.field, 'value'], () => action.value).updateIn([action.field, 'error'], () => '');
    case LOGIN:
      return state.set('process', 'Logging in...');
    case LOGIN_SUCCEEDED:
      return state.set('process', 'LOGIN SUCCESS, REDIRECTING...');
    case LOGIN_FAILED:
      return setError(state, action).set('process', '');
    default:
      return state;
  }
}

export default loginContainerReducer;

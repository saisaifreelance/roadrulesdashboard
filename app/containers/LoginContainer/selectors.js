import {createSelector} from 'reselect';

/**
 * Direct selector to the loginContainer state domain
 */
const selectLoginContainerDomain = () => (state) => {
  return state.get('loginContainer');
};

const selectForm = () => createSelector(
  selectLoginContainerDomain(),
  (substate) => {
    return {
      email: substate.get('email').toJS(),
      password: substate.get('password').toJS(),
      error: substate.get('error'),
    };
  }
);

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoginContainer
 */

const makeSelectLoginContainer = () => createSelector(
  selectLoginContainerDomain(),
  (substate) => substate.toJS()
);


export default makeSelectLoginContainer;
export {
  selectLoginContainerDomain,
  selectForm,
};

/*
 *
 * ClientsContainer
 *
 */

import React, {PropTypes} from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import {firebaseConnect } from 'react-redux-firebase';
import Helmet from 'react-helmet';
import Card from 'react-md/lib/Cards/Card';
import makeSelectClientsContainer from './selectors';
import { sort, paginate } from './actions';
import DataTableComponent from '../../components/DataTableComponent';
import CardHeader from '../../components/CardHeader';
import TableActions from '../../components/TableActions';
import EditClients from '../../components/EditClients';
import '../../../node_modules/react-md/dist/react-md.amber-blue.min.css';
import { userIsAdmin, userIsAuthenticated } from '../../auth';

export class ClientsContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      selectedRows: {},
      count: 0,
      dialogVisible: false,
    };
    this.handleRowToggle = this.handleRowToggle.bind(this);
    this.openAddRowDialog = this.openAddRowDialog.bind(this);
    this.closeDialog = this.closeDialog.bind(this);

    this.submitRows = this.submitRows.bind(this);
  }
  handleRowToggle(row, toggled, count) {
    if (row === -1) {
      if (toggled) {
        const selectedRows = {};
        for (let i = 0; i < this.props.table.pageSize; i += 1) {
          selectedRows[`${this.props.table.data[i].id}`] = true;
        }
        return this.setState({ selectedRows });
      }

      const selectedRows = {}
      return this.setState({ selectedRows });
    }

    const { selectedRows: oldSelectedRows } = this.state;
    const client = this.props.table.data[row];
    if (toggled) {
      const selectedRows = Object.assign({}, oldSelectedRows, { [`${client.id}`]: true });
      return this.setState({ selectedRows });
    }
    const selectedRows = Object.assign({}, oldSelectedRows);
    delete selectedRows[`${client.id}`];
    return this.setState({ selectedRows });
  }
  reset() {

  }
  submitRows(e) {
    e.preventDefault();
  }
  removeSelected() {}
  openAddRowDialog() {
    this.setState({ dialogVisible: true });
  }
  closeDialog() {
    this.setState({ dialogVisible: false });
  }
  render() {
    return (
      <section id="content">
        <Helmet
          title="ClientsContainer"
          meta={[
            {name: 'description', content: 'Description of ClientsContainer'},
          ]}
        />
        <div className="container">
          <div className="c-header">
            <h2>Data Table</h2>
          </div>
          <Card tableCard>
            <TableActions
              title="Clients"
              mobile={this.props.mobile}
              count={Object.keys(this.state.selectedRows).length}
              reset={this.reset}
              removeSelected={this.removeSelected}
              openAddRowDialog={this.openAddRowDialog}
            />
            <DataTableComponent onRowToggle={this.handleRowToggle} {...this.props.table} sort={this.props.sort} paginate={this.props.paginate} />
            <EditClients clients={this.state.selectedRows} visible={this.state.dialogVisible} closeDialog={this.closeDialog} submitRows={this.submitRows} mobile={this.props.mobile} />
          </Card>
        </div>
      </section>
    );
  }
}

ClientsContainer.propTypes = {
  table: React.PropTypes.shape({
    data: React.PropTypes.Array,
    loaded: React.PropTypes.bool,
    empty: React.PropTypes.bool,
    schema: React.PropTypes.Array,
    sorted: React.PropTypes.string,
    direction: React.PropTypes.oneOf(['asc', 'desc']),
    count: React.PropTypes.number,
    pageSize: React.PropTypes.number,
    page: React.PropTypes.number,
    start: React.PropTypes.number,
    startAt: React.PropTypes.number,
    endAt: React.PropTypes.string,
  }),
  sort: React.PropTypes.func.isRequired,
  paginate: React.PropTypes.func.isRequired,
  mobile: React.PropTypes.bool,
};

ClientsContainer.defaultProps = {
  mobile: false,
}

const mapStateToProps = makeSelectClientsContainer();

function mapDispatchToProps(dispatch, props) {
  return {
    sort: (sorted, direction) => dispatch(sort(sorted, direction)),
    paginate: (start, pageSize, currentPage) => dispatch(paginate(start, pageSize, currentPage)),
  };
}


export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect((props) => {
    const queryParams = [`orderByChild=${props.table.sorted}`];

    if (!props.table.endAt && !props.table.startAt) {
      queryParams.push(`limitToFirst=${props.table.pageSize + 1}`);
    }

    if (props.table.startAt) {
      queryParams.push(`limitToFirst=${props.table.pageSize + 1}`);
      queryParams.push(`startAt=${props.table.startAt}`);
    }

    if (props.table.endAt) {
      queryParams.push(`limitToLast=${props.table.pageSize + 1}`);
      queryParams.push(`endAt=${props.table.endAt}`);
    }
    return ([
      {
        path: '/learners',
        queryParams,
      },
      {
        path: '/meta_learners',
      },
    ]);
  }),
  userIsAuthenticated
)(ClientsContainer);

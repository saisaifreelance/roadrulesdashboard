import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, orderedToJS, toJS} from 'react-redux-firebase';

/**
 * Direct selector to the clientsContainer state domain
 */
const selectClientsContainerDomain = (state) => state.get('clientsContainer');

/**
 * Other specific selectors
 */
const selectTable = createSelector(
  selectClientsContainerDomain,
  (clientsContainerDomain) => {
    const clientsContainer = clientsContainerDomain.toJS();
    const { metaData, data, isFetchingClients, isFetchingMetaClients, startAt, endAt, schema, sorted, direction, pageSize, start } = clientsContainer;
    const page = Math.floor(start / pageSize) + 1
    return {
      isFetching: isFetchingClients || isFetchingMetaClients,
      empty: !data || !data.length,
      count: metaData ? metaData.count : 0,
      data: data ? data.slice(0, pageSize).map((client) => {
        const { name, id, phoneNumber, location, activationCode, createdAt, modifiedAt } = client
        return {
          name, id, phoneNumber, location, activationCode,
          createdAt: (new Date(createdAt)).toDateString(),
          modifiedAt: (new Date(modifiedAt)).toDateString()
        };
      }) : data,
      page,
      startAt,
      endAt,
      schema,
      sorted,
      direction,
      pageSize,
      start,
    };
  }
);


/**
 * Default selector used by ClientsContainer
 */

const makeSelectClientsContainer = () => createSelector(
  selectClientsContainerDomain,
  selectTable,
  (substate, table) => ({
    table,
  })
);

export default makeSelectClientsContainer;
export {
  selectClientsContainerDomain,
};

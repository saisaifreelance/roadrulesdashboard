
import { fromJS } from 'immutable';
import clientsContainerReducer from '../reducer';

describe('clientsContainerReducer', () => {
  it('returns the initial state', () => {
    expect(clientsContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

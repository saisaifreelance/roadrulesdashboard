import { take, call, put, select } from 'redux-saga/effects';
import { takeLatest } from 'redux-saga';

import {
  paginateSuccess,
  paginateFail,
} from './actions';

import {
  PAGINATE,
} from './constants';

function* paginate(action, store) {
  return Promise.resolve('success');
}

// Individual exports for testing
export function* paginateSaga() {
  // See example in containers/HomePage/sagas.js
  yield* takeLatest(PAGINATE, paginate);
}

// All sagas to be loaded
export default [
  paginateSaga,
];

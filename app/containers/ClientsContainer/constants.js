/*
 *
 * ClientsContainer constants
 *
 */

export const PAGINATE = 'app/ClientsContainer/PAGINATE';
export const PAGINATE_SUCCESS = 'app/ClientsContainer/PAGINATE_SUCCESS';
export const PAGINATE_FAIL = 'app/ClientsContainer/PAGINATE_FAIL';
export const SORT = 'app/ClientsContainer/SORT';

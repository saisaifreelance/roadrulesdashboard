
import { fromJS } from 'immutable';
import paidMonthContainerReducer from '../reducer';

describe('paidMonthContainerReducer', () => {
  it('returns the initial state', () => {
    expect(paidMonthContainerReducer(undefined, {})).toEqual(fromJS({}));
  });
});

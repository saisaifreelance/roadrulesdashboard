/*
 *
 * PaidMonthContainer
 *
 */

import React, { PropTypes } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import { createStructuredSelector } from 'reselect';
import makeSelectPaidMonthContainer, { makeSelectPaidMonthSalesData, makeSelectPaidMonthSalesStatus, makeSelectOptions } from './selectors';
import ChartPie from '../../components/ChartPie';
import Loading from '../../components/Loading';

export class PaidMonthContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (this.props.isFetching) {
      return (
        <div className="card">
          <div className="card-header">
            <h2>Unpaid / Paid</h2>
          </div>
          <div
            style={{height: 200, width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}}
          >
            <Loading
              color="amber"
              message={this.props.isFetching}
            />
          </div>
        </div>
      );
    }
    return (
      <div className="card">
        <div className="card-header">
          <h2>Unpaid / Paid</h2>
        </div>
        <ChartPie className="card-body card-padding" data={this.props.data} {...this.props.options} />
      </div>
    );
  }
}

PaidMonthContainer.propTypes = {
  data: PropTypes.array,
  isFetching: PropTypes.string,
  options: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  paidMonthContainer: makeSelectPaidMonthContainer(),
  data: makeSelectPaidMonthSalesData(),
  isFetching: makeSelectPaidMonthSalesStatus(),
  options: makeSelectOptions(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firebaseConnect(() => ([
    {
      path: '/stats_monthly',
      queryParams: ['orderByKey', 'limitToLast=12'],
    },
  ]))
)(PaidMonthContainer);

import {createSelector} from 'reselect';
import {firebaseConnect, isLoaded, isEmpty, dataToJS, pathToJS, toJS, orderedToJS} from 'react-redux-firebase';

const getDataStatus = (firebase, path) => {
  const requested = toJS(firebase.get('requested'));
  const requesting = toJS(firebase.get('requesting'));
  // const timestamp = toJS(firebase.get('timestamp'));
  if (requested[path]) {
    return null;
  }
  if (requesting[path]) {
    return 'Fetching data...';
  }
  return 'Data not loaded';
};

function getMonthStartTime(time) {
  const date = new Date(time);
  date.setDate(1);
  date.setHours(0, 0, 0, 0);
  return date.getTime();
}

const defaultPeriodStats = {
  count: 0,
  paid_learner: 0,
  paid_driver: 0,
};

/**
 * Direct selector to the paidMonthContainer state domain
 */
const selectPaidMonthContainerDomain = () => (state) => state.get('paidMonthContainer');

/**
 * Other specific selectors
 */

const makeSelectFirebase = () => (state) => state.get('firebase');


/**
 * Default selector used by PaidMonthContainer
 */

const makeSelectPaidMonthContainer = () => createSelector(
  selectPaidMonthContainerDomain(),
  (substate) => substate.toJS()
);

const makeSelectPaidMonthSalesData = () => createSelector(
  makeSelectFirebase(),
  makeSelectPaidMonthSalesStatus(),
  (firebase, isFetching) => {
    if (isFetching) {
      return null;
    }
    const statsPaidMonthly = dataToJS(firebase, 'stats_monthly');
    const month = getMonthStartTime((new Date()).getTime());
    const statsPaidMonth = statsPaidMonthly[month] ? statsPaidMonthly[month] : defaultPeriodStats;

    return [
      { data: statsPaidMonth.count, color: '#00FF00', label: 'Total Downloaded' },
      { data: statsPaidMonth.paid_learner, color: '#03A9F4', label: 'Paid For Learner' },
      { data: statsPaidMonth.paid_driver, color: '#F44336', label: 'Paid For Driver' },
    ];
  }
);

const makeSelectPaidMonthSalesStatus = () => createSelector(
  makeSelectFirebase(),
  (firebase) => getDataStatus(firebase, 'stats_monthly')
);

const makeSelectOptions = () => createSelector(
  () => ({
    series: {
      pie: {
        show: true,
        stroke: {
          width: 2,
        },
      },
    },
    legend: {
      container: '.flc-pie',
      backgroundOpacity: 0.5,
      noColumns: 0,
      backgroundColor: 'white',
      lineWidth: 0,
    },
    grid: {
      hoverable: true,
      clickable: true,
    },
    tooltipOpts: {
      show: true,
      content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
      shifts: {
        x: 20,
        y: 0,
      },
      defaultTheme: false,
      cssClass: 'flot-tooltip',
    },
  })
);

export default makeSelectPaidMonthContainer;
export {
  selectPaidMonthContainerDomain,
  makeSelectPaidMonthSalesData,
  makeSelectPaidMonthSalesStatus,
  makeSelectOptions,
};

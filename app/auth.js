import locationHelperBuilder from 'redux-auth-wrapper/history3/locationHelper';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history3/redirect';
import { routerActions } from 'react-router-redux';
import { pathToJS } from 'react-redux-firebase';

import Loading from './components/Loading';

const locationHelper = locationHelperBuilder({})

export const userIsAuthenticated = connectedRouterRedirect({
  redirectPath: '/login',
  authenticatedSelector: state => {
    const firebase = state.get('firebase');
    const auth = pathToJS(firebase, 'auth');
    return !!auth;
  },
  authenticatingSelector: state => {
    const firebase = state.get('firebase');
    const auth = pathToJS(firebase, 'auth');
    return typeof auth !== 'object';
  },
  AuthenticatingComponent: Loading,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticated',
});

export const userIsAdmin = connectedRouterRedirect({
  redirectPath: '/',
  allowRedirectBack: false,
  authenticatedSelector: state => {
    const firebase = state.get('firebase');
    const auth = pathToJS(firebase, 'auth');
    return auth !== null;
  },
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAdmin',
})

export const userIsNotAuthenticated = connectedRouterRedirect({
  redirectPath: (state, ownProps) => locationHelper.getRedirectQueryParam(ownProps) || '/',
  allowRedirectBack: false,
  // Want to redirect the user when they are done loading and authenticated
  authenticatedSelector: (state) => {
    const firebase = state.get('firebase');
    const auth = pathToJS(firebase, 'auth');
    return !auth;
  },
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsNotAuthenticated',
})

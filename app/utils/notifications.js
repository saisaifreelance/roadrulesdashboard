import '../../vendors/bootstrap-growl/bootstrap-growl.min';
export function notify(message, type, options = {}) {
  $.growl(
    {
      message,
    },
    {
      type,
      allow_dismiss: false,
      label: 'Cancel',
      className: 'btn-xs btn-inverse',
      placement: {
        from: 'bottom',
        align: 'right',
      },
      delay: 2500,
      animate: {
        enter: 'animated fadeInRight',
        exit: 'animated fadeOutRight',
      },
      offset: {
        x: 30,
        y: 30,
      },
      ...options,
    });
};

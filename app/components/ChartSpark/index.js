/**
*
* ChartSpark
*
*/

import React from 'react';
import '../../../vendors/sparklines/jquery.sparkline.min.js';

import styles from './styles.css';

/*----------------------------------------------------------
 Sparkline
 -----------------------------------------------------------*/
function sparklineBar(id, value, height, barWidth, barColor, barSpacing) {
  $('.'+id).sparkline(value, {
    type: 'bar',
    height,
    barWidth,
    barColor,
    barSpacing,
  })
}

function sparklineLine(id, value, width, height, lineColor, fillColor, lineWidth, maxSpotColor, minSpotColor, spotColor, spotRadius, hSpotColor, hLineColor) {
  $('.'+id).sparkline(value, {
    type: 'line',
    width,
    height,
    lineColor,
    fillColor,
    lineWidth,
    maxSpotColor,
    minSpotColor,
    spotColor,
    spotRadius,
    highlightSpotColor: hSpotColor,
    highlightLineColor: hLineColor,
  });
}

class ChartSpark extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    if ($('.sparkline-1')[0]) {
      sparklineLine('sparkline-1', [0,0,0,0,0,0,0,0,0,0,0,4,9], '100%', 50, 'rgba(255,255,255,0.6)', 'rgba(0,0,0,0)', 1.5, '#fff', '#fff', '#fff', 5, '#fff', '#fff');
    }
  }

  render() {
    return (
      <div className={`card c-dark palette-Light-Blue bg ${styles.chartSpark}`}>
        <div className="card-header">
          <h2>Tests started
            <small>Number of times users have started tests in the past week</small>
          </h2>

          <ul className="actions a-alt">
            <li className="dropdown">
              <a href="" data-toggle="dropdown">
                <i className="zmdi zmdi-more-vert"></i>
              </a>

              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <a href="">Change Date Range</a>
                </li>
                <li>
                  <a href="">Change Graph Type</a>
                </li>
                <li>
                  <a href="">Other Settings</a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <div className="card-body card-padding">
          <h2 className="m-t-0 m-b-15 c-white">
            <i className="zmdi zmdi-caret-up-circle m-r-5"></i>
            13
          </h2>

          <div className="sparkline-1 text-center"></div>
        </div>
      </div>
    );
  }
}

export default ChartSpark;

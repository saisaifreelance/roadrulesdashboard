/**
 *
 * ChartSettings
 *
 */

import React from 'react';
import classNames from 'classnames';
import TextField from 'react-md/lib/TextFields';

class ChartSettings extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    targets: React.PropTypes.objectOf(React.PropTypes.number.isRequired).isRequired,
    className: React.PropTypes.string,
    onSubmit: React.PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      targets: props.targets,
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange(field, value) {
    const targets = { ...this.state.targets, [field]: value }
    this.setState({targets});
  }
  onSubmit() {
    this.props.onSubmit(this.state.targets);
  }
  render() {
    const inputs = Object.keys(this.state.targets).map((key) => (
      <TextField
        id={`floatingCenterTitle-${key}`}
        key={key}
        value={this.state.targets[key]}
        onChange={(e) => {
          this.onChange(key, e.target.value);
        }} type="number"
        label={`Set ${key}`}
        lineDirection="center"
        placeholder={`Set ${key}`}
        className="md-cell md-cell--12"
      />
    ));
    return (
      <div className={classNames({ [this.props.className]: this.props.className })}>
        <div className="form-group fg-float m-b-30">
          <h6>Enter Target</h6>
          <div className="fg-line">{inputs}</div>
        </div>

        <div className="clearfix"></div>

        <div className="m-t-20">
          <button className="btn btn-info" onClick={this.onSubmit}>Update</button>
        </div>
      </div>
    );
  }
}

export default ChartSettings;

/**
 *
 * ChartDynamic
 *
 */

import React from 'react';
import ReactFlot from 'react-flot';
import '../../../node_modules/react-flot/flot/jquery.flot.time.min.js';
import '../../../node_modules/react-flot/flot/jquery.flot.resize.js';

import styles from './styles.css';

/* Make some random data*/

var data = [];
var totalPoints = 300;
var updateInterval = 30;

function getRandomData() {
  if (data.length > 0)
    data = data.slice(1);

  while (data.length < totalPoints) {

    var prev = data.length > 0 ? data[data.length - 1] : 50,
      y = prev + Math.random() * 10 - 5;
    if (y < 0) {
      y = 0;
    } else if (y > 90) {
      y = 90;
    }

    data.push(y);
  }

  var res = [];
  for (var i = 0; i < data.length; ++i) {
    res.push([i, data[i]])
  }

  return res;
}

const options = {
  series: {
    label: 'Server Process Data',
    lines: {
      show: true,
      lineWidth: 0.2,
      fill: 0.6,
    },

    color: '#00BCD4',
    shadowSize: 0,
  },
  yaxis: {
    min: 0,
    max: 100,
    tickColor: '#eee',
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,

  },
  xaxis: {
    tickColor: '#eee',
    show: true,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
    min: 0,
    max: 250,
  },
  grid: {
    borderWidth: 1,
    borderColor: '#eee',
    labelMargin: 10,
    hoverable: true,
    clickable: true,
    mouseActiveRadius: 6,
  },
  legend: {
    container: '.flc-dynamic',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
  },
}

class ChartDynamic extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      data: [getRandomData()],
    };
  }
  componentDidMount() {
  }
  update() {
    this.setState({ data: [getRandomData()] });
    setTimeout(this.update, updateInterval);
  }
  render() {
    return (
      <div className={`card ${styles.chartDynamic}`}>
        <div className="card-header">
          <h2>User activity</h2>
        </div>

        <div className="card-body card-padding-sm">
          <ReactFlot id="dynamic-chart" className="flot-chart" options={options} data={this.state.data} width="100%" height="200px" />
          <div className="flc-dynamic"></div>
        </div>
      </div>
    );
  }
}

export default ChartDynamic;

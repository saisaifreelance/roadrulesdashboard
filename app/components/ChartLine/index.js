/**
 *
 * ChartLine
 *
 */

import React from 'react';
import classNames from 'classnames';
import ReactFlot from 'react-flot';
import '../../../node_modules/react-flot/flot/jquery.flot.time.min';
import '../../../node_modules/react-flot/flot/jquery.flot.resize';
import '../../../vendors/bower_components/flot.curvedlines/curvedLines';

import styles from './styles.css';

class ChartLine extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    className: React.PropTypes.string,
    grid: React.PropTypes.object,
    yaxis: React.PropTypes.object,
    xaxis: React.PropTypes.object,
    legend: React.PropTypes.object,
    data: React.PropTypes.array.isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      id: Date.now(),
    };
  }

  componentDidMount() {
    /* Tooltips for Flot Charts */
    const chart = $(`#${this.state.id}`);
    if (chart[0]) {
      chart.bind('plothover', (event, pos, item) => {
        if (item) {
          let x = item.datapoint[0].toFixed(2);
          let y = item.datapoint[1].toFixed(2);
          try {
            const ticks = item.series.xaxis.ticks;
            const index = Math.floor(x);
            const tick = ticks[index].label;
            x = tick;
          } catch (e) {}
          $('.flot-tooltip').html(`${item.series.label} | x: ${x} y: ${y}`).css({
            top: item.pageY + 5,
            left: item.pageX + 5,
          }).show();
        }
        else {
          $('.flot-tooltip').hide();
        }
      });

      $('<div class="flot-tooltip chart-tooltip"></div>').appendTo('body');
    }
  }

  render() {
    const options = {
      series: {
        shadowSize: 0,
        curvedLines: {
          apply: true,
          active: true,
          monotonicFit: true,
        },
        lines: {
          show: true,
          lineWidth: 2,
        },
      },
      grid: this.props.grid,
      yaxis: this.props.yaxis,
      xaxis: this.props.xaxis,
      legend: this.props.legend,
    }
    return (
      <div className={classNames('chart-edge', {[this.props.className]: this.props.className})}>
        <ReactFlot
          id={`${this.state.id}`}
          className="flot-chart"
          options={options}
          data={this.props.data} width="100%"
          height="200px"/>
          <div className="flc-bar"></div>
      </div>
    );
  }
}

ChartLine.defaultProps = {
  grid: {
    borderWidth: 0,
    labelMargin: 10,
    hoverable: true,
    clickable: true,
    mouseActiveRadius: 6,
    show: true,
  },
  yaxis: {
    tickColor: '#eee',
    tickDecimals: 0,
    font:{
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
  },
}


export default ChartLine;

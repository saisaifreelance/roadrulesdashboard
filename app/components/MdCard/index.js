/**
*
* MdCard
*
*/

import React from 'react';
import { Card, CardTitle, CardText, CardActions, Button } from 'react-md';


import styles from './styles.css';
import './md.scss';

function MdCard() {
  return (
    <div className={styles.mdCard}>
      <div className="md-grid">
        <Card className="md-cell">
          <CardTitle title="Hello, World!" />
          <CardText>
            Lorem ipsum... pretend more ...
          </CardText>
          <CardActions>
            <Button flat label="Action 1" />
            <Button flat label="Action 2" />
          </CardActions>
        </Card>
      </div>
    </div>
  );
}

export default MdCard;

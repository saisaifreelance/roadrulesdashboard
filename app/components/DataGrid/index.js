/**
 *
 * DataGrid
 *
 */

import React from 'react';
import '../../../vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js';

import styles from './styles.css';

/*----------------------------------------------------------
 Easy Pie Charts
 -----------------------------------------------------------*/
function easyPieChart(id, barColor, trackColor, scaleColor, lineWidth, size) {
  $('.'+id).easyPieChart({
    easing: 'easeOutBounce',
    barColor: barColor,
    trackColor: trackColor,
    scaleColor: scaleColor,
    lineCap: 'square',
    lineWidth: lineWidth,
    size: size,
    animate: 3000,
    onStep: function(from, to, percent) {
      $(this.el).find('.percent').text(Math.round(percent));
    }
  });
}

class DataGrid extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    easyPieChart('easy-pie-1', 'rgba(255,255,255,0.8)', 'rgba(0,0,0,0.08)', 'rgba(0,0,0,0)', 3, 150);
    easyPieChart('easy-pie-2', '#fff', 'rgba(0,0,0,0.08)', 'rgba(0,0,0,0)', 2, 75);
    easyPieChart('easy-pie-3', '#fff', 'rgba(0,0,0,0.08)', 'rgba(0,0,0,0)', 2, 75);
    easyPieChart('easy-pie-4', '#fff', 'rgba(0,0,0,0.08)', 'rgba(0,0,0,0)', 2, 75);
  }
  render() {
    return (
      <div className={`card palette-Red-400 bg ${styles.dataGrid}`}>
        <div className="pie-grid clearfix text-center">
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-2 easy-pie" data-percent="92">
              <span className="ep-value">92</span>
            </div>
            <div className="pgi-title">Email<br /> Scheduled</div>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-3 easy-pie" data-percent="11">
              <span className="ep-value">11</span>
            </div>
            <div className="pgi-title">Email<br /> Bounced</div>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-4 easy-pie" data-percent="52">
              <span className="ep-value">52</span>
            </div>
            <div className="pgi-title">Email<br /> Opened</div>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-2 easy-pie" data-percent="44">
              <span className="ep-value">44</span>
            </div>
            <div className="pgi-title">Storage<br />Remaining</div>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-3 easy-pie" data-percent="78">
              <span className="ep-value">78</span>
            </div>
            <div className="pgi-title">Web Page<br /> Views</div>
          </div>
          <div className="col-xs-4 col-sm-6 col-md-4 pg-item">
            <div className="easy-pie-4 easy-pie" data-percent="32">
              <span className="ep-value">32</span>
            </div>
            <div className="pgi-title">Server<br /> Processing</div>
          </div>
        </div>
      </div>
    );
  }
}

export default DataGrid;

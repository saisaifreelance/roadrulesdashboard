/**
 *
 * DataTable
 *
 */

import React from 'react';
import DataTable from 'react-md/lib/DataTables/DataTable';

import Configuration from '../DataTableConfiguration';
import Header from '../DataTableHeader';
import Body from '../DataTableBody';
import Pagination from '../DataTablePagination';
import Loading from '../Loading';

function DataLoading({ message }) {
  return (
    <div className="card-padding">
      <Loading color={'amber'} message={message} />
    </div>
  );
}

function DataEmpty() {
  return (
    <div>No data found matching query</div>
  );
}

function DataError() {
  return (
    <div>Data error</div>
  );
}


class DataTableComponent extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (this.props.isFetching) {
      return (
        <DataLoading message={this.props.isFetching} />
      );
    }

    if (this.props.empty) {
      return <DataEmpty />;
    }

    return (
      <DataTable
        baseId="data"
        onRowToggle={this.props.onRowToggle}
      >
        <Header
          schema={this.props.schema}
          sorted={this.props.sorted}
          direction={this.props.direction}
          sort={this.props.sort}
        />
        <Body
          data={this.props.data}
          schema={this.props.schema}
          inline={this.props.inline}
          large={this.props.large}
          saveOnOutsideClick={this.props.saveOnOutsideClick}
        />
        <Pagination
          paginate={this.props.paginate}
          pageSize={this.props.pageSize}
          count={this.props.count}
          page={this.props.page}
        />
      </DataTable>
    );
  }
}

DataTableComponent.propTypes = {
  data: React.PropTypes.array,
  isFetching: React.PropTypes.string,
  empty: React.PropTypes.bool,
  schema: React.PropTypes.array,
  sorted: React.PropTypes.string,
  direction: React.PropTypes.oneOf(['asc', 'desc']),
  count: React.PropTypes.number,
  pageSize: React.PropTypes.number,
  page: React.PropTypes.number,
  start: React.PropTypes.number,
  sort: React.PropTypes.func.isRequired,
  paginate: React.PropTypes.func.isRequired,

  inline: React.PropTypes.bool,
  large: React.PropTypes.bool,
  saveOnOutsideClick: React.PropTypes.bool,
  onRowToggle: React.PropTypes.func.isRequired,
}

DataTableComponent.defaultProps = {
  inline: false,
  large: false,
  saveOnOutsideClick: false,
}

export default DataTableComponent;

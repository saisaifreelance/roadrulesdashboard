/**
*
* IconOrFlat
*
*/

import React, { PropTypes } from 'react';
import Button from 'react-md/lib/Buttons/Button';
// import styled from 'styled-components';


function IconOrFlat({ mobile, label, ...props }) {
  return (
    <Button
      icon={mobile}
      flat={!mobile}
      label={mobile ? null : label}
      primary
      tooltipLabel={mobile ? label : null}
      {...props}
    />
  );
}

IconOrFlat.propTypes = {
  mobile: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
};

export default IconOrFlat;

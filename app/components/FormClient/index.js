/**
 *
 * FormClient
 *
 */

import React from 'react';
// import styled from 'styled-components';
import SelectField from 'react-md/lib/SelectFields';
import TextField from 'react-md/lib/TextFields';
import SelectionControl from 'react-md/lib/SelectionControls/SelectionControl';
import DialogContainer from 'react-md/lib/Dialogs/DialogContainer';
import Button from 'react-md/lib/Buttons/Button';
import CardPayment from '../../components/CardPayment';
import CardAddPayment from '../../components/CardAddPayment';

const LOCATIONS = [
  {
    value: "other",
    label: "Select your province",

  },
  {
    value: "bulawayo",
    label: "Bulawayo",

  },
  {
    value: "harare",
    label: "Harare",

  },
  {
    value: "manicaland",
    label: "Manicaland",

  },
  {
    value: "mash_central",
    label: "Mash Central",

  },
  {
    value: "mash_east",
    label: "Mash East",

  },
  {
    value: "mash_west",
    label: "Mash West",

  },
  {
    value: "masvingo",
    label: "Masvingo",

  },
  {
    value: "mat_north",
    label: "Mat North",

  },
  {
    value: "mat_south",
    label: "Mat South",

  },
  {
    value: "midlands",
    label: "Midlands",

  },
];

const dataFlat = {
  id: '',
  phone_number: '',
  firstname: '',
  lastname: '',
  location: '',
  imei: '',
  firebase_token: '',
  activation_code_learner: '',
  activation_code_driver: '',
  payment_learner: '',
  payment_driver: '',
  date_created: '',
  date_modified: '',
  date_paid_learner: '',
  date_paid_driver: '',
};

const dataPopulated = {
  id: '',
  phone_number: '',
  firstname: '',
  lastname: '',
  location: '',
  imei: '',
  firebase_token: '',
  activation_code_learner: '',
  activation_code_driver: '',
  payment_learner: {
    date_created: '',
    merchant_code: '',
    payment_provider: '',
    txn: '',
    amount: '',
    client_verified: '',
    merchant_verified: '',
  },
  payment_driver: {
    date_created: '',
    merchant_code: '',
    payment_provider: '',
    txn: '',
    amount: '',
    client_verified: '',
    merchant_verified: '',
  },
  payments: {},
  date_created: '',
  date_modified: '',
  date_paid_learner: '',
  date_paid_driver: '',
};

const paymentShape = React.PropTypes.shape({
  date_created: React.PropTypes.number,
  merchant_code: React.PropTypes.string,
  payment_provider: React.PropTypes.string,
  product: React.PropTypes.string,
  id: React.PropTypes.string,
  txn: React.PropTypes.string,
  client: React.PropTypes.string,
  amount: React.PropTypes.number,
  client_verified: React.PropTypes.number,
  merchant_verified: React.PropTypes.number,
});

const clientShape = React.PropTypes.shape({
  id: React.PropTypes.string.isRequired,
  phone_number: React.PropTypes.string.isRequired,
  firstname: React.PropTypes.string.isRequired,
  lastname: React.PropTypes.string.isRequired,
  location: React.PropTypes.string,
  imei: React.PropTypes.string,
  firebase_token: React.PropTypes.string,
  activation_code_learner: React.PropTypes.string,
  activation_code_driver: React.PropTypes.string,
  payment_learner: paymentShape,
  payment_driver: paymentShape,
  date_created: React.PropTypes.number,
  date_modified: React.PropTypes.number,
  date_paid_learner: React.PropTypes.number,
  date_paid_driver: React.PropTypes.number,
  payments: React.PropTypes.object,
});

class FormClient extends React.Component {
  static propTypes = {
    index: React.PropTypes.number.isRequired,
    client: clientShape.isRequired,
    client_errors: React.PropTypes.object,
    onEditClient: React.PropTypes.func.isRequired,
    onEditClientError: React.PropTypes.func.isRequired,
    onActivate: React.PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props)
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);
    this.acivateSendCode = this.acivateSendCode.bind(this);
    this.acivateNoSendCode = this.acivateNoSendCode.bind(this);
    this.state = { visible: false };
  }
  show() {
    this.setState({ visible: true });
  }
  hide() {
    this.setState({ visible: false });
  }
  acivateSendCode() {
    this.setState({ visible: false });
    this.props.onActivate(this.props.client.id, this.props.client.activationCode);
  }
  acivateNoSendCode() {
    this.setState({ visible: false });
    this.props.onActivate(this.props.client.id, null);
  }
  render() {
    const { index, client, client_errors } = this.props
    const actions = [
      { secondary: true, children: 'No', onClick: this.acivateNoSendCode },
      (<Button flat primary onClick={this.acivateSendCode}>Yes</Button>),
    ];
    return (
      <section className="md-grid" aria-labelledby={`new-row-group-${index + 1}`}>
        <h2 id={`new-row-group-${index + 1}`} className="md-cell md-cell--12">Edit Client {client.id}</h2>
        <TextField
          id={`name-${index}`}
          name={`name-${index}`}
          label="Name"
          defaultValue={client.name}
          placeholder="Name"
          className="md-cell"
          error={!!client_errors.name}
          errorText={client_errors.name}
        />
        <TextField
          id={`phoneNumber-${index}`}
          name={`phoneNumber-${index}`}
          label="Phone number"
          defaultValue={client.phoneNumber}
          placeholder="Phone Number"
          className="md-cell"
          error={!!client_errors.phoneNumber}
          errorText={client_errors.phoneNumber}
        />

        <SelectField
          id={`location-${index}`}
          name={`location-${index}`}
          label="Location"
          itemLabel="label"
          itemValue="value"
          menuItems={LOCATIONS}
          defaultValue={client.location}
          className="md-cell md-cell--bottom"
          error={!!client_errors.location}
          errorText={client_errors.location}
        />
        <TextField
          id={`activationCode-${index}`}
          name={`activationCode-${index}`}
          label="Learner Activation Code"
          defaultValue={client.activationCode}
          placeholder="Learner Activation Code"
          className="md-cell"
          error={!!client_errors.activationCode}
          errorText={client_errors.activationCode}
        />
        <SelectionControl
          id="activatedAt"
          type="switch"
          label="Activated"
          name="activatedAt"
          defaultChecked={false}
          className="md-cell"
          checked={!!client.activatedAt}
          onChange={this.show}
          error={!!client_errors.activatedAt}
          errorText={client_errors.activatedAt}
        />
        <DialogContainer
          id="activate-dialog"
          visible={this.state.visible}
          onHide={this.hide}
          actions={actions}
          title="Send confirmation message?"
        >
        </DialogContainer>
      </section>
    );
  }

}

export default FormClient;

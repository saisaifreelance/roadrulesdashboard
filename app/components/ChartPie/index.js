/**
 *
 * ChartPie
 *
 */

import React from 'react';

import ReactFlot from 'react-flot';
import '../../../node_modules/react-flot/flot/jquery.flot.time.min.js';
import '../../../node_modules/react-flot/flot/jquery.flot.resize.js';
import '../../../vendors/bower_components/flot/jquery.flot.pie.js';
import '../../../vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js';

class ChartPie extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      id: `pie_${Date.now()}`,
      flcID: `flc_${Date.now()}`,
    };
  }
  componentDidMount() {

  }

  render() {
    const options = {
      series: this.props.series,
      legend: {...this.props.legend, container: `#${this.state.flcID}`},
      grid: this.props.grid,
      tooltip: this.props.tooltip,
    }
    return (
      <div className={this.props.className}>
        <ReactFlot id={this.state.id} className="flot-chart-pie" options={options} data={this.props.data} width="100%" height="200px" />
        <div id={this.state.flcID} className="flc-pie hidden-xs"></div>
      </div>
    );
  }
}

ChartPie.propTypes = {
  className: React.PropTypes.string,
  tooltip: React.PropTypes.object.isRequired,
  grid: React.PropTypes.object.isRequired,
  legend: React.PropTypes.object.isRequired,
  series: React.PropTypes.object.isRequired,
  data: React.PropTypes.arrayOf(React.PropTypes.shape({
    data: React.PropTypes.number.isRequired,
    label: React.PropTypes.string.isRequired,
    color: React.PropTypes.string.isRequired,
  })).isRequired,
};

ChartPie.defaultProps = {
  series: {
    pie: {
      show: true,
      stroke: {
        width: 2,
      },
    },
  },
  legend: {
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
  },
  grid: {
    hoverable: true,
    clickable: true,
  },
  tooltip: {
    show: true,
    content: '%p.0%, %s', // show percentages, rounding to 2 decimal places
    shifts: {
      x: 20,
      y: 0,
    },
    defaultTheme: false,
    cssClass: 'flot-tooltip',
  },
};

export default ChartPie;

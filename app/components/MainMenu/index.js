/**
*
* MainMenu
*
*/

import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';


class MainMenu extends React.PureComponent {
  static propTypes = {
    open: React.PropTypes.string,
    toggleMessages: React.PropTypes.func.isRequired,
    toggleAlerts: React.PropTypes.func.isRequired,
    toggleTasks: React.PropTypes.func.isRequired,
    location: React.PropTypes.shape({
      action: React.PropTypes.string,
      hash: React.PropTypes.string,
      key: React.PropTypes.string,
      pathname: React.PropTypes.string,
      query: React.PropTypes.object,
      search: React.PropTypes.string,
      state: React.PropTypes.object,
    }).isRequired,
    params: React.PropTypes.object.isRequired,
  };
  render() {
    return (
      <aside id="s-main-menu" className={classNames('sidebar', { toggled: this.props.open === 'main-menu' })}>
        <div className="smm-header">
          <i className="zmdi zmdi-long-arrow-left" data-ma-action="sidebar-close"></i>
        </div>

        <ul className="smm-alerts">
          <li onClick={this.props.toggleMessages} className={classNames({ toggled: this.props.open === 'messages' })} data-user-alert="sua-messages" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i className="zmdi zmdi-email"></i>
          </li>
          <li onClick={this.props.toggleAlerts} className={classNames({ toggled: this.props.open === 'alerts' })} data-user-alert="sua-notifications" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i className="zmdi zmdi-notifications"></i>
          </li>
          <li onClick={this.props.toggleTasks} className={classNames({ toggled: this.props.open === 'tasks' })} data-user-alert="sua-tasks" data-ma-action="sidebar-open" data-ma-target="user-alerts">
            <i className="zmdi zmdi-view-list-alt"></i>
          </li>
        </ul>

        <ul className="main-menu">
          <li className={classNames({active: this.props.location.pathname === '/'})}>
            <Link to="/"><i className="zmdi zmdi-home"></i> Home</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/clients'})}>
            <Link to="/clients"><i className="zmdi zmdi-account-circle"></i> Clients</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/questions'})}>
            <Link to="/questions"><i className="zmdi zmdi-file-text"></i> Questions</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/notes'})}>
            <Link to="/notes"><i className="zmdi zmdi-book"></i> Notes</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/driving_schools'})}>
            <Link to="/driving_schools"><i className="zmdi zmdi-local-taxi"></i> Driving schools</Link>
          </li>
        </ul>
      </aside>
    );
  }
}

/*

<li className={classNames({active: this.props.location.pathname === '/school_bookings'})}>
            <Link to="/school_bookings"><i className="zmdi zmdi-receipt"></i> Driving school bookings</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/test_bookings'})}>
            <Link to="/test_bookings"><i className="zmdi zmdi-ticket-star"></i>Provisional test bookings</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/push_notifications'})}>
            <Link to="/push_notifications"><i className="zmdi zmdi-notifications"></i>Push notifications</Link>
          </li>
          <li className={classNames({active: this.props.location.pathname === '/ads'})}>
            <Link to="/ads"><i className="zmdi zmdi-camera-roll"></i> Advertisements</Link>
          </li>

* */


export default MainMenu;

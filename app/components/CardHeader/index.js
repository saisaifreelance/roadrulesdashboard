/**
 *
 * CardHeader
 *
 */

import React from 'react';
import CardActions from '../CardActions';


import styles from './styles.css';

function CardHeader({ title, description, actions }) {
  return (
    <div className={`card-header ${styles.cardHeader}`}>
      <h2>{title}
        <small>{description}</small>
      </h2>
      <CardActions actions={actions} />
    </div>
  );
}

CardHeader.propTypes = {
  title: React.PropTypes.string.isRequired,
  description: React.PropTypes.string.isRequired,
  actions: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      icon: React.PropTypes.string,
      title: React.PropTypes.string,
      onClick: React.PropTypes.func,
      actions: React.PropTypes.arrayOf(
        React.PropTypes.shape({
          icon: React.PropTypes.string,
          title: React.PropTypes.string,
          onClick: React.PropTypes.func,
        })
      ),
    })
  ),
}

export default CardHeader;

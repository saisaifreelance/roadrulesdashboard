/**
*
* Button
*
*/

import React from 'react';
import classNames from 'classnames';
// import styled from 'styled-components';


function Button(props) {
  return (
    <button
      className={classNames(
        'btn',
        { [props.colorScheme]: props.colorScheme },
        { 'btn-default': !props.colorScheme },
        { [props.class]: props.class },
        { 'btn-icon-text': props.icon && props.children },
        { 'btn-icon': props.icon && !props.children },
        { 'btn-block': props.block },
        )}
      disabled={props.disabled}
      onClick={(e) => {
      if (props.onClick) {
        props.onClick();
      }
    }}>{ props.icon ? (<i className={`zmdi ${props.icon}`}></i>) : null}{props.children}</button>
  );
}

Button.propTypes = {
  colorScheme: React.PropTypes.oneOf(['btn-default', 'btn-info', 'btn-primary', 'btn-success', 'btn-warning', 'btn-danger']),
  size: React.PropTypes.oneOf(['btn-lg', 'btn-sm', 'btn-xs']),
  class: React.PropTypes.string,
  onClick: React.PropTypes.func,
  disabled: React.PropTypes.bool,
  block: React.PropTypes.bool,
  icon: React.PropTypes.string,
  children: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element, React.PropTypes.arrayOf(React.PropTypes.element)]),
};

export default Button;

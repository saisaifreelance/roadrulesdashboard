/**
 *
 * DataTableConfiguration
 *
 */

import React from 'react';
import CardText from 'react-md/lib/Cards/CardText';
import SelectionControl from 'react-md/lib/SelectionControls/SelectionControl';
import SelectionControlGroup from 'react-md/lib/SelectionControls/SelectionControlGroup';

import styles from './styles.css';
// import './style.scss';

function DataTableConfiguration({
                                  sorted,
                                  onSortChange,
                                  dialogChecked,
                                  onDialogChange,
                                  inlineChecked,
                                  onInlineChange,
                                  saveChecked,
                                  onSaveChange,
                                }) {
  return (
    <CardText className={styles.dataTableConfiguration}>
      <SelectionControl
        type="switch"
        id="configuration-use-edit-dialog"
        label="Use large Edit Dialog"
        name="edit-dialog"
        checked={dialogChecked}
        onChange={onDialogChange}
      />
      <SelectionControl
        type="switch"
        id="configuration-use-inline-edit"
        label="Edit inline"
        name="edit-inline"
        checked={inlineChecked}
        onChange={onInlineChange}
      />
      <SelectionControl
        type="switch"
        id="configuration-save-on-outside"
        label="Save data on outside click"
        name="save-on-outside"
        checked={saveChecked}
        onChange={onSaveChange}
      />
    </CardText>
  );
}

DataTableConfiguration.propTypes = {
  sorted: React.PropTypes.string.isRequired,
  onSortChange: React.PropTypes.func.isRequired,
  dialogChecked: React.PropTypes.bool.isRequired,
  onDialogChange: React.PropTypes.func.isRequired,
  inlineChecked: React.PropTypes.bool.isRequired,
  onInlineChange: React.PropTypes.func.isRequired,
  saveChecked: React.PropTypes.bool.isRequired,
  onSaveChange: React.PropTypes.func.isRequired,
};

export default DataTableConfiguration;

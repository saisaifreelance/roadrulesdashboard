/**
 *
 * Dropdown
 *
 */

import React from 'react';
// import styled from 'styled-components';
import classnames from 'classnames';


class Dropdown extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props);
    this.state = {
      open: false,
    };
    this.toggle = this.toggle.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }

  toggle(e) {
    e.preventDefault();
    this.setState({open: !this.state.open});
  }

  open(e) {
    e.preventDefault();
    this.setState({open: true});
  }

  close() {
    this.setState({open: false});
  }

  renderIcon(icon) {
    switch (icon.type) {
      case 'icon': {
        return (
          <i className={classnames('zmdi', icon.name, {[icon.class]: icon.class})}></i>
        );
      }
      case 'image': {
        return (
          <img src={icon.name} className={classnames({[icon.class]: icon.class})} alt=""/>
        );
      }
      default:
        return null;
    }
  }

  render() {
    return (
      <li className={classnames('dropdown', { open: this.state.open }, {[this.props.class]: this.props.class})}>
        <a href="" onClick={this.open}>
          {this.renderIcon(this.props.icon)}
        </a>
        <ul className={classnames('dropdown-menu pull-right', { [this.props.dropdown.class]: this.props.dropdown.class })}>
          {this.props.children}
        </ul>
      </li>
    );
  }
}

Dropdown.propTypes = {
  children: React.PropTypes.oneOfType([React.PropTypes.element, React.PropTypes.arrayOf(React.PropTypes.element)]),
  class: React.PropTypes.string,
  dropdown: React.PropTypes.shape({
    class: React.PropTypes.string,
  }),
  icon: React.PropTypes.shape({
    type: React.PropTypes.oneOf(['icon', 'image']),
    name: React.PropTypes.string.isRequired,
    class: React.PropTypes.string,
  }).isRequired,
  items: React.PropTypes.arrayOf(React.PropTypes.shape({
    class: React.PropTypes.string,
  })),

};

Dropdown.defaultProps = {
  dropdown: {},
}

export default Dropdown;

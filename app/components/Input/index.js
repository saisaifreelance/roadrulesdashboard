/**
 *
 * Input
 *
 */

import React from 'react';
import classNames from 'classnames';
// import styled from 'styled-components';

function Input({type, label, placeholder, value, onChange, error, name}) {
  const message = error ? (<small className="help-block">{error}</small>) : null
  return (
    <div className={classNames('form-group', 'fg-float', { 'has-error': error })}>
      <div className="fg-line">
        <input
          name={name}
          value={value}
          placeholder={label}
          type={type}
          onChange={(event) => {
            onChange(name, event.target.value);
            event.preventDefault();
          }}
          className="input-sm form-control fg-input"
        />
        {message}
      </div>
    </div>
  );
}

Input.propTypes = {
  type: React.PropTypes.string.isRequired,
  label: React.PropTypes.string.isRequired,
  placeholder: React.PropTypes.string.isRequired,
  value: React.PropTypes.string.isRequired,
  onChange: React.PropTypes.func.isRequired,
  error: React.PropTypes.string,
};

Input.defaultProps = {
  type: 'text',
  placeholder: '',
};

export default Input;

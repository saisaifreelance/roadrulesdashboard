/**
 *
 * CardPayment
 *
 */

import React from 'react';
import currencyFormatter from 'currency-formatter';
import Button from 'react-md/lib/Buttons/Button';
import Card from 'react-md/lib/Cards/Card';
import CardTitle from 'react-md/lib/Cards/CardTitle';
import CardActions from 'react-md/lib/Cards/CardActions';
import List from 'react-md/lib/Lists/List';
import ListItem from 'react-md/lib/Lists/ListItem';
import FontIcon from 'react-md/lib/FontIcons';
import Divider from 'react-md/lib/Dividers';
import Switch from 'react-md/lib/SelectionControls/Switch';
import Loading from '../Loading';

// import styled from 'styled-components';


class CardPayment extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (this.props.isFetching) {
      return (
        <Card className="md-cell">
          <CardTitle title="Learner activation" />
          <Loading message={this.props.isFetching} />
        </Card>
      );
    }
    if (!this.props.payment) {
      return null;
    }
    const title = this.props.payment.product;
    const subtitle = (new Date(this.props.payment.date_created)).toDateString();
    return (
      <Card className="md-cell md-cell--6">
        <CardTitle
          title={title}
          subtitle={subtitle} />
        <List>
          <ListItem
            leftIcon={<FontIcon iconClassName="zmdi zmdi-money" />}
            primaryText={currencyFormatter.format(this.props.payment.amount, { code: 'USD' })}
          />
          <ListItem
            leftIcon={<FontIcon iconClassName="zmdi zmdi-money" />}
            primaryText={this.props.payment.txn}
          />
          <ListItem
            leftIcon={<FontIcon iconClassName="zmdi zmdi-balance-wallet" />}
            primaryText={this.props.payment.merchant_code}
          />
          <ListItem
            leftIcon={<FontIcon iconClassName="zmdi zmdi-store" />}
            primaryText={this.props.payment.payment_provider}
          />
          <Divider />
          <ListItem
            leftIcon={<FontIcon iconClassName="zmdi zmdi-account" />}
            primaryText={this.props.payment.client_verified ? 'Client confirmed' : 'Client not confirmed'}
          />
          <Switch
            id="merchant_confirmation"
            name="merchant_confirmation"
            label="Merchant confirmation"
            checked={!!this.props.payment.merchant_verified}
            onChange={() => {
              this.props.onChangeField('merchant_confirmation', (new Date()).getTime());
            }}
          />
        </List>
        <CardActions className="md-divider-border md-divider-border--top">
          <Button flat label="Remove" secondary />
          <Button flat label="Send code" secondary />
        </CardActions>
      </Card>
    );
  }
}

CardPayment.propTypes = {
  isFetching: React.PropTypes.string,
  label: React.PropTypes.string.isRequired,
  payment: React.PropTypes.shape({
    amount: React.PropTypes.number,
    txn: React.PropTypes.string,
    merchant_code: React.PropTypes.string,
    payment_provider: React.PropTypes.string,
    client_verified: React.PropTypes.number,
    merchant_verified: React.PropTypes.number,
    date_created: React.PropTypes.number,
  }),
  onChange: React.PropTypes.func.isRequired,
  onChangeField: React.PropTypes.func.isRequired,
};

export default CardPayment;

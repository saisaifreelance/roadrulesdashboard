/**
 *
 * CardActions
 *
 */

import React from 'react';
import classNames from 'classnames';
import ListItem from 'react-md/lib/Lists/ListItem';
import MenuButton from 'react-md/lib/Menus/MenuButton';
import ListItemControl from 'react-md/lib/Lists/ListItemControl';
import Checkbox from 'react-md/lib/SelectionControls/Checkbox';


const styles = {
  active: {
    color: '#00ff00',
  },
  inactive: { },
};

class CardActions extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    actions: React.PropTypes.arrayOf(
      React.PropTypes.shape({
        icon: React.PropTypes.string,
        title: React.PropTypes.string,
        onClick: React.PropTypes.func,
        actions: React.PropTypes.arrayOf(
          React.PropTypes.shape({
            icon: React.PropTypes.string,
            title: React.PropTypes.string,
            onClick: React.PropTypes.func,
          })
        ),
      })
    ),
  }

  constructor(props) {
    super(props);
  }

  render() {
    const actionsComponents = Array.isArray(this.props.actions) && this.props.actions.map((action, i) => {
      if (!Array.isArray(action.actions)) {
        return (
          <MenuButton
            key={i}
            id={`vert-menu-${i}`}
            icon
            buttonChildren={action.icon}
            tooltipLabel={action.title}
            onClick={action.onClick}
            style={action.active ? styles.active : styles.inactive}
          />
        );
      }
      const subActionsComponents = action.actions.map((subAction, j) => {
        if (subAction.control) {
          return (
            <ListItemControl
              key={j}
              primaryAction={
                <Checkbox
                  id={`list_item_${j}`}
                  name={subAction.title}
                  label={subAction.title}
                  checked={subAction.checked}
                  onChange={subAction.onClick}
                />
              }
            />
          );
        }
        return (<ListItem key={j} onClick={subAction.onClick} primaryText={subAction.title} />);
      });
      return (
        <MenuButton
          key={i}
          id={`vert-menu-${i}`}
          icon
          buttonChildren={action.icon}
          tooltipLabel={action.title}
        >
          {subActionsComponents}
        </MenuButton>);
    });

    return (
      <div className="actions">
        {actionsComponents}
      </div>
    );
  }
}

export default CardActions;

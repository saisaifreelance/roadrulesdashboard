/**
 *
 * ChartBar
 *
 */

import React from 'react';
import ReactFlot from 'react-flot';
import '../../../node_modules/react-flot/flot/jquery.flot.time.min.js';
import '../../../node_modules/react-flot/flot/jquery.flot.resize.min.js';
import '../../../node_modules/react-flot/flot/jquery.flot.tooltip.js';
import '../../../vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js';

class ChartBar extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    className: React.PropTypes.string,
    data: React.PropTypes.array.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      id: Date.now(),
    };
  }
  render() {
    const data = this.props.data.map((line, index) => {
      return {
        data: line.data,
        label: line.label,
        bars: {
          show: true,
          barWidth: 0.08,
          order: index + 1,
          lineWidth: 0,
          fillColor: line.color,
        },
      };
    })
    const options = {
      grid: this.props.grid,
      yaxis: this.props.yaxis,
      xaxis: this.props.xaxis,
      legend: this.props.legend,
      tooltip: {
        show: true,
        content: '%s | x: %x; y: %y',
        // cssClass: 'flot-tooltip chart-tooltip',
      },
    }
    return (
      <div className={this.props.className}>
        <ReactFlot id={`${this.state.id}`} className="flot-chart" options={options} data={data} width="100%" height="200px" />
        <div className="flc-bar"></div>
      </div>
    );
  }
}

ChartBar.propTypes = {
  className: React.PropTypes.string,
  grid: React.PropTypes.object,
  yaxis: React.PropTypes.object,
  xaxis: React.PropTypes.object,
  legend: React.PropTypes.object,
  data: React.PropTypes.array.isRequired,
}

ChartBar.defaultProps = {
  grid: {
    borderWidth: 1,
    borderColor: '#eee',
    show: true,
    hoverable: true,
    clickable: true,
  },
  yaxis: {
    tickColor: '#eee',
    tickDecimals: 0,
    font:{
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  xaxis: {
    tickDecimals: 0,
    font: {
      lineHeight: 13,
      style: 'normal',
      color: '#9f9f9f',
    },
    shadowSize: 0,
  },
  legend: {
    container: '.flc-bar',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: 'white',
    lineWidth: 0,
  },
}

export default ChartBar;

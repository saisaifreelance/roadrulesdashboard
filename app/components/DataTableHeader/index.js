/**
 *
 * DataTableHeader
 *
 */

import React from 'react';
import TableHeader from 'react-md/lib/DataTables/TableHeader';
import TableRow from 'react-md/lib/DataTables/TableRow';
import TableColumn from 'react-md/lib/DataTables/TableColumn';
import FontIcon from 'react-md/lib/FontIcons';
import IconSeparator from 'react-md/lib/Helpers/IconSeparator';


import styles from './styles.css';

function DataTableHeader({ schema, sorted, sort, direction }) {
  const columns = schema.reduce((cols, { title, key, type }) => {
    if (type !== 'array') {
      cols.push(
        <TableColumn
          key={key}
          sorted={key === sorted ? direction === 'asc' : undefined}
          onClick={(e) => {
            e.preventDefault();
            if (sorted === key) {
              let newDirection = null;
              if (direction === 'asc') {
                newDirection = 'desc';
              }
              else {
                newDirection = 'asc';
              }
              return sort(key, newDirection);
            }
            return sort(key, 'asc');
          }}
          tooltipLabel={title}
        >
          {title}
        </TableColumn>
      );
    }
    return cols;
  }, []);
  return (
    <TableHeader className={styles.dataTableHeader}>
      <TableRow>
        {columns}
      </TableRow>
    </TableHeader>
  );
}

DataTableHeader.propTypes = {
  sort: React.PropTypes.func.isRequired,
  schema: React.PropTypes.array,
  sorted: React.PropTypes.string,
  direction: React.PropTypes.oneOf(['asc', 'desc']).isRequired,
};

export default DataTableHeader;

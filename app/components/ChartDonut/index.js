/**
 *
 * ChartDonut
 *
 */

import React from 'react';
import ReactFlot from 'react-flot';
import '../../../node_modules/react-flot/flot/jquery.flot.time.min.js';
import '../../../node_modules/react-flot/flot/jquery.flot.resize.js';
import '../../../vendors/bower_components/flot/jquery.flot.pie.js';
import '../../../vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.js';

import styles from './styles.css';

var pieData = [
  {data: 1, color: '#F44336', label: 'Toyota'},
  {data: 2, color: '#03A9F4', label: 'Nissan'},
  {data: 3, color: '#8BC34A', label: 'Hyundai'},
  {data: 4, color: '#FFEB3B', label: 'Scion'},
  {data: 4, color: '#009688', label: 'Daihatsu'},
];

const options = {
  series: {
    pie: {
      innerRadius: 0.5,
      show: true,
      stroke: {
        width: 2,
      },
    },
  },
  legend: {
    container: '.flc-donut',
    backgroundOpacity: 0.5,
    noColumns: 0,
    backgroundColor: "white",
    lineWidth: 0
  },
  grid: {
    hoverable: true,
    clickable: true
  },
  tooltip: true,
  tooltipOpts: {
    content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
    shifts: {
      x: 20,
      y: 0
    },
    defaultTheme: false,
    cssClass: 'flot-tooltip'
  }

}

class ChartDonut extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={`card ${styles.chartDonut}`}>
        <div className="card-header">
          <h2>Donut Chart</h2>
        </div>
        <div className="card-body card-padding">
          <ReactFlot id="donut-chart" className="flot-chart-pie" options={options} data={pieData} width="100%" height="200px" />
          <div className="flc-donut hidden-xs"></div>
        </div>
      </div>
    );
  }
}

export default ChartDonut;

/**
 *
 * Advert
 *
 */

import React from 'react';
import WebFont from 'webfontloader';
import Button from 'react-md/lib/Buttons/Button';
import Card from 'react-md/lib/Cards/Card';
import CardTitle from 'react-md/lib/Cards/CardTitle';
import CardText from 'react-md/lib/Cards/CardText';
import CardActions from 'react-md/lib/Cards/CardActions';
import List from 'react-md/lib/Lists/List';
import ListItem from 'react-md/lib/Lists/ListItem';
import FontIcon from 'react-md/lib/FontIcons';


const degree = '\u00B0';
WebFont.load({
  custom: {
    families: ['WeatherIcons'],
    urls: ['https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.9/css/weather-icons.min.css'],
  },
});


import styles from './styles.css';
import './styles.scss';

function makeIcon(min, max, sunny = true) {
  return [
    <span key="max" className="md-color--text">{`${max}`}</span>,
  ];
}

const tuesday = makeIcon(12, 24);
const wednesday = makeIcon(14, 22);
const thursday = makeIcon(15, 25, false);

function Advert({ad}) {
  return (
    <Card className="weather-card">
      <CardTitle title={ad.driving_school_name} subtitle={ad.description} />
      <CardText className="weather-block">
        <h2 className="md-display-4">{ad.potential_reach}</h2>
        <h6 className="md-headline">potential views</h6>
      </CardText>
      <List>
        <ListItem
          primaryText="potential views"
        />
      </List>
      <List ordered className="weather-list">
        <ListItem primaryText="Tuesday" rightIcon={tuesday}/>
        <ListItem primaryText="Wednesday" rightIcon={wednesday}/>
        <ListItem primaryText="Thursday" rightIcon={thursday}/>
      </List>
      <CardActions className="md-divider-border md-divider-border--top">
        <Button flat label="Full Report" secondary/>
      </CardActions>
    </Card>
  );
}

export default Advert;

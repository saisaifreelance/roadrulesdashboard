/**
 *
 * ChartDownloads
 *
 */

import React from 'react';
import classNames from 'classnames';
import ChartBar from '../../components/ChartBar';
import ChartLine from '../../components/ChartLine';
import ChartSummary from '../../components/ChartSummary';
import ChartSettings from '../../components/ChartSettings';
import CardHeader from '../../components/CardHeader';
import Loading from '../../components/Loading';

import styles from './styles.css';

const animationDuration = 1200;

class ChartDownloads extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      current: 'line',
      next: null,
    };
    this.flipCard = this.flipCard.bind(this);
    this.getActions = this.getActions.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.next && !prevState.next) {
      const timeout = setTimeout(() => {
        this.setState({next: null, current: this.state.next});
        clearTimeout(timeout);
      }, animationDuration / 4);
    }
  }

  getActions(side) {
    switch (side) {
      case 'front': {
        const shown = Object.keys(this.props.shown).map((key) => ({
          title: `Show ${key}`,
          onClick: (checked) => {
            this.props.setShown({ ...this.props.shown, [key]: checked });
          },
          control: true,
          checked: this.props.shown[key],
        }));
        return [
          {
            icon: 'invert_colors',
            title: 'Invert chart (show deficit)',
            active: this.props.invert,
            onClick: this.props.setInvert.bind(undefined, !this.props.invert),
          },
          {
            icon: 'show_chart',
            title: 'Line chart',
            active: this.state.current === 'line',
            onClick: this.flipCard.bind(this, 'line'),
          },
          {
            icon: 'insert_chart',
            title: 'Bar chart',
            active: this.state.current === 'bar',
            onClick: this.flipCard.bind(this, 'bar'),
          },
          {
            icon: 'list',
            title: 'Data summary',
            active: this.state.current === 'summary',
            onClick: this.flipCard.bind(this, 'summary'),
          },
          {
            icon: 'more_vert',
            title: 'Options',
            onClick: () => {
            },
            actions: [
              {
                title: 'Change Target',
                onClick: this.flipCard.bind(this, 'settings'),
              },
              ...shown,
            ],
          },
        ];
      }
      case 'back':
      default:
        return [
          {
            icon: 'close',
            title: '',
            onClick: this.flipCard.bind(this, 'line'),
          },
        ];
    }
  }

  flipCard(next) {
    this.setState({next});
  }

  render() {
    if (this.props.isFetching) {
      return (
        <div className={classNames(styles.chartDownloads, {'animated flipInX': this.state.next})}>
          <div className={classNames('card', styles.chartLine, {[this.props.className]: this.props.className})}>
            <CardHeader
              title={this.props.title}
              description={this.props.description}
            />
            <div className="card-body">
              <div className="chart-edge">
                <div
                  style={{height: 200, width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center'}}
                >
                  <Loading
                    color="amber"
                    message={this.props.isFetching}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    let component = null;
    let actions = null;
    switch (this.state.current) {
      case 'line':
        component = (
          <ChartLine data={this.props.data} {...this.props.options} />
        );
        actions = this.getActions('front');
        break;
      case 'bar':
        component = (
          <ChartBar data={this.props.data} {...this.props.options} />
        );
        actions = this.getActions('front');
        break;
      case 'summary':
        component = (
          <ChartSummary className="card-body card-padding" targets={this.props.targets} data={this.props.data} {...this.props.options} />
        );
        actions = this.getActions('back');
        break;
      case 'settings':
        component = (
          <ChartSettings
            className="card-body card-padding"
            targets={this.props.targets}
            onSubmit={(name, value) => {
              this.props.setTarget(name, value);
              return this.flipCard('line');
            }}
          />
        );
        actions = this.getActions('back');
        break;
      default:
        component = null;
    }
    return (
      <div className={classNames('card', {'animated flipInX': this.state.next})}>
        <CardHeader title={this.props.title} description={this.props.description} actions={actions}/>
        {component}
      </div>
    );
  }
}

ChartDownloads.propTypes = {
  options: React.PropTypes.object,
  data: React.PropTypes.array,
  title: React.PropTypes.string,
  description: React.PropTypes.string,
  isFetching: React.PropTypes.string,
  className: React.PropTypes.string,
  setTarget: React.PropTypes.func.isRequired,
  setInvert: React.PropTypes.func.isRequired,
  invert: React.PropTypes.bool.isRequired,
  targets: React.PropTypes.objectOf(React.PropTypes.number.isRequired).isRequired,
  shown: React.PropTypes.objectOf(React.PropTypes.bool.isRequired).isRequired,
  setShown: React.PropTypes.func.isRequired,
}

export default ChartDownloads;

/**
 *
 * Loading
 *
 */

import React from 'react';

// import styled from 'styled-components';


function Loading({color, message}) {
  const text = message ? (<h4>{message}</h4>) : null
  return (
    <div className="text-center">
      <div className={`preloader pl-xl pls-${color}`}>
        <svg className="pl-circular" viewBox="25 25 50 50">
          <circle className="plc-path" cx="50" cy="50" r="20" />
        </svg>
      </div>
      {text}
    </div>
  );
}

Loading.propTypes = {
  color: React.PropTypes.oneOf(['red', 'blue', 'green', 'yellow', 'bluegray', 'amber', 'teal', 'gray', 'pink', 'purple', 'white']),
  message: React.PropTypes.string,
};

Loading.defaultProps = {
  color: 'amber',
}

export default Loading;

/**
*
* Footer
*
*/

import React from 'react';


import styles from './styles.css';

function Footer() {
  return (
    <footer id="footer" className={styles.footer}>
      Copyright &copy; 2017 Road Rules

      <ul className="f-menu">
        <li><a href="">Home</a></li>
        <li><a href="">Dashboard</a></li>
        <li><a href="">Reports</a></li>
        <li><a href="">Support</a></li>
        <li><a href="">Contact</a></li>
      </ul>
    </footer>
  );
}

export default Footer;

/**
*
* EditClients
*
*/

import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import Button from 'react-md/lib/Buttons/Button';
import Dialog from 'react-md/lib/Dialogs';
import Toolbar from 'react-md/lib/Toolbars';
import ClientContainer from '../../containers/ClientContainer';
// import styled from 'styled-components';


class EditClients extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  closeDialog = () => {
    this.props.closeDialog();
    this.setState({ count: 1 });
  };

  handleSubmit = (e) => {
    this.props.submitRows(e);
    this.setState({ count: 1 });
  };

  render() {
    const { visible, clients } = this.props;
    const clientKeys = Object.keys(clients);
    const submit = <Button type="submit" label="Submit" flat />;
    const groups = clientKeys.map((clientKey, index) => (
      <ClientContainer key={index} index={index} clientId={clientKey} />
    ));
    return (
      <Dialog
        id="add-row-dialog"
        aria-label="Edit clients data"
        visible={visible}
        onHide={this.closeDialog}
        fullPage
      >
        <CSSTransitionGroup
          transitionName="md-cross-fade"
          transitionEnterTimeout={300}
          transitionLeave={false}
          component="form"
          onSubmit={this.handleSubmit}
          className="md-text-container md-toolbar-relative"
          id="new-nutrition-row-form"
        >
          <Toolbar
            nav={<Button icon onClick={this.closeDialog}>arrow_back</Button>}
            title="Create new row"
            fixed
            colored
            actions={submit}
          />
          {groups}
        </CSSTransitionGroup>
        <Button floating fixed onClick={this.addGroup} primary>add</Button>
      </Dialog>
    );
  }
}

EditClients.propTypes = {
  visible: React.PropTypes.bool.isRequired,
  clients: React.PropTypes.object.isRequired,
  submitRows: React.PropTypes.func.isRequired,
  closeDialog: React.PropTypes.func.isRequired,
};

export default EditClients;

/**
*
* ButtonGroup
*
*/

import React from 'react';
// import styled from 'styled-components';


function ButtonGroup(props) {
  return (
    <div className="btn-group">
      {props.children}
    </div>
  );
}

ButtonGroup.propTypes = {
  children: React.PropTypes.oneOfType([React.PropTypes.element, React.PropTypes.arrayOf(React.PropTypes.element)]),
};

export default ButtonGroup;

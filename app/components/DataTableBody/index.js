/**
 *
 * DataTableBody
 *
 */

import React from 'react';
import TableBody from 'react-md/lib/DataTables/TableBody';
import TableRow from 'react-md/lib/DataTables/TableRow';
import TableColumn from 'react-md/lib/DataTables/TableColumn';
import EditDialogColumn from 'react-md/lib/DataTables/EditDialogColumn';

function DataTableBody({ inline, large, saveOnOutsideClick, data, schema }) {
  const rows = data.map((record) => {
    const columns = schema.reduce((cols, {title, key, type, editable}) => {
      cols.push((<TableColumn key={key}>{record[key]}</TableColumn>));
      return cols;
    }, []);
    return (
      <TableRow key={record.id}>
        {columns}
      </TableRow>
    );
  });

  return <TableBody>{rows}</TableBody>;
}

DataTableBody.propTypes = {
  large: React.PropTypes.bool.isRequired,
  inline: React.PropTypes.bool.isRequired,
  saveOnOutsideClick: React.PropTypes.bool.isRequired,
  data: React.PropTypes.array.isRequired,
};

export default DataTableBody;

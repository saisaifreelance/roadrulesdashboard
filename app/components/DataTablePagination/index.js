/**
*
* DataTablePagination
*
*/

import React from 'react';
import TablePagination from 'react-md/lib/DataTables/TablePagination';
// import styled from 'styled-components';


function DataTablePagination({ paginate, count, pageSize, page}) {
  return (
    <TablePagination onPagination={paginate} rows={count} rowsPerPage={pageSize} rowsPerPageItems={[5, 10, 20, 30, 40, 50, 100]} page={page} />
  );
}

DataTablePagination.propTypes = {
  paginate: React.PropTypes.func.isRequired,
  count: React.PropTypes.number.isRequired,
  pageSize: React.PropTypes.number.isRequired,
  page: React.PropTypes.number.isRequired,
};

export default DataTablePagination;

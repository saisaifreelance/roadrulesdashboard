/**
 *
 * FormRow
 *
 */

import React, { PropTypes } from 'react';
import { firebaseConnect } from 'react-redux-firebase';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import SelectField from 'react-md/lib/SelectFields';
import TextField from 'react-md/lib/TextFields';
import Button from 'react-md/lib/Buttons';
import LinearProgress from 'react-md/lib/Progress/LinearProgress';
import FileUpload from 'react-md/lib/FileInputs/FileUpload';
import Loading from '../Loading';
import FilePreview from '../FilePreview';
// import styled from 'styled-components';


class FormRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = { files: {} };
    this.onLoadStart = this.onLoadStart.bind(this);
    this.onLoadEnd = this.onLoadEnd.bind(this);
    this.onProgress = this.onProgress.bind(this);
  }

  onLoadStart(schema, file) {
    this.setState({ file });
  }

  onLoadEnd(schema, file, uploadResult) {
    this.props.uploadItemImage(this.props.path, schema, file, uploadResult);
  }

  onProgress(file, progress) {
    // The progress event can sometimes happen once more after the abort
    // has been called. So this just a sanity check
    if (this.state.file === file) {
      this.setState({ progress });
    }
  }

  render() {
    const { index, schema, editItem, editItemError, saveItem, path, pathData } = this.props;
    if (!pathData) {
      return <Loading color="amber" message="No data" />;
    }
    const { isFetching, dataErrors, formErrors } = pathData
    if (isFetching) {
      return <Loading color="amber" message={isFetching} />;
    }
    const data = Object.assign({}, pathData.data, pathData.form);
    const fields = schema.map((field, i) => {
      switch (field.type) {
        case 'text':
          return (
            <TextField
              key={field.key}
              id={`${field.key}-${index}`}
              name={`${field.key}-${index}`}
              label={field.title}
              value={data[field.key]}
              onChange={(...args) => editItem(path, field.key, ...args)}
              placeholder={field.title}
              className="md-cell"
              error={!!formErrors[field.key]}
              errorText={formErrors[field.key]}
              type="text"
              disabled={!field.formEditable}
            />
          );
        case 'multi-text':
          return (
            <TextField
              key={field.key}
              id={`${field.key}-${index}`}
              name={`${field.key}-${index}`}
              label={field.title}
              value={data[field.key]}
              onChange={(...args) => editItem(path, field.key, ...args)}
              placeholder={field.title}
              className="md-cell md-cell--12"
              error={!!formErrors[field.key]}
              errorText={formErrors[field.key]}
              type="text"
              rows={2}
              disabled={!field.formEditable}
            />
          );
        case 'number':
          return (
            <TextField
              key={field.key}
              id={`${field.key}-${index}`}
              name={`${field.key}-${index}`}
              label={field.title}
              value={data[field.key]}
              onChange={(...args) => editItem(path, field.key, ...args)}
              placeholder={field.title}
              className="md-cell"
              error={!!formErrors[field.key]}
              errorText={formErrors[field.key]}
              type="number"
              disabled={!field.formEditable}
            />
          );
        case 'select':
          return (
            <SelectField
              key={field.key}
              id={`${field.key}-${index}`}
              name={`${field.key}-${index}`}
              label={field.title}
              value={data[field.key]}
              onChange={(...args) => editItem(path, field.key, ...args)}
              placeholder={`Select a ${field.title}`}
              menuItems={field.options}
              error={!!formErrors[field.key]}
              errorText={formErrors[field.key]}
              itemLabel="label"
              itemValue="value"
              className="md-cell"
              helpOnFocus
              helpText={`Select a ${field.title} from the list`}
              disabled={!field.formEditable}
            />
          );
        case 'image': {
          console.log(field.key);
          return (
            <div key={field.key} className="md-cell md-cell--12">
              <FileUpload
                id={`${field.key}-${index}`}
                name={`${field.key}-${index}`}
                label={field.title}
                multiple
                secondary
                ref={(upload) => { this.upload = upload; }}
                onLoadStart={(...args) => this.onLoadStart(field, ...args)}
                onLoad={(...args) => this.onLoadEnd(field, ...args)}
                onProgress={(...args) => this.onProgress(field, ...args)}
              />
              <CSSTransitionGroup
                component="output"
                className="md-grid"
                transitionName="md-cross-fade"
                transitionEnterTimeout={300}
                transitionLeave={false}
              >
              </CSSTransitionGroup>
            </div>
          );
        }
        default:
          return <h2 key={field.key}>{field.type}</h2>;
      }
    });
    return (
      <section className="md-grid" aria-labelledby={`new-row-group-${index + 1}`}>
        <h2 id={`new-row-group-${index + 1}`} className="md-cell md-cell--12">Edit Item {data.id}</h2>
        {fields}
      </section>
    );
  }
}

FormRow.propTypes = {
  index: PropTypes.number.isRequired,
  schema: PropTypes.array.isRequired,
  path: PropTypes.string,
  uploadItemImage: PropTypes.func.isRequired,
  editItem: PropTypes.func.isRequired,
  editItemError: PropTypes.func.isRequired,
  saveItem: PropTypes.func.isRequired,
  pathData: PropTypes.shape({
    isFetching: PropTypes.string,
    data: PropTypes.object,
    dataErrors: PropTypes.object,
    form: PropTypes.object,
    formErrors: PropTypes.object,
  }),
};

export default firebaseConnect((props) => {
  if (props.path) {
    return ([
      {
        path: props.path,
      },
    ]);
  }
  return [];
})(FormRow);

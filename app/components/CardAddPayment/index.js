/**
 *
 * CardAddPayment
 *
 */

import React from 'react';
import currencyFormatter from 'currency-formatter';
import Button from 'react-md/lib/Buttons/Button';
import Card from 'react-md/lib/Cards/Card';
import CardTitle from 'react-md/lib/Cards/CardTitle';
import CardActions from 'react-md/lib/Cards/CardActions';
import CardText from 'react-md/lib/Cards/CardText';
import TextField from 'react-md/lib/TextFields';
import SelectField from 'react-md/lib/SelectFields';
import List from 'react-md/lib/Lists/List';
import ListItem from 'react-md/lib/Lists/ListItem';
import FontIcon from 'react-md/lib/FontIcons';
import Divider from 'react-md/lib/Dividers';
import Switch from 'react-md/lib/SelectionControls/Switch';
import Loading from '../Loading';
// import styled from 'styled-components';


const MERCHANT_CODES = [
  {
    name: 'Ecocash learner',
    value: '90980',
  },
  {
    name: 'Ecocash driver',
    value: '90980',
  },
  {
    name: 'Telecash',
    value: '90980',
  },
  {
    name: 'One Wallet',
    value: '90980',
  },
];


const PAYMENT_PROVIDERS = [
  {
    name: 'Ecocash',
    value: 'ecocash',
  },
  {
    name: 'Telecash',
    value: 'telecash',
  },
  {
    name: 'One Wallet',
    value: 'one_wallet',
  },
  {
    name: 'Cash',
    value: 'cash',
  },
]


const PRODUCTS = [
  {
    name: 'Learner Activation',
    value: 'activation_learner',
  },
  {
    name: 'Driver Activation',
    value: 'activation_driver',
  },
]


class CardAddPayment extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  onSubmit() {
    const errors = {};
    const { amount, txn, merchant_code, payment_provider, product, client_verified, merchant_verified, date_created} = this.props.payment;

    if (!amount || isNaN(amount)) {
      errors.amount = 'A numeric value for amount is required';
    }
    if (!txn) {
      errors.txn = 'A transaction ID is required';
    }
    if (!merchant_code) {
      errors.merchant_code = 'A merchant code is required';
    }
    if (!payment_provider) {
      errors.payment_provider = 'A payment provider code is required';
    }
    if (!product) {
      errors.product = 'A product code is required';
    }

    if (Object.keys(errors).length) {
      return this.props.onError(errors);
    }
    return this.props.submitPayment(this.props.payment, this.props.clientId);
  }
  render() {
    if (this.props.isFetching) {
      return (
        <Card className="md-cell">
          <CardTitle title="Learner activation" />
          <Loading message={this.props.isFetching} />
        </Card>
      );
    }
    return (
      <Card className="md-cell md-cell--6">
        <CardTitle title={'Add payment'} subtitle={(new Date(this.props.payment.date_created)).toDateString()} />

        <CardText>
          <TextField
            id={'amount'}
            name={'amount'}
            label="Amount"
            placeholder="Amount"
            className="md-cell md-cell--12"
            value={this.props.payment.amount}
            onChange={(...args) => this.props.onChangeField('amount', ...args)}
            error={!!this.props.payment_errors.amount}
            errorText={this.props.payment_errors.amount}
          />
          <SelectField
            id={'product'}
            name={'product'}
            label="Product"
            itemLabel="name"
            itemValue="value"
            menuItems={PRODUCTS}
            className="md-cell md-cell--12"
            value={this.props.payment.product}
            onChange={(...args) => this.props.onChangeField('product', ...args)}
            error={!!this.props.payment_errors.product}
            errorText={this.props.payment_errors.product}
          />
          <SelectField
            id={'merchant_code'}
            name={'merchant_code'}
            label="Merchant code"
            itemLabel="name"
            itemValue="value"
            menuItems={MERCHANT_CODES}
            className="md-cell md-cell--12"
            value={this.props.payment.merchant_code}
            onChange={(...args) => this.props.onChangeField('merchant_code', ...args)}
            error={!!this.props.payment_errors.merchant_code}
            errorText={this.props.payment_errors.merchant_code}
          />

          <SelectField
            id={'payment_provider'}
            name={'payment_provider'}
            label="Payment Provider"
            itemLabel="name"
            itemValue="value"
            menuItems={PAYMENT_PROVIDERS}
            className="md-cell md-cell--12"
            value={this.props.payment.payment_provider}
            onChange={(...args) => this.props.onChangeField('payment_provider', ...args)}
            error={!!this.props.payment_errors.payment_provider}
            errorText={this.props.payment_errors.payment_provider}
          />

          <TextField
            id={'txn'}
            name={'txn'}
            label="Transaction ID"
            placeholder="Transaction ID"
            className="md-cell md-cell--12"
            value={this.props.payment.txn}
            onChange={(...args) => this.props.onChangeField('txn', ...args)}
            error={!!this.props.payment_errors.txn}
            errorText={this.props.payment_errors.txn}
          />
        </CardText>

        <CardActions className="md-divider-border md-divider-border--top">
          <Button flat label="Remove" secondary />
          <Button flat label="Add Payment" secondary onClick={(e) => this.onSubmit()} />
        </CardActions>
      </Card>
    );
  }
}

CardAddPayment.propTypes = {
  clientId: React.PropTypes.string.isRequired,
  isFetching: React.PropTypes.string,
  submitPayment: React.PropTypes.func.isRequired,
  payment: React.PropTypes.object,
  payment_errors: React.PropTypes.object,
  onChangeField: React.PropTypes.func.isRequired,
  onError: React.PropTypes.func.isRequired,
};

export default CardAddPayment;

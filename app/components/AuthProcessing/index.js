/**
*
* AuthProcessing
*
*/

import React from 'react';
import Helmet from 'react-helmet';
// import styled from 'styled-components';

import Loading from '../../components/Loading';

function AuthProcessing({ message }) {
  return (
    <div className="l-block toggled" id="l-login">
      <Helmet
        title="Road Rules Admin|Processing..."
        meta={[
          {name: 'Road Rules Admin', content: 'Processing auth...'},
        ]}
      />
      <div className="lb-header palette-Orange bg">
        <i className="zmdi zmdi-account-circle"></i>
        Hi there
      </div>

      <div className="lb-body">
        <Loading message={message} color="amber" />
      </div>
    </div>
  );
}

AuthProcessing.propTypes = {
  message: React.PropTypes.string.isRequired,
}

AuthProcessing.propTypes = {

};

export default AuthProcessing;

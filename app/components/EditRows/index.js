/**
*
* EditRows
*
*/

import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import Button from 'react-md/lib/Buttons/Button';
import Dialog from 'react-md/lib/Dialogs';
import Toolbar from 'react-md/lib/Toolbars';
import EditRowsContainer from '../../containers/EditRowsContainer';
// import styled from 'styled-components';


class EditRows extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  closeDialog = () => {
    this.props.closeDialog();
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.editRowsContainer.getWrappedInstance().saveItem();
  };

  render() {
    const { visible, keys, schema, root } = this.props;
    const submit = <Button type="submit" label="Submit" flat />;
    return (
      <Dialog
        id="add-row-dialog"
        aria-label="Edit clients data"
        visible={visible}
        onHide={this.closeDialog}
        fullPage
      >
        <CSSTransitionGroup
          transitionName="md-cross-fade"
          transitionEnterTimeout={300}
          transitionLeave={false}
          component="form"
          onSubmit={this.handleSubmit}
          className="md-text-container md-toolbar-relative"
          id="new-nutrition-row-form"
        >
          <Toolbar
            nav={<Button icon onClick={this.closeDialog}>arrow_back</Button>}
            title="Create new row"
            fixed
            colored
            actions={submit}
          />
          <EditRowsContainer
            ref={(editRowsContainer) => { this.editRowsContainer = editRowsContainer; }}
            keys={keys}
            schema={schema}
            root={root}
          />
        </CSSTransitionGroup>
        <Button floating fixed onClick={this.addGroup} primary>add</Button>
      </Dialog>
    );
  }
}

EditRows.propTypes = {
  root: React.PropTypes.string,
  schema: React.PropTypes.array,
  keys: React.PropTypes.object.isRequired,
  visible: React.PropTypes.bool.isRequired,
  submitRows: React.PropTypes.func.isRequired,
  closeDialog: React.PropTypes.func.isRequired,
};

export default EditRows;

// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import {getAsyncInjectors} from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const {injectReducer, injectSagas} = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/DashboardContainer/reducer'),
          import('containers/DashboardContainer/sagas'),
          import('containers/MonthlySalesContainer/reducer'),
          import('containers/MonthlySalesContainer/sagas'),
          import('containers/WeeklySalesContainer/reducer'),
          import('containers/WeeklySalesContainer/sagas'),
          import('containers/DailySalesContainer/reducer'),
          import('containers/DailySalesContainer/sagas'),
          import('containers/PaidOverallContainer/reducer'),
          import('containers/PaidOverallContainer/sagas'),
          import('containers/PaidMonthContainer/reducer'),
          import('containers/PaidMonthContainer/sagas'),
          import('containers/DashboardContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, dashboardReducer, dashboardSagas, monthlySalesReducer, monthlySalesSagas, weeklySalesReducer, weeklySalesSagas, dailySalesReducer, dailySalesSagas, paidOverallReducer, paidOverallSagas, paidMonthReducer, paidMonthSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('dashboardContainer', dashboardReducer.default);
          injectSagas('dashboardContainer', dashboardSagas.default);
          injectReducer('monthlySalesContainer', monthlySalesReducer.default);
          injectSagas('monthlySalesContainer', monthlySalesSagas.default);
          injectReducer('weeklySalesContainer', weeklySalesReducer.default);
          injectSagas('weeklySalesContainer', weeklySalesSagas.default);
          injectReducer('dailySalesContainer', dailySalesReducer.default);
          injectSagas('dailySalesContainer', dailySalesSagas.default);
          injectReducer('paidOverallContainer', paidOverallReducer.default);
          injectSagas('paidOverallContainer', paidOverallSagas.default);
          injectReducer('paidMonthContainer', paidMonthReducer.default);
          injectSagas('paidMonthContainer', paidMonthSagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/login',
      name: 'loginContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/LoginContainer/reducer'),
          import('containers/LoginContainer/sagas'),
          import('containers/LoginContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('loginContainer', reducer.default);
          injectSagas('loginContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/register',
      name: 'registerContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/RegisterContainer/reducer'),
          import('containers/RegisterContainer/sagas'),
          import('containers/RegisterContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('registerContainer', reducer.default);
          injectSagas('registerContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/forgot',
      name: 'forgotPasswordContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/ForgotPasswordContainer/reducer'),
          import('containers/ForgotPasswordContainer/sagas'),
          import('containers/ForgotPasswordContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('forgotPasswordContainer', reducer.default);
          injectSagas('forgotPasswordContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/clients',
      name: 'clientsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/ClientsContainer/reducer'),
          import('containers/ClientsContainer/sagas'),
          import('containers/ClientContainer/reducer'),
          import('containers/ClientContainer/sagas'),
          import('containers/ClientsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, clientsReducer, clientsSagas, clientReducer, clientSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('clientsContainer', clientsReducer.default);
          injectSagas('clientsContainer', clientsSagas.default);
          injectReducer('clientContainer', clientReducer.default);
          injectSagas('clientContainer', clientSagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/initialise',
      name: 'initialiseContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/InitialiseContainer/reducer'),
          import('containers/InitialiseContainer/sagas'),
          import('containers/InitialiseContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('initialiseContainer', reducer.default);
          injectSagas('initialiseContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },

    {
      path: '/questions',
      name: 'questionsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/QuestionsContainer/reducer'),
          import('containers/QuestionsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/QuestionsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('questionsContainer', reducer.default);
          injectSagas('questionsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/notes',
      name: 'notesContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/NotesContainer/reducer'),
          import('containers/NotesContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/NotesContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('notesContainer', reducer.default);
          injectSagas('notesContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/fines',
      name: 'trafficFinesContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/TrafficFinesContainer/reducer'),
          import('containers/TrafficFinesContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/TrafficFinesContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('trafficFinesContainer', reducer.default);
          injectSagas('trafficFinesContainer', sagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/driving_schools',
      name: 'drivingSchoolsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/DrivingSchoolsContainer/reducer'),
          import('containers/DrivingSchoolsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/DrivingSchoolsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('drivingSchoolsContainer', reducer.default);
          injectSagas('drivingSchoolsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
/*
    {
      path: '/school_bookings',
      name: 'drivingSchoolBookingsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/DrivingSchoolBookingsContainer/reducer'),
          import('containers/DrivingSchoolBookingsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/DrivingSchoolBookingsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('drivingSchoolBookingsContainer', reducer.default);
          injectSagas('drivingSchoolBookingsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },*/
/*
    {
      path: '/test_bookings',
      name: 'provisionalTestBookingsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/ProvisionalTestBookingsContainer/reducer'),
          import('containers/ProvisionalTestBookingsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/ProvisionalTestBookingsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('provisionalTestBookingsContainer', reducer.default);
          injectSagas('provisionalTestBookingsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },*/
/*
    {
      path: '/push_notifications',
      name: 'pushNotificationsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/PushNotificationsContainer/reducer'),
          import('containers/PushNotificationsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/PushNotificationsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('pushNotificationsContainer', reducer.default);
          injectSagas('pushNotificationsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },*/
/*
    {
      path: '/ads',
      name: 'advertisementsContainer',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/AdvertisementsContainer/reducer'),
          import('containers/AdvertisementsContainer/sagas'),
          import('containers/EditRowsContainer/reducer'),
          import('containers/EditRowsContainer/sagas'),
          import('containers/AdvertisementsContainer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, reducer, sagas, editRowsReducer, editRowsSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          injectReducer('editRowsContainer', editRowsReducer.default);
          injectSagas('editRowsContainer', editRowsSagas.default);
          injectReducer('advertisementsContainer', reducer.default);
          injectSagas('advertisementsContainer', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },*/

    {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/HeaderContainer/reducer'),
          import('containers/HeaderContainer/sagas'),
          import('containers/NotFoundPage'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([headerReducer, headerSagas, component]) => {
          injectReducer('headerContainer', headerReducer.default);
          injectSagas('headerContainer', headerSagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
  ];
}

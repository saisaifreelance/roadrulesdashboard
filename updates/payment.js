export function createPayment(firebase, payment, clientId) {
  const data = { ...payment };
  const ref = firebase.ref();
  const paymentId = ref.child('payments').push().key;
  data.id = paymentId;
  data.client = clientId;

  const updates = {}
  updates[`/clients/${clientId}/payments/${paymentId}`] = true
  updates[`/payments/${paymentId}`] = data

  if (data.product === 'activation_learner') {
    updates[`/clients/${clientId}/date_paid_learner`] = data.date_created;
    updates[`/clients/${clientId}/payment_learner`] = paymentId;
  }

  if (data.product === 'activation_driver') {
    updates[`/clients/${clientId}/date_paid_driver`] = data.date_created;
    updates[`/clients/${clientId}/payment_driver`] = paymentId;
  }
  return ref.update(updates).then((error) => {
    if (!error) {
      return error;
    }
    throw new Error(error);
  });
}

export function updatePayment(firebase, payment, paymentId) {
  const data = { ...payment };
  const ref = firebase.ref();
  const updates = Object.keys(data).reduce((updates, updateKey) => ({ ...updates, [`/payments/${paymentId}/${updateKey}`]: data[updateKey] }), {})

  return ref.update(updates).then((error) => {
    if (!error) {
      return error;
    }
    throw new Error(error);
  });
}

export function deletePayment(firebase, payment) {
  const { client, id, product } = payment
  const ref = firebase.ref();
  const updates = {
    [`/payments/${id}`]: null,
    [`/clients/${client}/payments/${id}`]: null,
  };

  if (product === 'activation_learner') {
    //add code
  }

  return ref.update(updates).then((error) => {
    if (!error) {
      return error;
    }
    throw new Error(error);
  });
}
